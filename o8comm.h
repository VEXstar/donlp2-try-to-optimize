/* **************************************************************************** */
/*                                  o8comm.h                                    */
/* **************************************************************************** */
#pragma once                                      
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

/* Macros */

//#define max(A,B)    ((A) > (B) ? (A) : (B))
//#define min(A,B)    ((A) < (B) ? (A) : (B))

/* Types */

typedef int         INTEGER;
typedef int         LOGICAL;
enum { FALSE, TRUE };
typedef float       REAL;
typedef double      DOUBLE;

#include "errcod.h"

typedef struct {
	char name[256];
	INTEGER n;
	INTEGER nlin;
	INTEGER nonlin;
	INTEGER maxiter;
	INTEGER nstep;
	INTEGER Flags;
	DOUBLE epsdif;
	DOUBLE epsfcn;
	DOUBLE taubnd;
	INTEGER difftype;
	INTEGER nreset;
	DOUBLE del0;
	DOUBLE tau0;
	DOUBLE tau;
	DOUBLE big;
	DOUBLE *x;
	DOUBLE *low;
	DOUBLE *up;
	DOUBLE **gres;
	char Reserved[152];
} TOPSLV1_INITDATA, *POPSLV1_INITDATA;

typedef struct {
	INTEGER nIter;
	INTEGER nFuncEval;
	INTEGER nGradFuncEval;
	INTEGER nConstrEval;
	INTEGER nConstrTotalEval;
	INTEGER nGradConstrEval;
	INTEGER nGradConstrTotalEval;
	char Reserved[36];
} TOPSLV1_STATDATA, *POPSLV1_STATDATA;

typedef void(__stdcall *PDll_Init)(void *pContext, POPSLV1_INITDATA pData, void *pReserved);
typedef DOUBLE(__stdcall *PDll_Func)(void *pContext, DOUBLE *x, void *pReserved);
typedef void(__stdcall *PDll_GradFunc)(void *pContext, DOUBLE *x, DOUBLE *Grad, void *pReserved);
typedef void(__stdcall *PDll_ConstrFunc)(void *pContext, INTEGER type, INTEGER *liste, DOUBLE *x, DOUBLE *con, LOGICAL *err, void *pReserved);
typedef void(__stdcall *PDll_GradConstrFunc)(void *pContext, INTEGER *liste, INTEGER shift, DOUBLE *x, DOUBLE **grad, void *pReserved);
typedef LOGICAL(__stdcall *PDll_Step)(void *pContext, DOUBLE *x, DOUBLE *u, INTEGER itstep, DOUBLE **accinf, void *pReserved);
typedef void(__stdcall *PDll_InitAgain)(void *pContext, POPSLV1_INITDATA pData, void *pReserved);

typedef struct {
	PDll_Init Init;
	PDll_Func Func;
	PDll_GradFunc GradFunc;
	PDll_ConstrFunc ConstrFunc;
	PDll_GradConstrFunc GradConstrFunc;
	PDll_Step Step;
	POPSLV1_STATDATA pStatData;
	PDll_InitAgain InitAgain;
	char Reserved[32];
} TOPSLV1_PROBLEM, *POPSLV1_PROBLEM;

typedef struct
{
	REAL      runtim;
	REAL      optite;
	DOUBLE    **accinf;
	INTEGER   itstep, phase;

	DOUBLE    upsi, upsi0, upsi1, upsist, psi, psi0,
		psi1, psist, psimin,
		phi, phi0, phi1, phimin, fx, fx0, fx1,
		fxst, fmin_, b2n, b2n0, xnorm, x0norm, sig0, dscal, dnorm, d0norm;
	DOUBLE    sig, sigmin, dirder, cosphi, upsim;
	DOUBLE    *x, *x0, *x1, *xmin, *d, *d0,
		*dd, *difx, *resmin;

	DOUBLE    *gradf, gfn, *qgf, *gphi0, *gphi1,
		**gres, *gresn;

	INTEGER   *perm, *perm1, *colno, rank;
	DOUBLE    **qr, *betaq, *diag,
		*cscal,
		*colle;

	/* colno also used o8qpso with double length ! */

	DOUBLE    **a, scalm, scalm2, *diag0, matsc;

	INTEGER   *violis, *alist, *o8bind,
		*o8bind0, *aalist, *clist;

	DOUBLE    *u, *u0,
		*w, *w1, *res,
		*res0, *res1,
		*resst, scf, scf0,
		*yu, *slack, infeas, *work;

	
	INTEGER   iterma;
	DOUBLE    del, del0, del01, delmin, tau0, tau, ny;
	DOUBLE    smalld, smallw, rho, rho1, eta, epsx, c1d,
		scfmax, updmy0, tauqp, taufac, taumax;

	DOUBLE    alpha, beta, theta, sigsm, sigla, delta, stptrm;
	DOUBLE    delta1, stmaxl;
	

	DOUBLE    level;
	INTEGER   clow, lastdw, lastup, lastch;

	FILE      *prou, *meu;

	DOUBLE    *ug, *og;

	DOUBLE    *low, *up, big;

	INTEGER   nreset;

	DOUBLE    *xst;


	/* Global variables only used locally */

	DOUBLE   *donlp2_yy;
	DOUBLE   *o8bfgs_dg, *o8bfgs_adx, *o8bfgs_ltdx,
		*o8bfgs_gtdx, *o8bfgs_updx, *o8bfgs_updz;
	DOUBLE   *o8opti_qtx;
	DOUBLE   *o8opti_yy, *o8opti_yx, *o8opti_trvec, *o8opti_con;
	INTEGER  *o8opti_delist, *o8opti_bindba;
	DOUBLE   *o8eval_con;
	DOUBLE   *o8unim_step;
	DOUBLE   *o8dec_qri, *o8dec_qri0;
	DOUBLE   *o8elim_qri, *o8elim_y, *o8elim_rhsscal;
	INTEGER  *o8elim_col;
	DOUBLE   *o8sol_xl;
	DOUBLE   *o8upd_sdiag, *o8upd_rn1, *o8upd_w;
	DOUBLE   *o8qpdu_g0, *o8qpdu_ci0, *o8qpdu_cii,
		*o8qpdu_cei,
		*o8qpdu_xd, *o8qpdu_s, *o8qpdu_z, *o8qpdu_vr;
	DOUBLE   *o8qpdu_xdold, *o8qpdu_udold;
	INTEGER  *o8qpdu_ai, *o8qpdu_iai, *o8qpdu_iaexcl, *o8qpdu_aiold;
	INTEGER  *o8qpdu_eqlist, *o8qpdu_iqlist;
	DOUBLE   *o8qpdu_y, *o8qpdu_mult;
	INTEGER  *o8qpdu_qpdel;
	LOGICAL  *escongrad_errloc;
	DOUBLE   *escongrad_fhelp1, *escongrad_fhelp2,
		*escongrad_fhelp3, *escongrad_fhelp4,
		*escongrad_fhelp5, *escongrad_fhelp6;


	/* **************************************************************************** */
	/*                                  o8fuco.h                                    */
	/* **************************************************************************** */

	 LOGICAL   *val, *llow, *lup;

	 INTEGER   n, nr, nres, nlin, nonlin, nstep, ndualm, mdualm;

	 DOUBLE    epsmac, tolmac, deldif;

	 char      name[41];

	 DOUBLE    epsdif;

	 LOGICAL   intakt, te0, te1, te2, te3, singul;
	 LOGICAL   ident, eqres, silent, analyt, cold;

	 INTEGER   icf, icgf, cfincr, *cres, *cgres;

	 LOGICAL   ffuerr, *confuerr;

	/*  special variables for the interface to old fashioned function */
	/*  specification                                                 */
	/*  can be removed is only problems in the new formulation are to be used */
	 INTEGER nh, ng;

	 INTEGER *nonlinlist;

	 INTEGER **gunit;

	 LOGICAL *gconst;

	 LOGICAL *cfuerr;
	/* this is necessary because of the different index positions used */
	/*  in the old and new versions   */

	/* **************************************************************************** */
	/*                                   o8fint.h                                   */
	/* **************************************************************************** */

	/* if bloc = TRUE then it is assumed that functionevaluation takes place        */
	/* at once in an external routine and                                           */
	/* that user_eval has been called before calling for evaluation of functions    */
	/* the latter then simply consists in copying data                              */
	/* to donlp2's own data                                                         */
	/* user_eval must set valid = TRUE, if functionvalues are valid for the         */
	/* current xtr                                                                  */
	/* corr is set to true by donlp2, if the initial x does not satisfy             */
	/* the bound constraints. x is modified in this case                            */
	/* difftype = 1,2,3 numerical differentiation by the ordinary forward           */
	/* differences, by central differences or by Richardson-extrapolation           */
	/* of order 6, requiring n, 2n , 6n additional function evaluations             */
	/* respectively                                                                 */
	/* epsfcn is the assumed precision of the function evaluation, to be            */
	/* set by the user                                                              */
	/* taubnd: amount by which bound constraints may be violated during             */
	/* finite differencing, set by the user                                         */

	  LOGICAL   bloc, valid, corr;
	  INTEGER   difftype;
	  DOUBLE    *xtr, *xsc, *fu, **fugrad,
		 **fud, epsfcn, taubnd;

	
	  /* **************************************************************************** */
	  /*                                  o8gene.h                                    */
	  /* **************************************************************************** */
	INTEGER  qpterm, fcount, qpitma;

	INTEGER  ndual, mi, me, iq;
	DOUBLE   *np, rnorm, rlow, **xj,
		*ddual, **r, *ud,
		*ud1;

	INTEGER  iptr, iqtr, *aitr;
	DOUBLE   sstr, riitr;

	// OPSolve.c
	INTEGER nIter;
	INTEGER nFuncEval;
	INTEGER nGradFuncEval;
	INTEGER nConstrEval;
	INTEGER nConstrTotalEval;
	INTEGER nGradConstrEval;
	INTEGER nGradConstrTotalEval;

	TOPSLV1_INITDATA InitData;

	TOPSLV1_PROBLEM Problem;
	// POPSLV1_INITDATA pInitData = NULL;
	void *Context;

	void *pOPSLV_1_MULTI_CONTEXT;


	// errinf.h

	ERRCODE OPSlvErrorCode;
	ERRINFO OPSlvErrorInfo;
	WARNCODE OPSlvWarningCode;
	WARNINFO OPSlvWarningInfo;
	char ErrorText[256];
	char WarningText[256];

} TLContext;

static const DOUBLE zero = 0.e0, one = 1.e0, two = 2.e0, three = 3.e0, four = 4.e0,
five = 5.e0, six = 6.e0, seven = 7.e0,
twom2 = .25e0, twop4 = 16.e0, twop11 = 2048.e0, onep3 = 1.3e0,
onep5 = 1.5e0, p2 = .2e0, p4 = .4e0, p5 = .5e0, p7 = .7e0, p8 = .8e0, p9 = .9e0,
c45 = 45.e0, tm12 = 1.e-12,
tm10 = 1.e-10, tm9 = 1.e-9, tm8 = 1.e-8, tm7 = 1.e-7, tm6 = 1.e-6, tm5 = 1.e-5,
tm4 = 1.e-4, tm3 = 1.e-3, tm2 = 1.e-2, tm1 = 1.e-1, tp1 = 1.e1, tp2 = 1.e2,
tp3 = 1.e3, tp4 = 1.e4;

DOUBLE* d1_malloc(INTEGER size1, INTEGER init, TLContext *ctx);
void eval_extern(INTEGER mode);
void user_init_again(TLContext *ctx);
void o8inim(TLContext *ctx);
void o8dird(TLContext *ctx);
void o8cutd(TLContext *ctx);
void o8smax(TLContext *ctx);
void o8unim(DOUBLE sig1th, TLContext *ctx);
DOUBLE o8sc1(INTEGER i, INTEGER j, DOUBLE a[], DOUBLE b[]);
DOUBLE o8sc2(INTEGER n, INTEGER m, INTEGER j, DOUBLE **a, DOUBLE b[]);
DOUBLE o8sc3(INTEGER n, INTEGER m, INTEGER j, DOUBLE **a, DOUBLE b[]);
DOUBLE o8sc4(INTEGER n, INTEGER m, INTEGER j, DOUBLE **a);
DOUBLE o8sc3_(INTEGER n, INTEGER m, INTEGER j, DOUBLE **a, DOUBLE b[]);
void o8mdru_(DOUBLE **a, INTEGER ma, INTEGER na, char head[],
	FILE *lognum, LOGICAL fix);
void o8egph(DOUBLE gphi[], TLContext *ctx);
void o8dec(INTEGER nlow, INTEGER nrl, TLContext *ctx);
void o8ht(INTEGER id, INTEGER incr, INTEGER is1, INTEGER is2, INTEGER m,
	DOUBLE **a, DOUBLE beta[], DOUBLE b[], DOUBLE c[]);
void o8sol(INTEGER nlow, INTEGER nup, DOUBLE b[], DOUBLE x[], TLContext *ctx);
void o8solt(INTEGER nlow, INTEGER nup, DOUBLE b[], DOUBLE x[], TLContext *ctx);
void o8upd(DOUBLE **r, DOUBLE z[], DOUBLE y[], INTEGER n, LOGICAL *fail, TLContext *ctx);
void o8rght(DOUBLE **a, DOUBLE b[], DOUBLE y[], DOUBLE *yl, INTEGER n);
void o8left(DOUBLE **a, DOUBLE b[], DOUBLE y[], DOUBLE *yl, INTEGER n);
void o8qpdu(TLContext *ctx);
void o8zup(DOUBLE z[], TLContext *ctx);
void o8rup(DOUBLE rv[], TLContext *ctx);
void o8dlcd(INTEGER ai[], INTEGER l, TLContext *ctx);
void o8adcd(TLContext *ctx);
void o8rinv(INTEGER n, DOUBLE **a, INTEGER ndual, DOUBLE **x);
void esf(DOUBLE x[], DOUBLE *fx, TLContext *ctx);
void escon(INTEGER call_type, INTEGER liste[], DOUBLE x[], DOUBLE constr[],
	LOGICAL errliste[], TLContext *ctx);
void  escongrad(INTEGER liste[], INTEGER shift, DOUBLE x[],
	DOUBLE **grad_constr, TLContext *ctx);

void    o8st(TLContext *ctx);
void    o8fin(TLContext *ctx);
void    o8opti(TLContext *ctx);
DOUBLE  o8vecn(INTEGER nl, INTEGER nm, DOUBLE x[]);
void    esgradf(DOUBLE x[], DOUBLE gradf[], TLContext *ctx);
void global_mem_malloc(TLContext *ctx);
void global_mem_free(TLContext *ctx);

DOUBLE  o8sc3b(INTEGER n, INTEGER m, INTEGER j, DOUBLE **a, DOUBLE b[]);

void    setup(TLContext *ctx);
void user_init_size(TLContext *ctx);
void user_init(TLContext *ctx);
void    o8msg(INTEGER num, TLContext *ctx);
void    o8elim(TLContext *ctx);

void    solchk(TLContext *ctx);

void donlp2(TLContext *ctx);
void donlp2_multi_1_init(TLContext *ctx);
void donlp2_multi_1_go(TLContext *ctx);
void donlp2_multi_1_done(TLContext *ctx);

// user_eval.c
void user_eval(DOUBLE xvar[], INTEGER mode, TLContext *ctx);

// OPSolve.c
void ef(DOUBLE x[], DOUBLE *fx, TLContext *ctx);
void egradf(DOUBLE x[], DOUBLE gradf[], TLContext *ctx);
void econ(INTEGER type, INTEGER liste[], DOUBLE x[], DOUBLE con[],
	LOGICAL err[], TLContext *ctx);
void econgrad(INTEGER liste[], INTEGER shift, DOUBLE x[],
	DOUBLE **grad, TLContext *ctx);
void eval_extern(INTEGER mode);
void newx(DOUBLE x[], DOUBLE u[], INTEGER itstep, DOUBLE **accinf,
	LOGICAL *cont, TLContext *ctx);