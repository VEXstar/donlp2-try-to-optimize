#ifndef _ERRCOD_
#define _ERRCOD_

#define ON_ERR if (ctx->OPSlvErrorCode) return

typedef int ERRCODE;
typedef int WARNCODE;
typedef int ERRINFO;
typedef int WARNINFO;

#define OPSLV1_ERR_SUCCESS                      0
#define OPSLV1_ERR_INVALID_PARAM               -1
  #define OPSLV1_INV_PAR_UNKNOWN        0
  #define OPSLV1_INV_PAR_PPROBLEM       1
  #define OPSLV1_INV_PAR_INIT           2
  #define OPSLV1_INV_PAR_FUNC           3
  #define OPSLV1_INV_PAR_GRADFUNC       4
  #define OPSLV1_INV_PAR_GRADCONSTRFUNC 5
  #define OPSLV1_INV_PAR_DIMENSION      6
  #define OPSLV1_INV_PAR_NLIN           7
  #define OPSLV1_INV_PAR_NONLIN         8
  #define OPSLV1_INV_PAR_MAXITER        9
  #define OPSLV1_INV_PAR_NSTEP          10
  #define OPSLV1_INV_PAR_EPSDIF         11
  #define OPSLV1_INV_PAR_EPSFCN         12
  #define OPSLV1_INV_PAR_TAUBND         13
  #define OPSLV1_INV_PAR_DIFFTYPE       14
  #define OPSLV1_INV_PAR_NRESET         15
  #define OPSLV1_INV_PAR_DEL0           16
  #define OPSLV1_INV_PAR_TAU0           17
  #define OPSLV1_INV_PAR_TAU            18
  #define OPSLV1_INV_PAR_BIG            19
  #define OPSLV1_INV_PAR_X              20
  #define OPSLV1_INV_PAR_LOW            21
  #define OPSLV1_INV_PAR_UP             22
  #define OPSLV1_INV_PAR_GRES           23
  #define OPSLV1_INV_PAR_GRES_INSUFF    24
#define OPSLV1_ERR_NO_MEMORY                   -2

#define OPSLV1_ERR_INV_USER_EVAL_MODE          -3
#define OPSLV1_ERR_SCAL_VAR_IS_0               -4
#define OPSLV1_ERR_TAUBND_LE_0                 -5
#define OPSLV1_ERR_SETUP_CHANGED_XSC           -6
#define OPSLV1_ERR_LOW_UP_BOUNDS_BINDING       -7
#define OPSLV1_ERR_BLOC_CALL_FUNC_INFO_INVALID -8
#define OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF   -9
#define OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL      -10

#define OPSLV1_MULTI_ERR_INVALID_PARAM -1001
  #define OPSLV1_MULTI_INV_PAR_UNKNOWN          0
  #define OPSLV1_MULTI_INV_PAR_CONTEXT_NONEMPTY 1
  #define OPSLV1_MULTI_INV_PAR_CONTEXT_EMPTY    2
  #define OPSLV1_MULTI_INV_PAR_CONTEXT_NOTEQ    3
  #define OPSLV1_MULTI_INV_PAR_INITAGAIN        4

#define OPSLV1_WARN_NOWARNING 0
#define OPSLV1_WARN_FREE_MEMORY -2

#endif _ERRCOD_
