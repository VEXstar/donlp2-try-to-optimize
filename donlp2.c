/* Conditions of use:                                                        */
/* 1. donlp2 is under the exclusive copyright of P. Spellucci                */
/*    (e-mail:spellucci@mathematik.tu-darmstadt.de)                          */
/*    "donlp2" is a reserved name                                            */
/* 2. donlp2 and its constituent parts come with no warranty, whether ex-    */
/*    pressed or implied, that it is free of errors or suitable for any      */
/*    specific purpose.                                                      */
/*    It must not be used to solve any problem, whose incorrect solution     */
/*    could result in injury to a person , institution or property.          */
/*    It is at the users own risk to use donlp2 or parts of it and the       */
/*    author disclaims all liability for such use.                           */
/* 3. donlp2 is distributed "as is". In particular, no maintenance, support  */
/*    or trouble-shooting or subsequent upgrade is implied.                  */
/* 4. The use of donlp2 must be acknowledged, in any publication which       */
/*    contains                                                               */
/*    results obtained with it or parts of it. Citation of the authors name  */
/*    and netlib-source is suitable.                                         */
/* 5. The free use of donlp2 and parts of it is restricted for research      */
/*    purposes                                                               */
/*    commercial uses require permission and licensing from P. Spellucci.    */

/* d o n l p  2                                                              */

/*    version 28/11/2001 (*)                                                 */
/*    tauqp dependent on scalres only                                        */
/*    weights computed in a modified version in the singular case            */
/*    some comparisons relative to zero changed                              */
/*    error-return for function evaluation as added feature                  */
/*    termination of QP-solver changed (d not set to zero)                   */
/*    new version of BFGS: if nr  =  0 take Powell's update                  */
/*    no suppression of update beforehand (with exception dg = 0)            */
/*    plus some minor corrections                                            */

/*    for consistency reasons variable names cf and cgf changed into         */
/*    icf and icgf                                                           */
/*    added feature numerical differentiation of order 1,2,6                 */
/*    requires new parameters epsfcn, taubnd, difftype                       */
/*    added feature of external blockwise evaluation of functions            */
/*    rather than individual evaluation                                      */
/*    requires new parameter bloc                                            */
/*    new feature of user-supplied scaling of x                              */

/* ************************************************************************* */
/* (*) using a c-version of donlp2 as of 1998 made by                        */
/*     by S. Schoeffert, ASB, Bourges, France                                */
/*     dynamic memory allocation added by k.s. Cover                         */
/*     copyrights and right of commercial exploitation remaining by contract */
/*     with P. Spellucci                                                     */
/* ************************************************************************  */
/*  new function format                                                      */
/*  developed in 2002/2003 by P. Spellucci copyrigth P. Spellucci            */
/*  The problem format here is                                               */
/*     min_x f(x)                                                            */
/*                                                                           */
/*     subject to                                                            */
/*       low(i) <= x(i)   <=   up(i) , i=1,..,n                              */
/*     low(i+n) <= (Ax)_i <= up(i+n) , i=1,nlin     nlin=0 is allowed        */
/*low(i+nlin+n) <= c_i(x) <= up(i+n+nlin)  i=1,nonlin, nonlin=0 allowe       */
/*                                                                           */
/*                                                                           */

/*  low(i)=up(i) is allowed .   In this case only one constraint is used     */
/*  low(i)=-big, up(i)=big is allowed, big is userdefined                    */
/*  these are a total of 2*(n+nlin+nonlin) constraints, numbered             */
/*  consecutively                                                            */
/*  such that odd numbers correspond to the lower and even numbered to the   */
/*  upper                                                                    */
/*  bound.  A is to be stored in the nlin first columns of gres.             */
/*  the user evaluation routines for the constraints only deal with the      */
/*  nonlinear part and the objective function.                               */
/*  gradients of the bound constraints are never explicitly stored .         */
/*  the relevant parts of the code have been changed to reflect this.        */
/*  the parameters                                                           */
/*   n,nlin,nonlin,big                                                       */
/*   low                                                                     */
/*   up                                                                      */
/*   A  = gres[][i],   i=1,...,nlin                                          */
/*   are to be set (or read from a file) in the basic routine user_init      */
/*   the parameters tau0, del0, delmin etc keep there meaning and usage.     */
/*                                                                           */
/*  A new form of evaluation interface is used for the gradients:            */
/*  only the gradients of the nonlinear constraints must be evaluated,       */
/*  for a given list of indizes, that is grad_c_i , i in list.               */
/*  Also the nonlinear functions c_i(x) are evaluated blockwise              */
/*  bug fix: mes-file written before opened (correction of initial value)    */
/*  bug fix: dead loop in o8qpdu in case of degenerate and illconditioned    */
/*      primal solution                                                      */
/*   bug fix:   log(b2n) now only for b2n>0                                  */
/*   bug fix in user_eval.c: nres replaced by nonlin                         */
/*  fix of a problem with glibc : bind and bind0 renamed to o8bind and o8bind0*/
/*****************************************************************************/

#include <memory.h>
#include "o8comm.h"


/* ARRAY_BORDER_CHECK: Check array borders for indices writing off
   the end of arrays.  Turn off by commenting out the line
   "#define ARRAY_BORDER_CHECK".  It is recommended that  ARRAY_BORDER_CHECK
   be turned off during regular use and  turned on for debugging
   and testing only.  The function "checkArrayBorders" checks if there has been
   any array border violations up to point where it is called.  It is currently
   called at the beginning and end of donlp2 if border checking is turned on.
   To find the location of the first border violation insert additional
   calls to the function narrowing the bracketing on the initial border
   violation occurance.  Additionally, the values of the border counters
   (d_borders_count, i_borders_count, l_borders_count) can be printed
   out at various locations in global_mem_malloc to find out
   which array has been assigned which index. */
/*
#define ARRAY_BORDER_CHECK
*/

#ifdef ARRAY_BORDER_CHECK
#define ABC 1
#else
#define ABC 0
#endif

#ifdef ARRAY_BORDER_CHECK

#define ABC_NUM_1DARRAYS 100000
DOUBLE  *d_borders[2*ABC_NUM_1DARRAYS];
INTEGER *i_borders[2*ABC_NUM_1DARRAYS];
LOGICAL *l_borders[2*ABC_NUM_1DARRAYS];
const DOUBLE  d_unique = 1.23413936727;
const INTEGER i_unique = 972897426;
const LOGICAL l_unique = 953712449;
INTEGER d_borders_count = 0;
INTEGER i_borders_count = 0;
INTEGER l_borders_count = 0;
extern void checkArrayBorders(char *str);
#endif

void donlp2(TLContext *ctx) {


	DOUBLE   term ;
	INTEGER  i,j,k,iz;
	char     fil[13],xxx[9] = "xxxxxxxx",name1[9];

    ctx->OPSlvErrorCode = 0;
    ctx->OPSlvErrorInfo = 0;
    ctx->OPSlvWarningCode = 0;
    ctx->OPSlvWarningInfo = 0;
    strcpy(ctx->ErrorText,"");
    strcpy(ctx->WarningText,"");

/* --------------->  step one of problem initialization by user               */
    /*  user_init_size must initialize  n, nlin, nonlin, iterma, nstep.       */
    /*  These values may not be changed.                                      */

    user_init_size(ctx);
    ctx->ndualm = 2* ctx->n+ ctx->nlin+ ctx->nonlin;
	ctx->mdualm = 2*(ctx->n+ ctx->nlin+ ctx->nonlin);

    /* allocate the global memory */

    global_mem_malloc(ctx); ON_ERR;
#ifdef ARRAY_BORDER_CHECK
    checkArrayBorders("donlp2: after global_mem_malloc");
#endif

    /* default settings of new parameters */

	ctx->bloc     = FALSE;
	ctx->analyt   = TRUE;
	ctx->valid    = FALSE;
	ctx->epsfcn   = 1.e-16;
	ctx->difftype = 3;
	ctx->taubnd   = 1.;
    for (i = 1 ; i <= ctx->n ; i++) {
		ctx->xsc[i] = one;
		ctx->xtr[i] = zero;
    }
	ctx->epsdif = tm8;
    for (i = 0 ; i <= ctx->nlin+ ctx->nonlin ; i++) {
		ctx->val[i]    = FALSE;
        if ( i > 0 ) ctx->gresn[i] = one;
    }
	ctx->ffuerr = FALSE;

    /* some standard initialization which may be overwritten by */
    /* user_init or setup                                       */

	ctx->silent = FALSE;

    /* the interactive input feature is no longer supported here. for the sake */
    /* of easy revision the variable is supplied here however                  */
    /* if intakt is set TRUE, output to protocol file is copied to stdout in   */
    /* addition                                                                */

	ctx->intakt = FALSE;
	ctx->te0    = TRUE;   /* control running optimizer on stdout */
	ctx->te1    = FALSE;   /* information about iteration in compressed form */
	ctx->te2    = FALSE;   /* detailed output in case of trouble */
	ctx->te3    = FALSE;   /* print gradients and Hessian        */
	ctx->cold   = TRUE;
	ctx->big    = 1.e20 ;

/* ->   here user initialization continued                                    */


    /*  user_init must initialize analyt, epsdif, del0, tau0 ,                */
    /*  low, up, and the gradients of the linear constraints, if any          */
    /*  (stored in gres[1:n][j], j=1,nlin)
    /*  bloc                                                                  */
    /*  analyt and if analyt = FALSE then also epsfcn , taubnd , viobnd ,     */
    /*  difftype                                                              */
    /*  and the initial value for x                                           */
    /* may also change settings of all variables initialized above            */

    user_init(ctx);

/*   open files for output if wanted                                          */

    j = 0;
    while (ctx->name[j] == ' ' ) {
        j = j+1;
    }
    if (ctx->name[j] == '\0' ) {
        strcpy(name1,xxx);
    } else {
        k = j+1;
        while (ctx->name[k] != ' ' && ctx->name[k] != '\0' && k-j < 8 ) {
            k = k+1;
        }
        strncpy(name1,&ctx->name[j],k-j);
        name1[k-j] = '\0';

        for (i = 0 ; i <= k-j-1 ; i++) {
            iz = name1[i];
            if ( iz < 48 || ( iz > 57 && iz < 65 )
            || ( iz > 90 && iz < 97 ) || iz > 122 ) name1[i] = 'x';
        }
        if ( k - j < 8 ) strncat(name1,xxx,8-k+j);
    }
    if ( !ctx->silent )
    {
        strcpy(fil,name1);
		ctx->meu  = fopen(strcat(fil,".mes"),"w");
        strcpy(fil,name1);
		ctx->prou = fopen(strcat(fil,".pro"),"w");
    }




/* ************************************************************************** */
/* ***** automatic correction of del0                                         */
/******************************************************************************/

    for ( i = 1 ; i <= ctx->n ; i++ )
    {
        if (ctx->xsc[i] == zero )
        {
		    ctx->OPSlvErrorCode = OPSLV1_ERR_SCAL_VAR_IS_0;
		    ctx->OPSlvErrorInfo = i;
		    sprintf(ctx->ErrorText,"scaling variable %i is zero\n",i);
			global_mem_free(ctx);
			return;
            /*fprintf(stderr,"scaling variable %i is zero\n",i);
            exit(1);*/
        }
    }
	ctx->nres = ctx->n+ ctx->nlin+ ctx->nonlin ;
  for( i = 1 ; i <= ctx->n; i++){
       term = ctx->xsc[i] ;
       if (! (ctx->low[i] == ctx->up[i]) )
       {
         ctx->del0 = min (ctx->del0 , (ctx->up[i]- ctx->low[i])/(four*term) );
       }
  }
    for ( i = ctx->n+1 ; i <= ctx->nres ; i++ )
    {
         term = one ;
         if (! (ctx->low[i] == ctx->up[i]) )
         {
           ctx->del0 = min (ctx->del0 , (ctx->up[i]- ctx->low[i])/(four*term) );
         }
    }
	ctx->delmin = tm6;

    for (i = 1 ; i <= ctx->n ; i++)
    {

		ctx->ug[i] = ctx->low[i];
    ctx->llow[i] = !(ctx->ug[i] <= -ctx->big);

		ctx->og[i] = ctx->up[i];
    ctx->lup[i] = !(ctx->og[i] >= ctx->big);

    }

    for (i=1; i<= ctx->n; i++)
    {
       if (ctx->x[i] < ctx->low[i] || ctx->x[i] > ctx->up[i] )
       {
		   ctx->corr = TRUE ;
          if(ctx->llow[i]){
            if(ctx->lup[i])
              ctx->x[i] = (ctx->up[i]+ ctx->low[i])/2.0;
            else
              ctx->x[i] = ctx->low[i]+2.0*ctx->del0;
          }
          else if(ctx->lup[i]){
            ctx->x[i] = ctx->up[i]-2.0*ctx->del0;
          }
      //    if (ctx->llow[i] && ctx->lup[i]  ) ctx->x[i] = (ctx->up[i]+ ctx->low[i])/2.0;
      //    if (ctx->llow[i] && !ctx->lup[i] ) ctx->x[i] = ctx->low[i]+2.0*ctx->del0;
      //    if (ctx->lup[i]  && !ctx->llow[i]) ctx->x[i] = ctx->up[i]-2.0*ctx->del0;
       }
    }

    if ( ctx->corr && ! ctx->silent ) o8msg(13, ctx);

    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->xst[i] = ctx->x[i];
        ctx->x[i] = ctx->x[i]/ctx->xsc[i];
        ctx->o8opti_yy[i] = ctx->xsc[i];

        /* ug and og have been evaluted for the original variables */
        /* here we use them internally for the scaled ones         */
        /* ug = low up to scaling etc                              */
        ctx->ug[i] = ctx->ug[i]/ctx->xsc[i];
        ctx->og[i] = ctx->og[i]/ctx->xsc[i];

    }
    for ( i = 1 ; i <= ctx->nlin ; i++ )
    {
 /*   rescale the gradients of the linear constraints */
      ctx->gresn[i] = zero ;
      for ( j = 1 ; j <= ctx->n ; j++ )
      {
          ctx->gres[j][i] = ctx->xsc[j]*ctx->gres[j][i] ;
          ctx->gresn[i] += pow(ctx->gres[j][i],2);
      }
      ctx->gresn[i] = max ( one , sqrt(ctx->gresn[i]) ) ;
      ctx->cgres[i] = 1 ;
      ctx->val[i]   = TRUE ;
    }
/*    the sign of the gradient is determined from the current constraint */
/*    and stored in gres[0][i]. done afterwards                          */
    ctx->nreset = ctx->n;

    o8st(ctx); if (ctx->OPSlvErrorCode) { global_mem_free(ctx); return; }

    /* setup may change standard settings of parameters  */
    /* and add some computations in the user environment */
    ctx->qpitma = ctx->nres  ;
    setup(ctx);

    if ( ctx->taubnd <= 0 ) {
	  ctx->OPSlvErrorCode = OPSLV1_ERR_TAUBND_LE_0;
	  //OPSlvErrorInfo = ctx->taubnd;
      sprintf(ctx->ErrorText,"ctx->taubnd le zero is not allowed ");
	  global_mem_free(ctx);
	  return;
      /*fprintf(stderr,"ctx->taubnd le zero is not allowed ");
      exit(1);*/
    }
    for ( i = 1 ; i<= ctx->n ; i++ ) {
      if ( ctx->o8opti_yy[i] != ctx->xsc[i] ) {
	    ctx->OPSlvErrorCode = OPSLV1_ERR_SETUP_CHANGED_XSC;
	    ctx->OPSlvErrorInfo = i;
        sprintf(ctx->ErrorText,"setup has changed xsc, not allowed");
		global_mem_free(ctx);
  	    return;
        /*fprintf(stderr,"setup has changed xsc, not allowed");
        exit(1);*/
      }
    }
    /* preevaluation of gradients of linear functions    */
    /* done now in user_init                             */


    ctx->runtim = clock();
    /* check for redundant linear equality constraints   */
    o8elim(ctx);



    /* call the optimizer */

    o8opti(ctx); if (ctx->OPSlvErrorCode) { global_mem_free(ctx); return; }

    ctx->runtim = (clock()-ctx->runtim)/CLOCKS_PER_SEC;

    /* do final solution check and output */

    o8fin(ctx);


#ifdef ARRAY_BORDER_CHECK
    checkArrayBorders("donlp2: before global_mem_free");
#endif

    /* free up memory */

    global_mem_free(ctx); ON_ERR;

    return;
}

void donlp2_multi_1_init(TLContext *ctx) {

     DOUBLE   term ;
     INTEGER  i,j,k,iz;
     char     fil[13],xxx[9] = "xxxxxxxx",name1[9];

    ctx->OPSlvErrorCode = 0;
    ctx->OPSlvErrorInfo = 0;
    ctx->OPSlvWarningCode = 0;
    ctx->OPSlvWarningInfo = 0;
    strcpy(ctx->ErrorText,"");
    strcpy(ctx->WarningText,"");

/* --------------->  step one of problem initialization by user               */
    /*  user_init_size must initialize  n, nlin, nonlin, iterma, nstep.       */
    /*  These values may not be changed.                                      */

    user_init_size(ctx);
    ctx->ndualm = 2*ctx->n+ctx->nlin+ctx->nonlin;
    ctx->mdualm = 2*(ctx->n+ctx->nlin+ctx->nonlin);

    /* allocate the global memory */

    global_mem_malloc(ctx); ON_ERR;
#ifdef ARRAY_BORDER_CHECK
    checkArrayBorders("donlp2: after global_mem_malloc");
#endif

    /* default settings of new parameters */

    ctx->bloc     = FALSE;
    ctx->analyt   = TRUE;
    ctx->valid    = FALSE;
    ctx->epsfcn   = 1.e-16;
    ctx->difftype = 3;
    ctx->taubnd   = 1.;
    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->xsc[i] = one;
        ctx->xtr[i] = zero;
    }
    ctx->epsdif = tm8;
    for (i = 0 ; i <= ctx->nlin+ctx->nonlin ; i++) {
        ctx->val[i]    = FALSE;
        if ( i > 0 ) ctx->gresn[i] = one;
    }
    ctx->ffuerr = FALSE;

    /* some standard initialization which may be overwritten by */
    /* user_init or setup                                       */

    ctx->silent = FALSE;

    /* the interactive input feature is no longer supported here. for the sake */
    /* of easy revision the variable is supplied here however                  */
    /* if intakt is set TRUE, output to protocol file is copied to stdout in   */
    /* addition                                                                */

    ctx->intakt = FALSE;
    ctx->te0    = TRUE;   /* control running optimizer on stdout */
    ctx->te1    = FALSE;   /* information about iteration in compressed form */
    ctx->te2    = FALSE;   /* detailed output in case of trouble */
    ctx->te3    = FALSE;   /* print gradients and Hessian        */
    ctx->cold   = TRUE;
    ctx->big    = 1.e20 ;

/* ->   here user initialization continued                                    */


    /*  user_init must initialize analyt, epsdif, del0, tau0 ,                */
    /*  low, up, and the gradients of the linear constraints, if any          */
    /*  (stored in gres[1:n][j], j=1,nlin)
    /*  bloc                                                                  */
    /*  analyt and if analyt = FALSE then also epsfcn , taubnd , viobnd ,     */
    /*  difftype                                                              */
    /*  and the initial value for x                                           */
    /* may also change settings of all variables initialized above            */

    user_init(ctx);

/*   open files for output if wanted                                          */

    j = 0;
    while ( ctx->name[j] == ' ' ) {
        j = j+1;
    }
    if ( ctx->name[j] == '\0' ) {
        strcpy(name1,xxx);
    } else {
        k = j+1;
        while ( ctx->name[k] != ' ' && ctx->name[k] != '\0' && k-j < 8 ) {
            k = k+1;
        }
        strncpy(name1,&ctx->name[j],k-j);
        name1[k-j] = '\0';

        for (i = 0 ; i <= k-j-1 ; i++) {
            iz = name1[i];
            if ( iz < 48 || ( iz > 57 && iz < 65 )
            || ( iz > 90 && iz < 97 ) || iz > 122 ) name1[i] = 'x';
        }
        if ( k - j < 8 ) strncat(name1,xxx,8-k+j);
    }
    if ( ! ctx->silent )
    {
        strcpy(fil,name1);
        ctx->meu  = fopen(strcat(fil,".mes"),"w");
        strcpy(fil,name1);
        ctx->prou = fopen(strcat(fil,".pro"),"w");
    }




/* ************************************************************************** */
/* ***** automatic correction of del0                                         */
/******************************************************************************/

    for ( i = 1 ; i <= ctx->n ; i++ )
    {
        if ( ctx->xsc[i] == zero )
        {
		    ctx->OPSlvErrorCode = OPSLV1_ERR_SCAL_VAR_IS_0;
		    ctx->OPSlvErrorInfo = i;
		    sprintf(ctx->ErrorText,"scaling variable %i is zero\n",i);
			global_mem_free(ctx);
			return;
            /*fprintf(stderr,"scaling variable %i is zero\n",i);
            exit(1);*/
        }
    }
    ctx->nres = ctx->n+ctx->nlin+ctx->nonlin ;
    for ( i = 1 ; i <= ctx->nres ; i++ )
    {
       if ( i <= ctx->n )
       {
         term = ctx->xsc[i] ;
       }
       else
       {
         term = one ;
       }
       if (! (ctx->low[i] == ctx->up[i]) )
       {
          ctx->del0 = min ( ctx->del0 , (ctx->up[i]-ctx->low[i])/(four*term) );
       }
    }
    ctx->delmin = tm6;

    for (i = 1 ; i <= ctx->n ; i++)
    {

        ctx->ug[i] = ctx->low[i];
        if ( ctx->ug[i] <= -ctx->big )
        {
           ctx->llow[i] = FALSE ;
        }
        else
        {
           ctx->llow[i] = TRUE ;
        }
        ctx->og[i] = ctx->up[i];
        if ( ctx->og[i] >= ctx->big )
        {
           ctx->lup[i] = FALSE ;
        }
        else
        {
           ctx->lup[i] = TRUE ;
        }

    }

    for (i=1; i<=ctx->n; i++)
    {
       if ( ctx->x[i] < ctx->low[i] || ctx->x[i] > ctx->up[i] )
       {
          ctx->corr = TRUE ;
          if ( ctx->llow[i] && ctx->lup[i]  ) ctx->x[i] = (ctx->up[i]+ctx->low[i])/2.0;
          if ( ctx->llow[i] && !ctx->lup[i] ) ctx->x[i] = ctx->low[i]+2.0*ctx->del0;
          if ( ctx->lup[i]  && !ctx->llow[i]) ctx->x[i] = ctx->up[i]-2.0*ctx->del0;
       }
    }

    if ( ctx->corr && ! ctx->silent ) o8msg(13, ctx);

    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->xst[i] = ctx->x[i];
        ctx->x[i] = ctx->x[i]/ctx->xsc[i];
        ctx->o8opti_yy[i] = ctx->xsc[i];

    }
    for (i = 1 ; i <= ctx->n ; i++) {

        /* ug and og have been evaluted for the original variables */
        /* here we use them internally for the scaled ones         */
        /* ug = low up to scaling etc                              */
        ctx->ug[i] = ctx->ug[i]/ctx->xsc[i];
        ctx->og[i] = ctx->og[i]/ctx->xsc[i];
    }
    for ( i = 1 ; i <= ctx->nlin ; i++ )
    {
 /*   rescale the gradients of the linear constraints */
      ctx->gresn[i] = zero ;
      for ( j = 1 ; j <= ctx->n ; j++ )
      {
          ctx->gres[j][i] = ctx->xsc[j]*ctx->gres[j][i] ;
          ctx->gresn[i] += pow(ctx->gres[j][i],2);
      }
      ctx->gresn[i] = max ( one , sqrt(ctx->gresn[i]) ) ;
      ctx->cgres[i] = 1 ;
      ctx->val[i]   = TRUE ;
    }
/*    the sign of the gradient is determined from the current constraint */
/*    and stored in gres[0][i]. done afterwards                          */
    ctx->nreset = ctx->n;

    o8st(ctx); if (ctx->OPSlvErrorCode) { global_mem_free(ctx); return; }

    /* setup may change standard settings of parameters  */
    /* and add some computations in the user environment */
    ctx->qpitma = ctx->nres  ;
    setup(ctx);

    if ( ctx->taubnd <= 0 ) {
	  ctx->OPSlvErrorCode = OPSLV1_ERR_TAUBND_LE_0;
	  //OPSlvErrorInfo = taubnd;
      sprintf(ctx->ErrorText,"taubnd le zero is not allowed ");
	  global_mem_free(ctx);
	  return;
      /*fprintf(stderr,"taubnd le zero is not allowed ");
      exit(1);*/
    }
    for ( i = 1 ; i<= ctx->n ; i++ ) {
      if ( ctx->o8opti_yy[i] != ctx->xsc[i] ) {
	    ctx->OPSlvErrorCode = OPSLV1_ERR_SETUP_CHANGED_XSC;
	    ctx->OPSlvErrorInfo = i;
        sprintf(ctx->ErrorText,"setup has changed xsc, not allowed");
		global_mem_free(ctx);
  	    return;
        /*fprintf(stderr,"setup has changed xsc, not allowed");
        exit(1);*/
      }
    }
    /* preevaluation of gradients of linear functions    */
    /* done now in user_init                             */


    ctx->runtim = clock();
    /* check for redundant linear equality constraints   */
    o8elim(ctx);



    /* call the optimizer */
}

void donlp2_multi_1_go(TLContext *ctx)
{
     DOUBLE   infiny,term;
     INTEGER  i,j;

    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->xsc[i] = one;
        ctx->xtr[i] = zero;
    }
    ctx->epsdif = tm8;
    for (i = 0 ; i <= ctx->nlin+ctx->nonlin ; i++) {
        ctx->val[i]    = FALSE;
        if ( i > 0 ) ctx->gresn[i] = one;
    }
    ctx->ffuerr = FALSE;

/* ************************************************************************** */
/* ***** automatic correction of del0                                         */
/******************************************************************************/

    for ( i = 1 ; i <= ctx->n ; i++ )
    {
        if ( ctx->xsc[i] == zero )
        {
		    ctx->OPSlvErrorCode = OPSLV1_ERR_SCAL_VAR_IS_0;
		    ctx->OPSlvErrorInfo = i;
		    sprintf(ctx->ErrorText,"scaling variable %i is zero\n",i);
			global_mem_free(ctx);
			return;
            /*fprintf(stderr,"scaling variable %i is zero\n",i);
            exit(1);*/
        }
    }
    ctx->nres = ctx->n+ctx->nlin+ctx->nonlin ;
    for ( i = 1 ; i <= ctx->nres ; i++ )
    {
       if ( i <= ctx->n )
       {
         term = ctx->xsc[i] ;
       }
       else
       {
         term = one ;
       }
       if (! (ctx->low[i] == ctx->up[i]) )
       {
          ctx->del0 = min ( ctx->del0 , (ctx->up[i]-ctx->low[i])/(four*term) );
       }
    }
    ctx->delmin = tm6;

    for (i = 1 ; i <= ctx->n ; i++)
    {

        ctx->ug[i] = ctx->low[i];
        if ( ctx->ug[i] <= -ctx->big )
        {
           ctx->llow[i] = FALSE ;
        }
        else
        {
           ctx->llow[i] = TRUE ;
        }
        ctx->og[i] = ctx->up[i];
        if ( ctx->og[i] >= ctx->big )
        {
           ctx->lup[i] = FALSE ;
        }
        else
        {
           ctx->lup[i] = TRUE ;
        }

    }

    for (i=1; i<=ctx->n; i++)
    {
       if ( ctx->x[i] < ctx->low[i] || ctx->x[i] > ctx->up[i] )
       {
          ctx->corr = TRUE ;
          if ( ctx->llow[i] && ctx->lup[i]  ) ctx->x[i] = (ctx->up[i]+ctx->low[i])/2.0;
          if ( ctx->llow[i] && !ctx->lup[i] ) ctx->x[i] = ctx->low[i]+2.0*ctx->del0;
          if ( ctx->lup[i]  && !ctx->llow[i]) ctx->x[i] = ctx->up[i]-2.0*ctx->del0;
       }
    }

    if ( ctx->corr && ! ctx->silent ) o8msg(13, ctx);

    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->xst[i] = ctx->x[i];
        ctx->x[i] = ctx->x[i]/ctx->xsc[i];
        ctx->o8opti_yy[i] = ctx->xsc[i];

    }
    for (i = 1 ; i <= ctx->n ; i++) {

        /* ug and og have been evaluted for the original variables */
        /* here we use them internally for the scaled ones         */
        /* ug = low up to scaling etc                              */
        ctx->ug[i] = ctx->ug[i]/ctx->xsc[i];
        ctx->og[i] = ctx->og[i]/ctx->xsc[i];
    }
    for ( i = 1 ; i <= ctx->nlin ; i++ )
    {
 /*   rescale the gradients of the linear constraints */
      ctx->gresn[i] = zero ;
      for ( j = 1 ; j <= ctx->n ; j++ )
      {
          ctx->gres[j][i] = ctx->xsc[j]*ctx->gres[j][i] ;
          ctx->gresn[i] += pow(ctx->gres[j][i],2);
      }
      ctx->gresn[i] = max ( one , sqrt(ctx->gresn[i]) ) ;
      ctx->cgres[i] = 1 ;
      ctx->val[i]   = TRUE ;
    }
/*    the sign of the gradient is determined from the current constraint */
/*    and stored in gres[0][i]. done afterwards                          */
    ctx->nreset = ctx->n;


    /* standard initialization */

    ctx->lastch = 0;
    ctx->lastdw = 0;
    ctx->lastup = 0;
    ctx->level  = one;
    ctx->tau    = tm1;
    ctx->epsx   = tm5;
    ctx->sigsm  = sqrt(ctx->epsmac);
    ctx->smalld = tm1;

    /* formerly tm2. smalld has much influence on the Maratos-effect */

    ctx->smallw = exp(two*log(ctx->epsmac)/three);
    ctx->rho    = tm6;
    ctx->rho1   = tm10;
    ctx->del01  = ctx->del0/tp1;
    ctx->delmin = min(ctx->del01,max(tm6*ctx->del0,ctx->smallw));
    if ( ! ctx->analyt ) ctx->delmin = min(ctx->del01,max(ctx->epsdif,ctx->delmin));

    ctx->c1d    = tm2;
    ctx->scfmax = tp4;
    ctx->taufac = tp1;
    ctx->taumax = pow(ctx->scfmax,2);
    ctx->updmy0 = tm1;

    /* take del0 and tau0 from  user_init in the user function suite       */
    /* may be modified by subsequent call of setup                         */


    infiny = ctx->epsmac/ctx->tolmac;
    ctx->fx     = zero;
    ctx->b2n    = zero;
    ctx->b2n0   = zero;
    ctx->nres   = ctx->n+ctx->nlin+ctx->nonlin;
/*  but remember there are formally 2*nres constraints */
    if ( ctx->cold )
    {
    /*  initialize the quasi Newton update                 */
      for (i = 1 ; i <= ctx->n ; i++)
      {
         for (j = 1 ; j <= ctx->n ; j++)
         {
            ctx->a[i][j] = zero;
         }
         ctx->a[i][i]  = one;
         ctx->diag0[i] = one;
      }
      ctx->matsc = one ;
    }
      for (i = 1 ; i <= ctx->n ; i++)
      {
        ctx->diag[i] = zero;
      }
      for ( i = 1 ; i <= ctx->nres ; i++ )
      {
        for (j = 1 ; j <= ctx->n ; j++)
        {
          ctx->qr[j][i]   = zero;
        }
      }
      for ( i = ctx->nlin+1 ; i <= ctx->nlin+ctx->nonlin ; i++ )
      {
         for ( j = 1 ; j <= ctx->n ; j++ )
         {
           ctx->gres[j][i] = zero;
         }
         ctx->gres[0][i] = one ;
      }


    /* nonlinear gradients not yet evaluated */
    for ( i = ctx->nlin+1 ; i<=ctx->nlin+ctx->nonlin ; i++ ) ctx->val[i] = FALSE ;

    if ( ctx->bloc )
    {
       ctx->valid = FALSE ;
       /* user_eval must reset valid */
       user_eval(ctx->x,1, ctx); ON_ERR;
    }
    /* this call gives all function and gradient values in fu[] and fugrad[][]*/
    /* later call of escon and escongrad simply gives a move in this case     */


    for ( i = 1 ; i <= 2*ctx->nres ; i ++)
    {
        ctx->o8bind[i]  = 0;
        ctx->o8bind0[i] = 0;
        ctx->u[i]     = zero;
        ctx->u0[i]    = zero;
        /* initial weights of the penalty-term */
        if ( ctx->cold ) ctx->w[i] = one;
    }

    for ( i = 1 ; i <= ctx->nlin+ctx->nonlin ; i++ )
    {
        ctx->cres[i] = 0 ;
        ctx->cgres[i] = 0 ;
    }

    ctx->clow = one;
    ctx->ny   = two;

    /* scf  = weight factor for objective function    */
    /* scf0 = damping factor for tangential direction */

    ctx->scf    = one;
    ctx->scf0   = one;
    ctx->sigla  = twop11;
    ctx->beta   = four;  /* formerly two */
    ctx->alpha  = tm1;
    ctx->delta1 = p9;
    ctx->delta  = tm3;   /* delta = tm2  formerly */
    ctx->theta  = p9;    /* theta = 0.99 formerly */
    ctx->icf    = 0;
    ctx->icgf   = 0;


    ctx->qpitma = ctx->nres  ;

    /* preevaluation of gradients of linear functions    */
    /* done now in user_init                             */


    o8opti(ctx); ON_ERR;

	solchk(ctx);
}

void donlp2_multi_1_done(TLContext *ctx)
{
    ctx->runtim = (clock()-ctx->runtim)/CLOCKS_PER_SEC;

    /* do final solution check and output */

    //o8fin();

#ifdef ARRAY_BORDER_CHECK
    checkArrayBorders("donlp2: before global_mem_free");
#endif

    /* free up memory */

    global_mem_free(ctx);
}





/* **************************************************************************** */
/*        initialization program , standard parameter settings done here        */
/* **************************************************************************** */

// To ensure that some variables "go through" memory - D.Zaytsev, 24.06.2006
/* BEGIN */
void EnsureVarGoesThroughMemory(DOUBLE *var)
{
	volatile DOUBLE VolatileDouble;
	/* Compiler cannot eliminate assignment to volatile var */
	VolatileDouble = *var;
	*var = VolatileDouble;
}
/* END */

void o8st(TLContext *ctx) {

     INTEGER  i,j,k;
     DOUBLE   tol1 ,bd0,infiny,gxi,hxi,term;
     time_t   tim;

    ctx->epsmac = pow(two,-20);

    L100:

    ctx->epsmac = ctx->epsmac/two;
    term   = one+ctx->epsmac;

    if ( term != one ) goto L100;

    ctx->epsmac = ctx->epsmac+ctx->epsmac;
    ctx->tolmac = ctx->epsmac;

    L200:

    tol1   = ctx->tolmac;
    ctx->tolmac = ctx->tolmac/twop4;

    /* tolmac must be saved to memory and then loaded again! */
	/* the optimizer is too smart, damn it */
	/* it eliminates the following statement: */
	/* tol1 = tol1 == tolmac ? tol1 : tol1; */
	EnsureVarGoesThroughMemory(&ctx->tolmac);
	/* thus we break optimization - D.Zaytsev, 28.02.2006 */

    if ( ctx->tolmac != zero ) goto L200;

    ctx->tolmac = tol1;

    /* epsmac machine precision, tolmac smallest machine number */
    /* larger than zero (approximately , base 16 for exponent   */
    /* therefore division by 16 assumed)                        */

    /*                        ***** warning *****                        */
    /* on some machines the computation of tolmac may result in an error */
    /* because underflow is not accepted as zero as is assumed here      */

    /* warning: the computation of epsmac may give faulty results if     */
    /* compiler uses value residing in register instead of the one stored */
    /* back to memory. with gcc use the switch  -ffloat-store in order */
    /* to prevent this                                                 */

    if ( ctx->tau0 == zero ) ctx->tau0 = one;
    if ( ctx->del0 == zero ) ctx->del0 = ctx->tau0*p5;

    if ( ctx->nreset > ctx->n  ) ctx->nreset = ctx->n;
    if ( ctx->nreset <= 4 ) ctx->nreset = 4;

    /* standard initialization */

    ctx->lastch = 0;
    ctx->lastdw = 0;
    ctx->lastup = 0;
    ctx->level  = one;
    ctx->tau    = tm1;
    ctx->epsx   = tm5;
    ctx->sigsm  = sqrt(ctx->epsmac);
    ctx->smalld = tm1;

    /* formerly tm2. smalld has much influence on the Maratos-effect */

    ctx->smallw = exp(two*log(ctx->epsmac)/three);
    ctx->rho    = tm6;
    ctx->rho1   = tm10;
    ctx->del01  = ctx->del0/tp1;
    ctx->delmin = min(ctx->del01,max(tm6*ctx->del0,ctx->smallw));
    if ( ! ctx->analyt ) ctx->delmin = min(ctx->del01,max(ctx->epsdif,ctx->delmin));

    ctx->c1d    = tm2;
    ctx->scfmax = tp4;
    ctx->taufac = tp1;
    ctx->taumax = pow(ctx->scfmax,2);
    ctx->updmy0 = tm1;

    /* take del0 and tau0 from  user_init in the user function suite       */
    /* may be modified by subsequent call of setup                         */


    infiny = ctx->epsmac/ctx->tolmac;
    ctx->fx     = zero;
    ctx->b2n    = zero;
    ctx->b2n0   = zero;
    ctx->nres   = ctx->n+ctx->nlin+ctx->nonlin;
/*  but remember there are formally 2*nres constraints */
    if ( ctx->cold )
    {
    /*  initialize the quasi Newton update                 */
      for (i = 1 ; i <= ctx->n ; i++)
      {
         for (j = 1 ; j <= ctx->n ; j++)
         {
            ctx->a[i][j] = zero;
         }
         ctx->a[i][i]  = one;
         ctx->diag0[i] = one;
      }
      ctx->matsc = one ;
    }
      for (i = 1 ; i <= ctx->n ; i++)
      {
        ctx->diag[i] = zero;
      }
      for ( i = 1 ; i <= ctx->nres ; i++ )
      {
        for (j = 1 ; j <= ctx->n ; j++)
        {
          ctx->qr[j][i]   = zero;
        }
      }
      for ( i = ctx->nlin+1 ; i <= ctx->nlin+ctx->nonlin ; i++ )
      {
         for ( j = 1 ; j <= ctx->n ; j++ )
         {
           ctx->gres[j][i] = zero;
         }
         ctx->gres[0][i] = one ;
      }


    /* nonlinear gradients not yet evaluated */
    for ( i = ctx->nlin+1 ; i<=ctx->nlin+ctx->nonlin ; i++ ) ctx->val[i] = FALSE ;

    if ( ctx->bloc )
    {
       ctx->valid = FALSE ;
       /* user_eval must reset valid */
       user_eval(ctx->x,1, ctx); ON_ERR;
    }
    /* this call gives all function and gradient values in fu[] and fugrad[][]*/
    /* later call of escon and escongrad simply gives a move in this case     */


    for ( i = 1 ; i <= 2*ctx->nres ; i ++)
    {
        ctx->o8bind[i]  = 0;
        ctx->o8bind0[i] = 0;
        ctx->u[i]     = zero;
        ctx->u0[i]    = zero;
        /* initial weights of the penalty-term */
        if ( ctx->cold ) ctx->w[i] = one;
    }

    for ( i = 1 ; i <= ctx->nlin+ctx->nonlin ; i++ )
    {
        ctx->cres[i] = 0 ;
        ctx->cgres[i] = 0 ;
    }

    ctx->clow = one;
    ctx->ny   = two;

    /* scf  = weight factor for objective function    */
    /* scf0 = damping factor for tangential direction */

    ctx->scf    = one;
    ctx->scf0   = one;
    ctx->sigla  = twop11;
    ctx->beta   = four;  /* formerly two */
    ctx->alpha  = tm1;
    ctx->delta1 = p9;
    ctx->delta  = tm3;   /* delta = tm2  formerly */
    ctx->theta  = p9;    /* theta = 0.99 formerly */
    ctx->icf    = 0;
    ctx->icgf   = 0;
    if ( ! ctx->silent ) {
        fprintf(ctx->prou,"donlp2-intv 21/06/04, copyright P. Spellucci\n");

        time(&tim);

        fprintf(ctx->prou,"%s",ctime(&tim));
        fprintf(ctx->prou,"%s\n", ctx->name);

        fprintf(ctx->meu, "donlp2-intv 21/06/04, copyright P. Spellucci\n");
        fprintf(ctx->meu, "%s",ctime(&tim));
        fprintf(ctx->meu, "%s\n", ctx->name);
    }
    return;
}

/* **************************************************************************** */
/*                        do final solution check and output                    */
/* **************************************************************************** */
void o8fin(TLContext *ctx) {

    INTEGER  i,j,k,ih1,ih2,ih3,ih5,ih6,ih7,ih8,ih9;
    DOUBLE   umin,term;
    INTEGER  nsing,crtot,cgrtot,nupreg,nbfgs1,nresta;
    char     line[65];

    /* termination reason + 11 = message number */

    static const char *messag[] = {
    "",     /* not used : index 0 */
    "constraint evaluation returns error with current point",
    "objective evaluation returns error with current point",
    "QPsolver: working set singular in dual extended QP ",
    "QPsolver: extended QP-problem seemingly infeasible ",
    "QPsolver: no descent direction from QP for tau=tau_max",
    "QPsolver: on exit correction small, infeasible point",
    "stepsizeselection: computed d not a direction of descent",
    "more than MAXIT iteration steps",
    "stepsizeselection: no acceptable stepsize in [sigsm,sigla]",
    "stepsizeselection: directional deriv. very small, infeasible",
    "KT-conditions satisfied, no further correction computed",
    "KT-conditions satisfied, computed correction small",
    "stepsizeselection: x (almost) feasible, dir. deriv. very small",
    "KT-conditions (relaxed) satisfied, singular point",
    "very slow primal progress, singular or illconditoned problem",
    "very slow progress in x, singular problem",
    "correction very small, almost feasible but singular point",
    "max(n,10) small differences in penalty function,terminate",
    "user required termination                                "
    };

    if ( ctx->scf != zero ) {
        for (i = 1 ; i <= 2*ctx->nres ; i++) {
            ctx->u[i] = ctx->u[i]/ctx->scf;
        }
    }
    /* in solchk the user might add some additional checks and/or output */

    solchk(ctx);

    if ( ctx->silent && ! ctx->intakt ) return;

    if ( ctx->intakt && ! ctx->silent ) printf("%s\n", ctx->name);

    if ( ! ctx->silent ) {
        if ( ctx->intakt ) {
            printf(  "\n     n= %9i    nlin= %9i    nonlin= %9i\n", ctx->n,ctx->nlin,ctx->nonlin);
            printf(  "\n  epsx= %9.3e sigsm= %9.3e\n"       , ctx->epsx,ctx->sigsm);
            printf(  "\nstartvalue\n");
            for (i = 1 ; i <= ctx->n ; i++) {
                printf(  " %14.7e ", ctx->xst[i]);
                if ( i % 5 == 0 || i == ctx->n ) printf(  "\n");
            }
        }
        fprintf(ctx->prou,"\n     n= %9i    nlin= %9i    nonlin= %9i\n",
          ctx->n,ctx->nlin,ctx->nonlin);
        fprintf(ctx->prou,"\n  epsx= %9.3e sigsm= %9.3e\n"       , ctx->epsx,ctx->sigsm);
        fprintf(ctx->prou,"\nstartvalue\n");
        for (i = 1 ; i <= ctx->n ; i++) {
            fprintf(ctx->prou," %14.7e ", ctx->xst[i]);
            if ( i % 5 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
        }
    }
    if ( ctx->intakt && ! ctx->silent ) {
        printf("\n  eps= %9.2e  tol= %9.2e del0= %9.2e delm= %9.2e tau0= %9.2e\n",
        ctx->epsmac,ctx->tolmac,ctx->del0,ctx->delmin,ctx->tau0);
        printf(  "  tau= %9.2e   sd= %9.2e   sw= %9.2e  rho= %9.2e rho1= %9.2e\n",
        ctx->tau,ctx->smalld,ctx->smallw,ctx->rho,ctx->rho1);
    }
    if ( ! ctx->silent ) {
        fprintf(ctx->prou,
               "\n  eps= %9.2e  tol= %9.2e del0= %9.2e delm= %9.2e tau0= %9.2e\n",
        ctx->epsmac,ctx->tolmac,ctx->del0,ctx->delmin,ctx->tau0);
        fprintf(ctx->prou,
                 "  tau= %9.2e   sd= %9.2e   sw= %9.2e  rho= %9.2e rho1= %9.2e\n",
        ctx->tau,ctx->smalld,ctx->smallw,ctx->rho,ctx->rho1);
    }
    if ( ! ctx->silent ) {
        fprintf(ctx->prou,
        " scfm= %9.2e  c1d= %9.2e epdi= %9.2e\n  nre= %9i anal= %9i\n",
        ctx->scfmax,ctx->c1d,ctx->epsdif,ctx->nreset,ctx->analyt);
        if ( ctx->intakt ) printf(
        " scfm= %9.2e  c1d= %9.2e epdi= %9.2e\n  nre= %9i anal= %9i\n",
        ctx->scfmax,ctx->c1d,ctx->epsdif,ctx->nreset,ctx->analyt);
    }
    if ( ! ctx->silent && ! ctx->analyt ) {
        fprintf(ctx->prou," vbnd= %9.2e efcn= %9.2e diff=%1i\n"
        , ctx->taubnd,ctx->epsfcn,ctx->difftype);
        if ( ctx->intakt ) printf("taubnd= %9.2e epsfcn= %9.2e difftype=%1i\n"
        , ctx->taubnd,ctx->epsfcn,ctx->difftype);
    }
    i    = 0;
    j    = 0;
    umin = zero;
    for (k = 1 ; k <= ctx->nlin+ctx->nonlin ; k++) {
        i = i+ ctx->cres[k];
        j = j+ctx->cgres[k];
    }
    for ( k = 1 ; k <= ctx->nres ; k++ ) {
        if ( ctx->low[k] != ctx->up[k]  ) umin =min( min( umin,ctx->u[2*k-1] ) ,ctx->u[2*k]);
    }
    crtot  = i;
    cgrtot = j;
    nsing  = 0;
    nresta = 0;
    nupreg = 0;
    nbfgs1 = 0;
    for (k = 1 ; k <= ctx->itstep ; k++) {
        if ( ctx->accinf[k][10] == one ) nsing  = nsing+1;
        if ( ctx->accinf[k][27] == one ) nbfgs1 = nbfgs1+1;

        /* for the Pantoja Mayne update */

        if ( ctx->accinf[k][29] == zero && ctx->accinf[k][27] == one ) nupreg = nupreg+1;
        if ( ctx->accinf[k][27] == -one ) nresta = nresta+1;
    }
    k = (int)ctx->optite+11;
    if ( k >= 1 && k <= 18 ) {
        strcpy(line,messag[k]);
    } else {
        strcpy(line,"variable optite undefined on exit");
    }
    if ( ctx->intakt && ! ctx->silent ) {
        printf(      "\n termination reason:\n %s\n",line);
        printf(        " evaluations of f                    %9i\n",    ctx->icf);
        printf(        " evaluations of grad f               %9i\n",    ctx->icgf);
        printf(        " evaluations of constraints          %9i\n",    crtot);
        printf(        " evaluations of grads of constraints %9i\n",    cgrtot);
        printf(        " final scaling of objective          %13.6e\n", ctx->scf);
        printf(        " norm of grad(f)                     %13.6e\n", ctx->gfn);
        printf(        " lagrangian violation                %13.6e\n", ctx->b2n);
        printf(        " feasibility violation               %13.6e\n", ctx->upsi);
        printf(        " dual feasibility violation          %13.6e\n", umin);
        printf(        " optimizer runtime sec's             %13.6e\n", ctx->runtim);
    }
    if ( ! ctx->silent ) {
        fprintf(ctx->prou,"\n termination reason:\n %s\n",line);
        fprintf(ctx->prou,  " evaluations of f                    %9i\n",    ctx->icf);
        fprintf(ctx->prou,  " evaluations of grad f               %9i\n",    ctx->icgf);
        fprintf(ctx->prou,  " evaluations of constraints          %9i\n",    crtot);
        fprintf(ctx->prou,  " evaluations of grads of constraints %9i\n",    cgrtot);
        fprintf(ctx->prou,  " final scaling of objective          %13.6e\n", ctx->scf);
        fprintf(ctx->prou,  " norm of grad(f)                     %13.6e\n", ctx->gfn);
        fprintf(ctx->prou,  " lagrangian violation                %13.6e\n", ctx->b2n);
        fprintf(ctx->prou,  " feasibility violation               %13.6e\n", ctx->upsi);
        fprintf(ctx->prou,  " dual feasibility violation          %13.6e\n", umin);
        fprintf(ctx->prou,  " optimizer runtime sec's             %13.6e\n", ctx->runtim);
    }
    if ( ctx->intakt && ! ctx->silent ) printf("\n\n optimal value of f =  %21.14e\n", ctx->fx);
    if ( ! ctx->silent ) fprintf(ctx->prou,    "\n\n optimal value of f =  %21.14e\n", ctx->fx);

    if ( ctx->intakt && ! ctx->silent ) {
        printf(      "\n optimal solution  x =\n");
        for (i = 1 ; i <= ctx->n ; i++) {
            printf(      " %21.14e", ctx->x[i]);
            if ( i % 3 == 0 || i == ctx->n ) printf(     "\n");
        }
    }
    if ( ! ctx->silent ) {
        fprintf(ctx->prou,"\n optimal solution  x =\n");
        for (i = 1 ; i <= ctx->n ; i++) {
            fprintf(ctx->prou," %21.14e", ctx->x[i]);
            if ( i % 3 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
        }
    }
    if ( ctx->nres != 0 && ! ctx->silent )
    {
        fprintf(ctx->prou,"\n  multipliers are relativ to scf=1\n");
        fprintf(ctx->prou,"  nr.    constraint   multiplier        normgrad (or 1) \n");
        for (i = 1 ; i <= 2*ctx->nres ; i++)
        {
          if ( (i+1)%2 == 0 && i >2*ctx->n )
          {
            fprintf(ctx->prou," %4i  %14.7e   %14.7e   %14.7e\n"
            , i,ctx->res[i],ctx->u[i],ctx->gresn[(i+1)/2-ctx->n]);
          }
          else
          {
            fprintf(ctx->prou," %4i  %14.7e   %14.7e \n"
            , i,ctx->res[i],ctx->u[i]);
          }
        }
        if ( ctx->intakt )
        {
            printf(  "\n  multipliers are relativ to scf=1\n");
            printf(    "  nr.    constraint     multiplier norm(grad) or 1 \n");
            for (i = 1 ; i <= 2*ctx->nres ; i++)
            {
              if ( (i+1)%2 == 0 && i > 2*ctx->n  )
              {
                printf(" %4i  %14.7e   %14.7e   %14.7e\n"
                    , i,ctx->res[i],ctx->u[i],ctx->gresn[(i+1)/2]);
              }
              else
              {
                printf(" %4i  %14.7e   %14.7e \n"
                  , i,ctx->res[i],ctx->u[i]);
              }
            }


        }
    }
    if ( ctx->nres != 0 && ! ctx->silent ) {
        fprintf(ctx->prou,"\n evaluations of restrictions and their gradients\n");
        for (i = 1 ; i <= ctx->nlin+ctx->nonlin ; i++) {
            fprintf(ctx->prou," (%6i,%6i)", ctx->cres[i],ctx->cgres[i]);
            if ( i % 5 == 0 || i == ctx->nres ) fprintf(ctx->prou,"\n");
        }
        if ( ctx->intakt ) {
            printf(  "\n evaluations of restrictions and their gradients\n");
            for (i = 1 ; i <= ctx->nlin+ctx->nonlin ; i++) {
                printf(  " (%6i,%6i)", ctx->cres[i],ctx->cgres[i]);
                if ( i % 5 == 0 || i == ctx->nres ) printf(  "\n");
            }
        }
    }
    if ( ctx->itstep > 1 && ctx->optite == 0 ) ctx->itstep = ctx->itstep-1;
    if ( ! ctx->silent )
    fprintf(ctx->prou,    "\n last estimate of condition of active gradients %10.3e\n",
    ctx->accinf[ctx->itstep][13]);

    term = ctx->accinf[ctx->itstep][14];
    i    = ctx->itstep;
    while ( i > 1 && term == zero ) {
        i    = i-1;
        term = ctx->accinf[i][14];
    }
    if ( ! ctx->silent ) {
        fprintf(ctx->prou,"\n last estimate of condition of approx. hessian  %10.3e\n",
        term);
        fprintf(ctx->prou,"iterative steps total           %5i\n", ctx->itstep);
        fprintf(ctx->prou,"# of restarts                   %5i\n", nresta);
        fprintf(ctx->prou,"# of full regular updates       %5i\n", nupreg);
        fprintf(ctx->prou,"# of updates                    %5i\n", nbfgs1);
        fprintf(ctx->prou,"# of full regularized SQP-steps %5i\n", nsing);

        if ( ctx->intakt ) {
            printf(  "\n last estimate of cond.nr. of active gradients  %10.3e\n",
            ctx->accinf[ctx->itstep][13]);
            printf(  "\n last estimate of cond.nr. of approx.  hessian  %10.3e\n",
            ctx->accinf[ctx->itstep][14]);
            printf(  "iterative steps total           %5i\n", ctx->itstep);
            printf(  "# of restarts                   %5i\n", nresta);
            printf(  "# of full regular updates       %5i\n", nupreg);
            printf(  "# of updates                    %5i\n", nbfgs1);
            printf(  "# of regularized full SQP-steps %5i\n", nsing);
        }
    }
    if ( ctx->optite < zero ) ctx->te1 = TRUE;
    if ( ctx->silent )        ctx->te1 = FALSE;
    if ( ctx->te1 ) {
        for (i = 1 ; i <= ctx->itstep ; i++) {
            ih1 = ctx->accinf[i][1];
            ih2 = ctx->accinf[i][9];
            ih3 = ctx->accinf[i][10];
            ih5 = ctx->accinf[i][18];
            ih6 = ctx->accinf[i][19];
            ih7 = ctx->accinf[i][22];
            ih8 = ctx->accinf[i][26];
            ih9 = ctx->accinf[i][27];

            fprintf(ctx->prou,
             "%4i  fx= %13.6e scf= %13.6e psi= %13.6e ups= %13.6e\n",
            ih1,ctx->accinf[i][2],ctx->accinf[i][3],ctx->accinf[i][4],ctx->accinf[i][5]);
            fprintf(ctx->prou,
            "     del= %13.6e b20= %13.6e b2n= %13.6e  nr=%5i\n",
            ctx->accinf[i][6],ctx->accinf[i][7],ctx->accinf[i][8],ih2);
            fprintf(ctx->prou,
            "      si=%4i            u-= %13.6e c-r= %13.6e c-d= %13.6e\n",
            ih3,ctx->accinf[i][11],ctx->accinf[i][13],ctx->accinf[i][14]);
            fprintf(ctx->prou,
            "      xn= %13.6e  dn= %13.6e pha=%4i            cl=%14i\n",
            ctx->accinf[i][16],ctx->accinf[i][17],ih5,ih6);
            fprintf(ctx->prou,
            "     skm= %13.6e sig= %13.6e cf+=%5i          dir= %13.6e\n",
            ctx->accinf[i][20],ctx->accinf[i][21],ih7,ctx->accinf[i][23]);
            fprintf(ctx->prou,
            "     dsc= %13.6e cos= %13.6e vio=%5i\n",
            ctx->accinf[i][24],ctx->accinf[i][25],ih8);
            fprintf(ctx->prou,
            "     upd=%5i           tk= %13.6e xsi= %13.6e\n",
            ih9,ctx->accinf[i][28],ctx->accinf[i][29]);

            if ( ctx->accinf[i][10] == 1. ) {
                fprintf(ctx->prou,"     qpt= %13.0e tqp= %13.6e sla= %13.6e\n",
                ctx->accinf[i][30],ctx->accinf[i][31],ctx->accinf[i][32]);
            }
        }
    }
    /*  accinf a c c u m u l a t e d   i n f o r m a t i o n                    */
    /*  on iteration sequence                                                   */
    /*  1: step-nr                                                              */
    /*  2: f(x-k) current value of objective (zero in feasibility improvement   */
    /*            phase (-1) )                                                  */
    /*  3: scf    internal scaling of objective (zero in phase -1)              */
    /*  4: psi    the weighted penalty-term                                     */
    /*  5: upsi   the unweighted penalty-term (L1-norm of constraint vector)    */
    /*  6: del_k_1 bound for currently active constraints                       */
    /*  7: b2n0   L2-norm of projected gradient, based on constraints in level  */
    /*            delmin and below, measured in the norm induced by the         */
    /*            inverse hessian estimate                                      */
    /*  8: b2n    L2-norm of projected gradient based on del_k_1                */
    /*  9: nr     number of binding constraints                                 */
    /* 10: sing   if 1, the binding constraints don't satisfy the regularity    */
    /*            condition                                                     */
    /* 11: umin   infinity norm of negative part of multiplier                  */
    /* 12: -------------                                                        */
    /* 13: cond_r condition number of diagonal part of QR-decomp. of normalized */
    /*            gradients of binding constraints                              */
    /* 14: cond_h condition number of diagonal of Cholesky-factor of updated    */
    /*            full hessian                                                  */
    /* 15: scf0   the relative damping of tangential component if upsi>tau0/2   */
    /* 16: xnorm  L2-norm of x                                                  */
    /* 17: dnorm  L2-norm of d (correction from QP -subproblem, unscaled)       */
    /* 18: phase  -1 : infeasibility improvement phase, 0: initial optimization */
    /*            1  : binding constraints unchanged , 2: d small, Maratos      */
    /*                 correction in use                                        */
    /* 19: c_k    number of decreases of penalty weights                        */
    /* 20: wmax   infinity norm of weights                                      */
    /* 21: sig_k  stepsize from unidimensional minimization (backtracking)      */
    /* 22: cfincr number of objective evaluations for stepsize-algorithm        */
    /* 23: dirder directional derivative of penalty-function along d (scaled)   */
    /* 24: dscal  scaling factor for d                                          */
    /* 25: cosphi cos of arc between d and d_previous. if larger theta , sig    */
    /*            larger than one (up to sigla) is tried                        */
    /* 26: violis[0] number of constraints not binding at x but hit during      */
    /*            line search                                                   */
    /* 27:        type of update for h: 1 normal P&M-BFGS-update,               */
    /*            0 update suppressed, -1 restart with scaled unit matrix ,     */
    /*            2 standard BFGS, 3 BFGS modified by Powells device            */
    /* 28: ny_k/tk modification factor for damping the projector in BFGS/       */
    /*            Pantoja-Mayne-term respectively                               */
    /* 29: 1-my_k/xsik modification factor for damping the quasi-Newton-        */
    /*            relation in BFGS for unmodified BFGS ny_k should be larger    */
    /*            than updmy0 (near one) and 1-my_k equal one./Pantoja-Mayne    */
    /*            term respectively                                             */
    /* 30: qpterm 0, if sing = -1, termination indicator of QP-solver otherwise */
    /*            1: successful, -1: tau becomes larger than tauqp without      */
    /*            slack-variables becoming sufficiently small                   */
    /*            -3: working set of QP-solver becomes linearly dependent       */
    /*            -2: infeasible QP-problem (theoretically impossible)          */
    /* 31: tauqp  weight  of slack-variables in QP-solver                       */
    /* 32: infeas L1-norm of slack-variables in QP-solver                       */

    if ( ! ctx->silent ) fclose(ctx->prou);
    if ( ! ctx->silent ) fclose(ctx->meu);

    return;
}
/* **************************************************************************** */
/*                      prints informations if te2 = TRUE                       */
/* **************************************************************************** */
void o8info(INTEGER icase, TLContext *ctx) {

     INTEGER  i,j,l,k;
     DOUBLE   y,phih;
     char     head[41];

    if(!ctx->te2) return;

    switch (icase) {

    case 1:
        fprintf(ctx->prou,"\n\n\n");
        for (i = 1 ; i <= 80 ; i++) fprintf(ctx->prou,"=");
        fprintf(ctx->prou,"\n          %4i-th iteration step\n", ctx->itstep);
        fprintf(ctx->prou,"   scf= %11.4e psist= %11.4e   psi= %11.4e  upsi= %11.4e\n",
        ctx->scf,ctx->psist,ctx->psi,ctx->upsi);
        fprintf(ctx->prou,"  fxst= %11.4e    fx= %11.4e\n", ctx->fxst,ctx->fx);
        fprintf(ctx->prou,"  x=\n");
        for (i = 1 ; i <= ctx->n ; i++) {
            fprintf(ctx->prou,"  %11.4e", ctx->x[i]);
            if ( i % 6 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
        }
        fprintf(ctx->prou," valid permutation of x\n\n");

        for (i = 1 ; i <= ctx->n ; i++) {
            fprintf(ctx->prou,"%3i ", ctx->perm[i]);
            if ( i % 20 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
        }
        if ( ctx->phase >= 0 && ctx->te3 ) {
            strcpy(head,"quasi-Newton-matrix full update");

            o8mdru_(ctx->a,ctx->n,ctx->n,head,ctx->prou,FALSE);
        }
        if ( ctx->intakt ) {
            printf(  "\n\n\n");
            for (i = 1 ; i <= 80 ; i++) printf(  "=");
            printf(  "\n          %4i-th iteration step\n", ctx->itstep);
            printf(  "   scf= %11.4e psist= %11.4e   psi= %11.4e  upsi= %11.4e\n",
            ctx->scf,ctx->psist,ctx->psi,ctx->upsi);
            printf(  "  fxst= %11.4e    fx= %11.4e\n", ctx->fxst,ctx->fx);
            printf(  "  x=\n");
            for (i = 1 ; i <= ctx->n ; i++) {
                printf("  %11.4e", ctx->x[i]);
                if ( i % 6 == 0 || i == ctx->n ) printf(  "\n");
            }
            printf(  " valid permutation of x\n\n");

            for (i = 1 ; i <= ctx->n ; i++) {
                printf(  "%3i ", ctx->perm[i]);
                if ( i % 20 == 0 || i == ctx->n ) printf(  "\n");
            }
        }
        return;

    case 2:
      fprintf(ctx->prou,"\n\n  del= %12.5e  b2n0= %12.5e   b2n= %12.5e   gfn= %12.5e\n",
      ctx->del,ctx->b2n0,ctx->b2n,ctx->gfn);

        if ( ctx->aalist[0] != 0 )
        {
            fprintf(ctx->prou,"\n\n values of restrictions\n ");
            for (i = 1 ; i <= ctx->aalist[0] ; i++)
            {
                j=ctx->aalist[i];
                if ( j > 2*ctx->n )
                {
                  fprintf(ctx->prou,"(%4i   %11.4e   %11.4e)  ",
                    j,ctx->res[j],ctx->gresn[(j+1)/2-ctx->n]);
                }
                else
                {  fprintf(ctx->prou,"(%4i   %11.4e  )  ",
                    j,ctx->res[j]);
                }
                if ( i % 2 == 0 || i == ctx->aalist[0] ) fprintf(ctx->prou,"\n ");
            }
        }
        if ( ctx->aalist[0] != 0 && ! ctx->singul ) {
            fprintf(ctx->prou,"\n\n   diag[r]=\n");
            for (i = 1 ; i <= ctx->aalist[0] ; i++) {
                fprintf(ctx->prou,"  %11.4e", ctx->diag[i]);
                if ( i % 6 == 0 || i == ctx->aalist[0] ) fprintf(ctx->prou,"\n");
            }
        }
        if ( ctx->alist[0] != 0 && ctx->te3 ) {
            for (i = 1 ; i <= ctx->aalist[0] ; i++) {
              l = ctx->aalist[i];
              if ( l > 2*ctx->n )
              {
                fprintf(ctx->prou,"\n\n gradient of restriction nr.%4i\n ", l);
                for (j = 0 ; j <= ctx->n ; j++) {
                    fprintf(ctx->prou," %11.4e  ", ctx->gres[j][(l+1)/2-ctx->n]);
                    if ( j % 5 == 0 || j == ctx->n ) fprintf(ctx->prou,"\n ");
                }
              }
            }
        }
        if ( ctx->intakt ) {
            printf("\n\n  del= %12.5e  b2n0= %12.5e   b2n= %12.5e   gfn= %12.5e\n",
            ctx->del,ctx->b2n0,ctx->b2n,ctx->gfn);

            if ( ctx->aalist[0] != 0 ) {
                printf(  "\n\n values of restrictions\n ");
                for (i = 1 ; i <= ctx->aalist[0] ; i++)
                {
                  j=ctx->aalist[i];
                  if ( j > 2*ctx->n )
                  {
                    fprintf(ctx->prou,"(%4i   %11.4e   %11.4e)  ",
                    j,ctx->res[j],ctx->gresn[(j+1)/2-ctx->n]);
                  }
                  else
                  {  fprintf(ctx->prou,"(%4i   %11.4e  )  ",
                      j,ctx->res[j]);
                  }

                  if ( i % 2 == 0 || i == ctx->aalist[0] ) printf(  "\n ");
                }
            }
            if ( ctx->aalist[0] != 0 && ! ctx->singul ) {
                printf(  "\n\n   diag[r]=\n");
                for (i = 1 ; i <= ctx->aalist[0] ; i++) {
                    printf(  "  %11.4e", ctx->diag[i]);
                    if ( i % 6 == 0 || i == ctx->aalist[0] ) printf(  "\n");
                }
            }
            if ( ctx->alist[0] != 0 && ctx->te3 ) {
                for (i = 1 ; i <= ctx->aalist[0] ; i++) {
                  l = ctx->aalist[i];
                  if ( l > 2*ctx->n )
                  {
                    printf(  "\n\n gradient of restriction nr.%4i\n ", l);
                    /* component zero is sign */
                    for (j = 0 ; j <= ctx->n ; j++) {
                        printf(  " %11.4e  ", ctx->gres[j][(l+1)/2-ctx->n]);
                        if ( j % 5 == 0 || j == ctx->n ) printf(  "\n ");
                    }
                  }
                }
            }
        }
        return;

    case 3:
        if( ! (ctx->nr == 0 || ctx->phase == -1) ) {
            fprintf(ctx->prou,"\n  multipliers: first estimate\n  u =\n");
            for (k = 1 ; k <= ctx->nr ; k++) {
                fprintf(ctx->prou," %4i  %11.4e", ctx->aalist[k],ctx->u[ctx->aalist[k]]);
                if ( k % 4 == 0 || k == ctx->nr ) fprintf(ctx->prou,"\n");
            }
        }
        if( ! (ctx->nr == 0 || ctx->phase == -1) && ctx->intakt ) {
            printf(      "\n  multipliers: first estimate\n  u =\n");
            for (k = 1 ; k <= ctx->nr ; k++) {
                printf(      " %4i  %11.4e", ctx->aalist[k],ctx->u[ctx->aalist[k]]);
                if ( k % 4 == 0 || k == ctx->nr ) printf(      "\n");
            }
        }
        return;

    case 4:
        if( ! (ctx->nr == 0 || ctx->phase == -1) ) {
            fprintf(ctx->prou,"\n  multipliers: second estimate\n  u =\n");
            for (k = 1 ; k <= ctx->nr ; k++) {
                fprintf(ctx->prou," %4i  %11.4e", ctx->aalist[k],ctx->u[ctx->aalist[k]]);
                if ( k % 4 == 0 || k == ctx->nr ) fprintf(ctx->prou,"\n");
            }
        }
        if( ! (ctx->nr == 0 || ctx->phase == -1) && ctx->intakt ) {
            printf(      "\n  multipliers: second estimate\n  u =\n");
            for (k = 1 ; k <= ctx->nr ; k++) {
                printf(      " %4i  %11.4e", ctx->aalist[k],ctx->u[ctx->aalist[k]]);
                if ( k % 4 == 0 || k == ctx->nr ) printf(      "\n");
            }
        }
        return;

    case 5:
        if( ctx->intakt )
        printf(          "  condition number of r     %.15e\n",
              ctx->accinf[ctx->itstep][13]);
        fprintf(ctx->prou,    "  condition number of r     %.15e\n",
              ctx->accinf[ctx->itstep][13]);
        if ( ctx->phase == -1 ) {

            return;

        } else {
            fprintf(ctx->prou,"  condition number of a     %.15e\n",ctx->accinf[ctx->itstep][14]);
            if ( ctx->intakt ) {
                printf(  "  condition number of a     %.15e\n",ctx->accinf[ctx->itstep][14]);
            }
            return;
        }
    case 6:

        return;

    case 7:
        fprintf(ctx->prou,"\n\n  phase=%3i  scf0= %11.4e\n", ctx->phase,ctx->scf0);
        fprintf(ctx->prou,    "  d =\n");
        for (i = 1 ; i <= ctx->n ; i++) {
            fprintf(ctx->prou,"  %11.4e", ctx->d[i]);
            if ( i % 6 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
        }
        if ( ctx->phase == 2 ) {
            fprintf(ctx->prou,"\n\n  dd=\n");
            for (i = 1 ; i <= ctx->n ; i++) {
                fprintf(ctx->prou,"  %11.4e", ctx->dd[i]);
                if ( i % 6 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
            }
        }
        if ( ctx->intakt ) {
            printf(  "\n\n  phase=%3i  scf0= %11.4e\n", ctx->phase,ctx->scf0);
            printf(      "  d =\n");
            for (i = 1 ; i <= ctx->n ; i++) {
                printf(  "  %11.4e", ctx->d[i]);
                if ( i % 6 == 0 || i == ctx->n ) printf(  "\n");
            }
            if ( ctx->phase == 2 ) {
                printf(  "\n\n  dd=\n");
                for (i = 1 ; i <= ctx->n ; i++) {
                    printf(  "  %11.4e", ctx->dd[i]);
                    if ( i % 6 == 0 || i == ctx->n ) printf(  "\n");
                }
            }
        }
        return;

    case 8:
        y    = ctx->tau0*p5;
        phih = ctx->fx*ctx->scf+ctx->psi;

        if ( ctx->intakt ) {
            printf(  "\n\n start unimin\n\n");
            printf(  "    phi= %11.4e   dphi= %11.4e    psi= %11.4e tau0/2= %11.4e\n",
            phih,ctx->dirder,ctx->psi,y);
            printf(  "     fx= %11.4e  dscal= %11.4e    scf= %11.4e   upsi= %11.4e\n",
            ctx->fx,ctx->dscal,ctx->scf,ctx->upsi);
        }
        fprintf(ctx->prou,"\n\n start unimin\n\n");
        fprintf(ctx->prou,"    phi= %11.4e   dphi= %11.4e    psi= %11.4e tau0/2= %11.4e\n",
        phih,ctx->dirder,ctx->psi,y);
        fprintf(ctx->prou,"     fx= %11.4e  dscal= %11.4e    scf= %11.4e   upsi= %11.4e\n",
        ctx->fx,ctx->dscal,ctx->scf,ctx->upsi);

        return;

    case 9:
        fprintf(ctx->prou,"    sig= %11.4e     fx= %11.4e    psi= %11.4e   upsi= %11.4e\n",
        ctx->sig,ctx->fx1,ctx->psi1,ctx->upsi1);
        if ( ctx->intakt )
        printf(      "    sig= %11.4e     fx= %11.4e    psi= %11.4e   upsi= %11.4e\n",
        ctx->sig,ctx->fx1,ctx->psi1,ctx->upsi1);

        return;

    case 10:
        fprintf(ctx->prou,"\n\n end unimin\n");
        fprintf(ctx->prou,  "\n sig= %11.4e  num. f-evaluations%2i\n", ctx->sig,ctx->cfincr);
        fprintf(ctx->prou,    " list of inactive hit constraints\n");
        for (i = 1 ; i <= ctx->violis[0] ; i++) {
            fprintf(ctx->prou,"%4i  ", ctx->violis[i]);
            if ( i % 13 == 0 || i == ctx->violis[0] ) fprintf(ctx->prou,"\n");
        }
        if ( ctx->violis[0] == 0 ) fprintf(ctx->prou,"none\n");
        if ( ctx->intakt ) {
            printf(  "\n\n end unimin\n");
            printf(    "\n sig= %11.4e  num. f-evaluations%2i\n", ctx->sig,ctx->cfincr);
            printf(      " list of inactive hit constraints\n");
            for (i = 1 ; i <= ctx->violis[0] ; i++) {
                printf(  "%4i  ", ctx->violis[i]);
                if ( i % 13 == 0 || i == ctx->violis[0] ) printf(  "\n");
            }
            if ( ctx->violis[0] == 0 ) printf("none\n");
        }
        return;

    case 11:
        if ( ctx->intakt ) printf("additional increase of eta due to large clow\n");
        fprintf(ctx->prou,        "additional increase of eta due to large clow\n");

        return;

    case 12:
        fprintf(ctx->prou,
        "\n\n  current scaling,  scf =  %11.4e clow = %12i eta =  %11.4e\n",
        ctx->scf,ctx->clow,ctx->eta);

        if(ctx->nres != 0) {
            fprintf(ctx->prou,"\n\n  scalres=\n");
            for (i = 1 ; i <= 2*ctx->nres ; i++) {
                fprintf(ctx->prou,"  %11.4e", ctx->w[i]);
                if ( i % 6 == 0 || i == 2*ctx->nres ) fprintf(ctx->prou,"\n");
            }
        }
        if ( ctx->intakt ) {
            printf(
            "\n\n  current scaling,  scf =  %11.4e clow = %12i eta =  %11.4e\n",
            ctx->scf,ctx->clow,ctx->eta);

            if ( ctx->nres != 0 ) {
                printf(  "\n\n  scalres=\n");
                for (i = 1 ; i <= 2*ctx->nres ; i++) {
                    printf(  "  %11.4e", ctx->w[i]);
                    if ( i % 6 == 0 || i == 2*ctx->nres ) printf(  "\n");
                }
            }
        }
        return;

    case 13:
        if ( ctx->accinf[ctx->itstep][27] == zero ) {
            if ( ctx->intakt ) printf("update suppressed\n");
            fprintf(ctx->prou,        "update suppressed\n");
        } else if ( ctx->accinf[ctx->itstep][27] == -one ) {
            fprintf(ctx->prou,        "restart with scaled unit matrix\n");
            if ( ctx->intakt ) printf("restart with scaled unit matrix\n");
        } else {
            fprintf(ctx->prou,"BFGS-update\n");
            fprintf(ctx->prou," type = %14.6e\n", ctx->accinf[ctx->itstep][27]);
            fprintf(ctx->prou,"  ny  = %14.6e\n", ctx->accinf[ctx->itstep][28]);
            fprintf(ctx->prou," thet = %14.6e\n", ctx->accinf[ctx->itstep][29]);
            if ( ctx->intakt ) {
                printf(  "BFGS-update\n");
                printf(  " type = %14.6e\n", ctx->accinf[ctx->itstep][27]);
                printf(  "  ny  = %14.6e\n", ctx->accinf[ctx->itstep][28]);
                printf(  " thet = %14.6e\n", ctx->accinf[ctx->itstep][29]);
            }
        }
        return;

    case 14:
        if ( ctx->accinf[ctx->itstep][27] == zero ) {
            if ( ctx->intakt ) printf("update suppressed\n");
            fprintf(ctx->prou,        "update suppressed\n");
        } else if ( ctx->accinf[ctx->itstep][27] == one ) {
            fprintf(ctx->prou,"BFGS-update as in Pantoja and Mayne\n");
            fprintf(ctx->prou,"  tk  = %14.6e\n", ctx->accinf[ctx->itstep][28]);
            fprintf(ctx->prou," xsik = %14.6e\n", ctx->accinf[ctx->itstep][29]);
            if ( ctx->intakt ) {
                printf(  "BFGS-update\n");
                printf(  "  tk  = %14.6e\n", ctx->accinf[ctx->itstep][28]);
                printf(  " xsik = %14.6e\n", ctx->accinf[ctx->itstep][29]);
            }
        } else {
            if ( ctx->intakt ) printf("restart with scaled unit matrix\n");
            fprintf(ctx->prou,        "restart with scaled unit matrix\n");
        }
        return;

    case 15:
        if ( ctx->intakt ) printf("\n\n\n singular case : full regularized SQP\n");
        fprintf(ctx->prou,        "\n\n\n singular case : full regularized SQP\n");
        if ( ctx->intakt ) printf("  del = %.15e\n", ctx->del);
        fprintf(ctx->prou,        "  del = %.15e\n", ctx->del);

        if ( ctx->intakt ) {
            printf(  "\n\n  scalres=\n");
            for (i = 1 ; i <= 2*ctx->nres ; i++) {
                printf(  "  %11.4e", ctx->w[i]);
                if ( i % 6 == 0 || i == 2*ctx->nres ) printf(  "\n");
            }
        }
        fprintf(ctx->prou,"\n\n  scalres=\n");
        for (i = 1 ; i <= 2*ctx->nres ; i++) {
            fprintf(ctx->prou,"  %11.4e",ctx->w[i]);
            if ( i % 6 == 0 || i == 2*ctx->nres ) fprintf(ctx->prou,"\n");
        }
        if ( ctx->te3 && ctx->alist[0] != 0 )
        {
            fprintf(ctx->prou,"gradients of general constraints");

            for (i = 1 ; i <= ctx->aalist[0] ; i++)
            {
              l = ctx->aalist[i];
              if ( l > 2*ctx->n )
              {
                fprintf(ctx->prou,"\n\n gradient of restriction nr.%4i\n ", l);
                for (j = 0 ; j <= ctx->n ; j++)
                {
                    fprintf(ctx->prou," %11.4e  ", ctx->gres[j][(l+1)/2-ctx->n]);
                    if ( j % 5 == 0 || j == ctx->n ) fprintf(ctx->prou,"\n ");
                }
              }
            }
        }


        return;

    case 16:
        fprintf(ctx->prou,"exit from full SQP\n");
        fprintf(ctx->prou,"            termination reason  %7.0e\n",
        ctx->accinf[ctx->itstep][30]);
        fprintf(ctx->prou,"          final value of tauqp  %10.3e\n",
        ctx->accinf[ctx->itstep][31]);
        fprintf(ctx->prou,"      sum norm of slack vector  %10.3e\n",
        ctx->accinf[ctx->itstep][32]);

        fprintf(ctx->prou,"\n\n  phase=%3i  scf0= %11.4e\n",ctx->phase,ctx->scf0);
        fprintf(ctx->prou,"\n\n  d = \n");
        for (i = 1 ; i <= ctx->n ; i++) {
            fprintf(ctx->prou,"  %11.4e", ctx->d[i]);
            if ( i % 6 == 0 || i == ctx->n ) fprintf(ctx->prou,"\n");
        }
        if ( ctx->nres != 0 ) {
            fprintf(ctx->prou,"\n  multipliers: first estimate\n  u =\n");
            for (k = 1 ; k <= 2*ctx->nres ; k++) {
                fprintf(ctx->prou," %4i  %11.4e", k,ctx->u[k]);
                if ( k % 4 == 0 || k == 2*ctx->nres ) fprintf(ctx->prou,"\n");
            }
        }
        if ( ctx->intakt ) {
            printf(  "exit from full SQP\n");
            printf(  "            termination reason  %7.0e\n",
            ctx->accinf[ctx->itstep][30]);
            printf(  "          final value of tauqp  %10.3e\n",
            ctx->accinf[ctx->itstep][31]);
            printf(  "      sum norm of slack vector  %10.3e\n",
            ctx->accinf[ctx->itstep][32]);

            printf("\n\n  phase=%3i  scf0= %11.4e\n",  ctx->phase,ctx->scf0);
            printf("\n\n d = \n");
            for (i = 1 ; i <= ctx->n ; i++) {
                printf(  "  %11.4e", ctx->d[i]);
                if ( i % 6 == 0 || i == ctx->n ) printf(  "\n");
            }
            if ( ctx->nres != 0 ) {
                printf(  "\n  multipliers: first estimate\n  u =\n");
                for (k = 1 ; k <= 2*ctx->nres ; k++) {
                    printf(  " %4i  %11.4e", k,ctx->u[k]);
                    if ( k % 4 == 0 || k == 2*ctx->nres ) printf(  "\n");
                }
            }
        }
        return;

    case 17:
        fprintf(ctx->prou,"small directional derivative %.15e: finish\n",ctx->dirder);
        if ( ctx->intakt )
        printf(      "small directional derivative %.15e: finish\n",ctx->dirder);

        return;

    case 18:
        if ( ctx->intakt )
        printf(      "small correction from full regularized SQP,finish\n");
        fprintf(ctx->prou,"small correction from full regularized SQP,finish\n");

        return;

    case 19:
        fprintf(ctx->prou,        "QP-solver terminated unsuccessfully\n");
        if ( ctx->intakt ) printf("QP-solver terminated unsuccessfully\n");

        return;

    case 20:
        if ( ctx->intakt ) printf("restart with scaled unit matrix\n");
        fprintf(ctx->prou,        "restart with scaled unit matrix\n");

        return;

    case 21:

        return;

    case 22:

        return;
    }
}

/* **************************************************************************** */
/*         computation of new scaling factors for L1-penalty-function           */
/* **************************************************************************** */
void o8sce(TLContext *ctx) {


     INTEGER  i;
     DOUBLE   term,s1,s2,diff0;
     LOGICAL  wlow;

    wlow = FALSE;
    for (i = 1 ; i <= 2*ctx->nres ; i++) {

        /* w1 tentative new weights */

        term = ctx->ny*fabs(ctx->u[i])+ctx->tau;
        if ( term > ctx->w[i] ) {
            ctx->w1[i] = term+ctx->tau;
        } else {
            ctx->w1[i] = ctx->w[i];
            if ( term < ctx->w[i]*p5 && ctx->o8bind[i] == 1 ) ctx->w1[i] = (term+ctx->w[i])*p5;
        }
        if ( ctx->w1[i] < ctx->w[i]) wlow = TRUE;
    }
    /* wlow equals TRUE if one tentative weight at least has been decreased */

    s1 = zero;
    s2 = zero;
    for (i = 1 ; i <= ctx->nres ; i++) {
        if ( ctx->low[i] == ctx->up[i] ) {
/*  an equality constraint counts only once whereas an interval constraint */
/*  gives two inequality constraints                                       */

            s1 = s1+ctx->w1[2*i-1]*fabs(ctx->resst[2*i-1]);
            s2 = s2+ctx->w1[2*i-1]*fabs(ctx->res[2*i-1]);
        } else {
            s1 = s1-min(zero,ctx->resst[2*i-1])*ctx->w1[2*i-1];
            s2 = s2-min(zero,ctx->res[2*i-1]  )*ctx->w1[2*i-1];
            s1 = s1-min(zero,ctx->resst[2*i])*ctx->w1[2*i];
            s2 = s2-min(zero,ctx->res[2*i]  )*ctx->w1[2*i];

        }
    }
    diff0 = (ctx->fxst-ctx->fx)*ctx->scf+(s1-s2);
    if ( wlow && diff0 >= ctx->eta*ctx->clow && ctx->itstep-ctx->lastdw > max(5,min(20,ctx->n/10)) )
    {

        /* accept new (diminished ) weights */

        if ( ctx->clow > ctx->itstep/10 ) {
            ctx->eta = onep3*ctx->eta;
            if ( ! ctx->silent ) o8info(11, ctx);
        }
        ctx->lastch = ctx->itstep;
        ctx->lastdw = ctx->itstep;
        ctx->level  = diff0/ctx->iterma;
        ctx->psist  = s1;
        ctx->psi    = s2;
        for (i = 1 ; i <= 2*ctx->nres ; i++) {
            ctx->w[i] = ctx->w1[i];
        }
        ctx->clow = ctx->clow+one;
    }
    else
    {

        /* increase individual weights if necessary. let weigths unchanged */
        /* otherwise                                                       */

        s1 = zero;
        s2 = zero;
        for (i = 1 ; i <= ctx->nres ; i++) {
            if ( ctx->w1[2*i-1] > ctx->w[2*i-1] || ctx->w1[2*i] > ctx->w[2*i] ) {
                ctx->lastup = ctx->itstep;
                ctx->lastch = ctx->itstep;
            }
            ctx->w[2*i-1] = max(ctx->w[2*i-1],ctx->w1[2*i-1]);
            ctx->w[2*i]   = max(ctx->w[2*i],ctx->w1[2*i]);
            if ( ctx->low[i] == ctx->up[i] ) {
                s1 = s1+ctx->w[2*i-1]*fabs(ctx->resst[2*i-1]);
                s2 = s2+ctx->w[2*i-1]*fabs(ctx->res[2*i-1]);
            } else {
                s1 = s1-ctx->w[2*i-1]*min(zero,ctx->resst[2*i-1]);
                s2 = s2-ctx->w[2*i-1]*min(zero,ctx->res[2*i-1]);
                s1 = s1-ctx->w[2*i]*min(zero,ctx->resst[2*i]);
                s2 = s2-ctx->w[2*i]*min(zero,ctx->res[2*i]);
            }
        }
        ctx->psist = s1;
        ctx->psi   = s2;
    }
    term = zero;
    if ( ctx->nres >= 1 ) term = ctx->w[1];
    for (i = 2 ; i <= 2*ctx->nres ; i++) {
        term = max(term,ctx->w[i]);
    }
    ctx->accinf[ctx->itstep][20] = term;

    /* maximum of weights */

    ctx->accinf[ctx->itstep][19] = ctx->clow;

    if ( ! ctx->silent ) o8info(12, ctx);

    return;
}

/* **************************************************************************** */
/*          computation of the Pantoja-Mayne BFGS-update of hessian             */
/* **************************************************************************** */
void o8bfgs(TLContext *ctx) {

     INTEGER  i,j,k;
     DOUBLE   den1,den2,den3,
                    th,tk,xsik,
                    term,term1,anorm,acond,ndx,ngtdx,den21;
     LOGICAL  fail;

    for ( i = 1 ;  i <= ctx->n ;  i++) {
         ctx->o8bfgs_dg[i]   = ctx->gphi1[i]-ctx->gphi0[i];
    }
    if ( o8vecn(1,ctx->n,ctx->o8bfgs_dg) == zero ) {

        /* suppress update */

        ctx->accinf[ctx->itstep][27] = zero;
        ctx->accinf[ctx->itstep][28] = zero;
        ctx->accinf[ctx->itstep][29] = zero;

        if ( ! ctx->silent ) o8msg(21, ctx);

        return;
    }
    for ( i = 1 ;  i <= ctx->n ;  i++) {

        /* multiply dx = (s in the usual notation) by Cholesky-factor */
        /* stored in the upper half of a                              */

         ctx->o8bfgs_ltdx[i] = o8sc2(i,ctx->n,i,ctx->a,ctx->difx);
    }
    for (i = 1 ; i <= ctx->n ; i++) {
        /* multiply by Cholesky factor transpose : A=L*L' , L' = R    */
        ctx->o8bfgs_adx[i] = o8sc3_(1,i,i,ctx->a,ctx->o8bfgs_ltdx);
    }
    /* o8bfgs_adx = a * ( x-x0), x-x0 = difx */

    for (i = 1 ; i <= ctx->aalist[0] ; i++) {
        j=ctx->aalist[i];
        if ( j <= 2*ctx->n )
        /* a bound constraint */
        {
          ctx->o8bfgs_gtdx[i] = ctx->xsc[(j+1)/2]*ctx->difx[(j+1)/2];
          if ( j % 2 == 0 ) ctx->o8bfgs_gtdx[i] = -ctx->o8bfgs_gtdx[i];
        }
        else
        {
          j = ( j - 2*ctx->n +1)/2;
          ctx->o8bfgs_gtdx[i] = o8sc3(1,ctx->n,j,ctx->gres,ctx->difx)*ctx->gres[0][j];
          ctx->o8bfgs_gtdx[i] = ctx->o8bfgs_gtdx[i]/ctx->gresn[j];
        }
    }
    /* o8bfgs_gtdx = grad(res(alist))(transpose)*(x-x0) */

    ndx   = o8vecn(1,ctx->n,ctx->difx);
    tk    = min(p5,pow(ctx->dnorm,2));
    anorm = zero;
    term1 = fabs(ctx->a[1][1]);
    anorm = zero;
    for (i = 1 ; i <= ctx->n ; i++) {
        for (j = i ; j <= ctx->n ; j++) {
            anorm = anorm+pow(ctx->a[i][j],2);
        }
        term1 = min(term1,fabs(ctx->a[i][i]));
    }
    if ( term1 != zero ) {
        acond = anorm/pow(term1,2);
    } else {
        acond = ctx->epsmac/ctx->tolmac;
    }
    den1 = pow(o8vecn(1,ctx->n,ctx->o8bfgs_ltdx),2);
    den2 = o8sc1(1,ctx->n,ctx->o8bfgs_dg,ctx->difx);
    if ( den1 <= ctx->rho1*anorm*pow(ndx,2) || acond >= one/ctx->rho1 ) {

        /* take a restart step , since condition estimate indicates */
        /* near singularity                                         */
        o8inim(ctx);

        return;
    }
    if ( ctx->nr == 0 ) {
        /* formerly this was nres == 0 , but now we have always nres>=n */
        /* in the unconstrained case we take the Powell update          */

        th = one;
        if ( den2 < p2*den1 ) {
            th = p8*den1/(den1-den2);
            for (i = 1 ; i <= ctx->n ; i++) {
                ctx->o8bfgs_dg[i] = th*ctx->o8bfgs_dg[i]+(one-th)*ctx->o8bfgs_adx[i];
            }
            den2 = o8sc1(1,ctx->n,ctx->o8bfgs_dg,ctx->difx);
        }
        term = one/sqrt(den2);
        for (i = 1 ; i <= ctx->n ; i++) {
            ctx->o8bfgs_dg[i]   = ctx->o8bfgs_dg[i]*term;
            ctx->o8bfgs_updz[i] = ctx->o8bfgs_dg[i];
        }
        term = one/sqrt(den1);
        for (i = 1 ; i <= ctx->n ; i++) {
            ctx->o8bfgs_updx[i] = ctx->o8bfgs_adx[i]*term;
        }
        ctx->accinf[ctx->itstep][28] = den2/den1;
        ctx->accinf[ctx->itstep][29] = th;
        ctx->accinf[ctx->itstep][27] = two;
        if ( th != one ) ctx->accinf[ctx->itstep][27] = three;
    } else {
        ngtdx = o8vecn(1,ctx->aalist[0],ctx->o8bfgs_gtdx);
        term  = one/sqrt(den1);
        for (i = 1 ; i <= ctx->n ; i++) {
            ctx->o8bfgs_updx[i] = ctx->o8bfgs_adx[i]*term;
        }
        if ( den2 >= ctx->rho1*o8sc1(1,ctx->n,ctx->o8bfgs_dg,ctx->o8bfgs_dg)
        && o8vecn(1,ctx->n,ctx->o8bfgs_dg) >= sqrt(ctx->epsmac)*ndx ) {
            xsik = zero;
            for (i = 1 ; i <= ctx->n ; i++) {
                ctx->o8bfgs_updz[i] = ctx->o8bfgs_dg[i];
            }
            den21 = den2;
        } else {

            /* try Pantoja-Mayne modification */

            den3 = tk*pow(ndx,2)+pow(ngtdx,2);
            if ( den2 >= ctx->rho1*o8sc1(1,ctx->n,ctx->o8bfgs_dg,ctx->o8bfgs_dg) ) {
                xsik = one;
            } else {
                xsik = one+(tk*pow(ndx,2)+fabs(den2))/den3;
            }
            for (i = 1 ; i <= ctx->n ; i++) {
                term = zero;
                for (j = 1 ; j <= ctx->aalist[0] ; j++)
                {
                  k = ctx->aalist[j];
                  if ( k > 2*ctx->n )
                  {
                    k = ( k -2*ctx->n+1)/2 ;
                    term1 = ctx->gres[i][k]*ctx->o8bfgs_gtdx[j]*ctx->gres[0][k];
                    term1 = term1/ctx->gresn[k];
                    term  = term+term1;
                  }
                  else
                  {
                    term1 = zero ;
                    if ( i == (k+1)/2 ) term1 = ctx->xsc[i]*ctx->o8bfgs_gtdx[j] ;
                    if ( k % 2 == 0 ) term1 = -term1 ;
                    term += term1 ;
                  }
                }
                ctx->o8bfgs_updz[i] = ctx->o8bfgs_dg[i]+xsik*(tk*ctx->difx[i]+term);
            }
            den21 = o8sc1(1,ctx->n,ctx->o8bfgs_updz,ctx->difx);
        }
        /*  theoretically den21>0. if not, everything is spoiled by roundoff */
        /*  do nothing then                                                  */
        if ( den21 <= zero ) {
          return;
        }
        term = one/sqrt(den21);
        for (i = 1 ; i <= ctx->n ; i++) {
            ctx->o8bfgs_updz[i] = ctx->o8bfgs_updz[i]*term;
        }
        th = one;
        if ( den2 < p2*den1 ) {
            th = p8*den1/(den1-den2);
            for (i = 1 ; i <= ctx->n ; i++) {
                ctx->o8bfgs_dg[i] = th*ctx->o8bfgs_dg[i]+(one-th)*ctx->o8bfgs_adx[i];
            }
            den2 = o8sc1(1,ctx->n,ctx->o8bfgs_dg,ctx->difx);
        }
        term = one/sqrt(den2);
        for (i = 1 ; i <= ctx->n ; i++) {
            ctx->o8bfgs_dg[i] = ctx->o8bfgs_dg[i]*term;
        }
        if ( o8vecn(1,ctx->n,ctx->o8bfgs_dg) <= tm3*o8vecn(1,ctx->n,ctx->o8bfgs_updz) ) {

            /* the Powell update produces a smaller growth */

            for (i = 1 ; i <= ctx->n ; i++) {
                ctx->o8bfgs_updz[i] = ctx->o8bfgs_dg[i];
            }
            ctx->accinf[ctx->itstep][28] = den2/den1;
            ctx->accinf[ctx->itstep][29] = th;
            ctx->accinf[ctx->itstep][27] = two;
            if ( th != one ) ctx->accinf[ctx->itstep][27] = three;
        } else {

            /* no update if strongly irregular */

            ctx->accinf[ctx->itstep][27] = one;
            ctx->accinf[ctx->itstep][28] = tk;
            ctx->accinf[ctx->itstep][29] = xsik;
        }
    }
    o8upd(ctx->a,ctx->o8bfgs_updz,ctx->o8bfgs_updx,ctx->n,&fail, ctx);

    /* check illconditioning after updating */

    term  = fabs(ctx->a[1][1]);
    term1 = term;
    i     = 1;

    /* in order to overcome a curious error in hp's f77 compiler */
    /* this kind of loop                                         */

    while ( i < ctx->n ) {
        i     = i+1;
        term  = max(term, fabs(ctx->a[i][i]));
        term1 = min(term1,fabs(ctx->a[i][i]));
    }
    if ( fail || pow(term1,2) <= ctx->rho1*pow(term,2) ) {

        /* reset */

        o8inim(ctx);
    }
    return;
}

/* **************************************************************************** */
/*                      write short information on standard out                 */
/* **************************************************************************** */
void o8shms(TLContext *ctx) {

     DOUBLE   umin;

    if ( ctx->te0 && ! ctx->silent ) {
        umin = ctx->accinf[ctx->itstep][11];
        printf(
        "%5i fx= %14.6e upsi= %8.1e b2n= %8.1e umi= %8.1e nr%4i si%2i\n",
        ctx->itstep,ctx->fx,ctx->upsi,ctx->b2n,umin,ctx->nr,(int)ctx->accinf[ctx->itstep][10]);

        fprintf(ctx->prou,
        "%5i fx= %14.6e upsi= %8.1e b2n= %8.1e umi= %8.1e nr%4i si%2i\n",
        ctx->itstep,ctx->fx,ctx->upsi,ctx->b2n,umin,ctx->nr,(int)ctx->accinf[ctx->itstep][10]);
    }
    return;
}

/* **************************************************************************** */
/*                      write messages on "special events"                      */
/* **************************************************************************** */
void o8msg(INTEGER num, TLContext *ctx)
{

     INTEGER  i;

    if ( num <= 0 || num  > 26 ) return;

    switch (num) {

    case 1:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"rankdeficiency of grad's of active constr.!\n");
        fprintf(ctx->meu,"on the basis of delmin!\n");

        return;

    case 2:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"rescaling of objective function scf= %.15e\n",ctx->scf);

        return;

    case 3:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"rankdeficiency of grad's of active constr.!\n");
        fprintf(ctx->meu," del= %.15e\n", ctx->del);

        return;

    case 4:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"rankdeficiency of grad's of active constr.!\n");
        fprintf(ctx->meu," del= %.15e\n", ctx->del);

        return;

    case 5:
        fprintf(ctx->meu,"qpterm<0: no dir. of. desc., tauqp max\n");

        return;

    case 6:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"second order correction suppressed! \n");

        return;

    case 7:

        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"retry next step with a=id \n");

        return;

    case 8:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"some constraint has gradient equal to zero \n");
        fprintf(ctx->meu,"resulting d may be no direction of descent\n");

        return;

    case 9:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"try regularized SQP with increased weights\n");
        fprintf(ctx->meu,"since dnorm small or infeasibility large\n");

        return;

    case 10:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"QPsolver did not complete, try d anyway, may fail\n");

        return;

    case 11:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"direct. deriv. positive! may be due to inaccurate\n");
        fprintf(ctx->meu,"gradients or extreme illconditioning\n");
        fprintf(ctx->meu,"dirder=  %.15e\n", ctx->dirder);

        return;

    case 12:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"call of matdru suppressed, mat too large\n");

        return;

    case 13:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"startvalue corrected in order to fit bounds\n");

        return;

    case 14:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"try full SQP due to slow progress in x \n");

        return;

    case 15:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"try full SQP due to small stepsizes while\n");
        fprintf(ctx->meu,"infeasibility large\n");
    case 16:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"on exit from o8qpdu dir. deriv. positive!\n");
        fprintf(ctx->meu,"try increased tauqp\n");

        return;

    case 17:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"try regularized SQP with increased weights\n");
        fprintf(ctx->meu,"no decrease of weights possible\n");
        fprintf(ctx->meu,"and incompatibility large\n");

        return;

    case 18:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"try regularized SQP with increased weights\n");
        fprintf(ctx->meu,"since no direction of descent has been obtained\n");

        return;

    case 19:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"degeneracy in dual QP\n");
        fprintf(ctx->meu,"restr. %i to be added\n",ctx->iptr);
        fprintf(ctx->meu,"new rii= %.15e\n",ctx->riitr);
        fprintf(ctx->meu,"length of current working set=%i\n",ctx->iqtr);
        fprintf(ctx->meu,"working set\n");
        for (i = 1 ; i <= ctx->iqtr ; i++) {
            fprintf(ctx->meu,"%4i ",ctx->aitr[i]);
            if ( i % 15 == 0 || i == ctx->iqtr ) fprintf(ctx->meu,"\n");
        }
        fprintf(ctx->meu,"primal feasibility violation is= %.15e\n",ctx->sstr);

        return;

    case 20:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"dual QP seemingly infeasible \n");
        fprintf(ctx->meu,"theoretically impossible\n");

        return;

    case 21:
        fprintf(ctx->meu,"step=%i\n", ctx->itstep);
        fprintf(ctx->meu,"no update since o8bfgs_dg=0\n");

        return;

    case 22:
        fprintf(ctx->meu,"step%i\n", ctx->itstep);
        fprintf(ctx->meu,"function evaluation returns err=true\n");

        return;

    case 23:
        fprintf(ctx->meu,"step%i\n", ctx->itstep);
        fprintf(ctx->meu,"contraint evaluation returns err=true\n");

        return;

    case 24:
        fprintf(ctx->meu,"step%i\n", ctx->itstep);
        fprintf(ctx->meu,"current point cannot be changed in current\n");
        fprintf(ctx->meu,"direction due to error-message from function\n");
        fprintf(ctx->meu,"evaluation\n");

        return;

    case 25:
        fprintf(ctx->meu,"step%i\n", ctx->itstep);
        fprintf(ctx->meu,"reduce stepsize due to error-indicator set\n");
        fprintf(ctx->meu,"by users function evaluation\n");

        return;
    case 26:
        fprintf(ctx->meu,"step%i\n", ctx->itstep);
        fprintf(ctx->meu,"dual qp: no increase in primal objective: terminate\n");
        return;
    }
}

/* **************************************************************************** */
/* equality constrained recursive quadratic programming with                    */
/* multiple inactivation and superlinearly convergent projected                 */
/* BFGS-update (version 12/93 Spellucci, modified subsequently )                                       */
/* this version from 31.10.2001
/* **************************************************************************** */
void o8opti(TLContext *ctx)
{

     INTEGER  l,l0,i,j,k,csssig,csirup,csreg,cschgx;
     INTEGER  csmdph;
     DOUBLE   delsig,delx,sum,term;
     DOUBLE   umin,term1,scfh,unorm;
     DOUBLE   compl,del1;
     INTEGER  iumin,rank0,nr0,csdifx,clwold;
     INTEGER  nrbas;
     DOUBLE   eps,delold,uminsc,fac,slackn,tauqp0,denom;
     LOGICAL  nperm,qpnew,etaini;
    /* added feature 25.01.2000 */
     LOGICAL  viobnd ;
    /* end added feature */
    /* new form of constraint evaluation */
     LOGICAL eval_err;
    /* added feature user interrupt */
     LOGICAL cont ;
    /* initialization */


    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->d[i]  = zero;
        ctx->d0[i] = zero;
    }
    ctx->itstep    = 0;
    ctx->alist[0]  = 0;      /* active general constraints */
    ctx->aalist[0] = 0 ;     /* all active constraints in 1,...,2*nres */
    ctx->o8opti_delist[0] = 0;
    ctx->violis[0] = 0;
    ctx->clist[0]  = 0;      /* the nonlinear active constraints */
    ctx->upsi      = zero;
    ctx->psi       = zero;
    ctx->psi0      = zero;
    ctx->sig0      = zero;
    ctx->d0norm    = one;
    unorm     = one;
    ctx->fx        = zero ;
    ctx->fx0       = zero ;
    /* in order to have cosphi well defined for itstep = 1 */

    ctx->dnorm  = one;
    ctx->del    = ctx->del0;

    /* count successive regularization steps */

    csreg  = 0;

    /* count successive small changes in x */

    cschgx = 0;

    /* count small differences of fx */

    csdifx = 0;

    /* count irregular quasi-Newton-updates */

    csirup = 0;

    /* count successive small stepsizes */

    csssig = 0;

    /* count successive small differences of penalty-function */

    csmdph = 0;
    /*   moved to o8st : matsc  = one;  */

    /* formerly tauqp = tp2 is worse */

    ctx->tauqp  = one;
    nperm  = FALSE;
    ctx->ident  = FALSE;
    etaini = FALSE;
    if ( ctx->n > 100 || ctx->nres > 100 ) ctx->te3 = FALSE;
    for (i = 1 ; i <= ctx->n ; i ++)
    {
        ctx->perm[i]  = i;
        ctx->perm1[i] = i;
    }
    /*  compute first list of binding constraints */
    for ( i = 1 ; i <= ctx->nres ; i++ )
    {
    /*  equality constraint is always active . use the odd numbered formal
    constraint only */
        if ( ctx->low[i] == ctx->up[i] )
        {
          ctx->aalist[0]       += 1 ;
          ctx->aalist[ctx->aalist[0]] = 2*i-1 ;
          if ( i > ctx->n )
    /*  a general constraint                 */
          {
            ctx->alist[0] += 1;
            ctx->alist[ctx->alist[0]] = i-ctx->n ;
            ctx->gres[0][i-ctx->n] = one ;
          }
          if ( i > ctx->n+ctx->nlin )
          {
    /*  a nonlinear equality constraint                             */
            ctx->clist[0] += 1;
            ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin ;
          }

          ctx->o8bind[2*i-1]     = 1 ;
          ctx->o8bind[2*i]       = 0 ;
          ctx->o8bind0[2*i-1]    = 1 ;
          ctx->o8bind0[2*i]      = 0 ;
        }
    }
    if ( ctx->analyt )
    {
        eps = min(ctx->epsx,sqrt(ctx->epsmac));
    }
    else
    {
        eps = ctx->epsdif;
        if ( ctx->epsx < pow(ctx->epsdif,2) ) ctx->epsx = pow(ctx->epsdif,2);
    }
    eps = max(ctx->epsmac*tp3,min(tm3,eps));

    /* calling for external function evaluation necessary only if corr = TRUE */

    /* function and gradient values, from xtr = x*xsc */

    /*  remember that that es_routines consider bloc/non bloc call           */
    /*  in case of bloc == TRUE the evaluation has been done in o8st already */

    for ( i = 1 ; i <= ctx->n ; i++ )
    { ctx->res[2*i-1] = ctx->x[i]-ctx->ug[i];      /*  the scaled variables */
      ctx->res[2*i]   = ctx->og[i]-ctx->x[i] ;
    }
    for ( i = 1 ; i<=ctx->nlin ; i++ )
    {
      term = o8sc3(1,ctx->n,i,ctx->gres,ctx->x);  /*   the gradients of the linear         */
                                   /*   constraints are scaled accordingly  */
                                   /*   in o8st already                     */
      ctx->res[2*(ctx->n+i)-1] = term - ctx->low[i+ctx->n] ;
      ctx->res[2*(ctx->n+i)]   = ctx->up[i+ctx->n] - term ;
      ctx->cres[i] += 1 ;
    }
    /*  ------- >  evaluate _all_ nonlinear constraints                     */
    for ( i = 1 ; i <= ctx->nonlin ; i++ )
    {
       ctx->confuerr[i] = FALSE ;
    }
    escon(1,ctx->clist,ctx->x,ctx->o8opti_con,ctx->confuerr, ctx); ON_ERR;
    /*  1. parameter = 1 makes second parameter meaningless                 */
    /*                                                                      */

    /*  this evaluates the nonlinear constraints at x */
    for ( i = 1 ; i <= ctx->nonlin ; i++ ) ctx->cres[i+ctx->nlin] += 1 ;
    for ( i = 1 ; i <= ctx->nonlin ; i++ )
    {
      if ( ctx->confuerr[i] )
      {
        if ( ! ctx->silent ) o8msg(23, ctx);
        ctx->optite = -10;
        return;
      }
    }
    for ( i = 1 ; i <= ctx->nonlin ; i++ )
    {
      ctx->res[2*(i+ctx->n+ctx->nlin)-1] = ctx->o8opti_con[i]-ctx->low[i+ctx->n+ctx->nlin] ;
      ctx->res[2*(i+ctx->n+ctx->nlin)] =   ctx->up[i+ctx->n+ctx->nlin]-ctx->o8opti_con[i] ;
    }
    for ( i =1 ; i <= ctx->nres ; i++ )
/* check for active inequality constraints and compute the scaled and */
/* the unscaled penalty function                                      */
    {
       if ( ctx->low[i] == ctx->up[i] )
       {
          term  = fabs(ctx->res[2*i-1]);
          term1 = zero ;
       }
       else
       {
          term =  - min(zero,ctx->res[2*i-1]);
          term1=  - min(zero,ctx->res[2*i]);
          if ( ctx->res[2*i-1] <= ctx->delmin )
          {
            ctx->aalist[0]       += 1 ;
            ctx->aalist[ctx->aalist[0]] = 2*i-1 ;
            ctx->o8bind[2*i-1]     = 1 ;
          }
          if ( ctx->res[2*i] <= ctx->delmin )
          {
            ctx->aalist[0]       += 1 ;
            ctx->aalist[ctx->aalist[0]] = 2*i ;
            ctx->o8bind[2*i]       = 1 ;
          }
          if ( ctx->o8bind[2*i-1]+ctx->o8bind[2*i] == 2 )
          {
	        ctx->OPSlvErrorCode = OPSLV1_ERR_LOW_UP_BOUNDS_BINDING;
	        ctx->OPSlvErrorInfo = i;
            sprintf(ctx->ErrorText,"lower and upper bound are binding");
 	        return;
            /*fprintf(stderr," donlp2: lower and upper bound are binding ");
            fprintf(stdout," decrease delmin !\n ");
            exit(1);*/
          }
          if ( i > ctx->n  && (ctx->o8bind[2*i-1]+ctx->o8bind[2*i] == 1 ) )
          {
            ctx->alist[0] += 1 ;
            ctx->alist[ctx->alist[0]] = i-ctx->n ;
            if ( ctx->o8bind[2*i-1] == 1 )
            {
              ctx->gres[0][i-ctx->n] = one;
            }
            else
            {
              ctx->gres[0][i-ctx->n] = - one ;
            }
          }
          if ( i > ctx->n+ctx->nlin  && (ctx->o8bind[2*i-1]+ctx->o8bind[2*i] == 1 ) )
          {
            ctx->clist[0] += 1 ;
            ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin ;
          }


       }
/* because of the definition of delmin the lower and the upper bound cannot */
/* be active simultaneously                                                 */
       ctx->upsi += term+term1 ;
       ctx->psi  += ctx->w[2*i-1]*term+ctx->w[2*i]*term1 ;
    }
    /* only the gradients of the nonlinear constraints need to be evaluated     */
    /* they are stored in gres[][nlin+clist[]]                                  */
    escongrad(ctx->clist,ctx->nlin,ctx->x,ctx->gres, ctx); ON_ERR;
    for ( i = 1 ; i <= ctx->clist[0] ; i++ )
    {
       ctx->cgres[ctx->clist[i]+ctx->nlin] += 1 ;
       ctx->val[ctx->clist[i]+ctx->nlin] = TRUE ;
       ctx->gresn[ctx->clist[i]+ctx->nlin] = max ( one , sqrt(o8sc4(1,ctx->n,ctx->clist[i]+ctx->nlin,ctx->gres)) );
    }

    /* end first evaluation of all contraints */
    L100:

    /* ******************************************** */
    /* obtaining a point feasible within tau0 first */
    /* ******************************************** */

    if ( ctx->upsi >= ctx->tau0 )
    {
        ctx->scf   = zero;
        ctx->phase = -1;
    }
    else
    {
        ctx->ffuerr = FALSE;

        esf(ctx->x,&ctx->fx, ctx); ON_ERR;
        ctx->icf += 1 ;
        if ( ctx->ffuerr )
        {
            if ( ! ctx->silent ) o8msg(22, ctx);
            ctx->optite = -9;

            return;
        }
        if ( ! ctx->val[0] )
        {

            /* we assume that the gradient evaluation can be done whenever */
            /* the function has been evaluated                             */

            esgradf(ctx->x,ctx->gradf, ctx); ON_ERR;
            ctx->icgf += 1 ;
            ctx->val[0] = TRUE;
        }
        ctx->scf    = one;
        ctx->phase  = 0;
        ctx->fxst   = ctx->fx;
        ctx->psist  = ctx->psi;
        ctx->upsist = ctx->upsi;
        for (j = 1 ; j <= 2*ctx->nres ; j++)
        {
            ctx->resst[j] = ctx->res[j];
        }
        ctx->eta = zero;
    }
    L200:

    /* *************************************** */
    /* main iteration loop: getting a better x */
    /* *************************************** */
    if ( ctx->itstep > 0 )
    {
       cont = FALSE ;
       newx( ctx->x , ctx->u , ctx->itstep , ctx->accinf , &cont, ctx);
       if ( ! cont )
       {
         ctx->optite = 8 ;
         return ;
       }
    }
    if ( ! ctx->ident )
    {
        ctx->itstep = ctx->itstep+1;
        if ( ctx->itstep > ctx->iterma )
        {
            ctx->optite = -three;
            ctx->itstep = ctx->iterma;
            ctx->b2n    = ctx->accinf[ctx->itstep][8];
            ctx->b2n0   = ctx->accinf[ctx->itstep][7];

            return;
        }
        qpnew  = FALSE;
        ctx->qpterm = 0;
        delold = ctx->del;
        ctx->del    = zero;
        ctx->b2n0   = -one;
        ctx->b2n    = -one;
        ctx->singul = FALSE;
        nperm  = FALSE;
        for (i = 1 ; i <= ctx->n ; i++)
        {
            nperm   = nperm || (ctx->perm[i] != ctx->perm1[i]);
            ctx->perm[i] = ctx->perm1[i];
            ctx->diag[i] = zero;
        }
    }
    /* ********************************************************* */
    /* current valid row permutation for QR-decomposition of     */
    /* matrix of binding gradients in order to obtain continuity */
    /* of the QR-decomposition is given by perm                  */
    /* ********************************************************* */

    ctx->nr    = ctx->aalist[0];
    nrbas = ctx->nr;
    for (i = 1 ; i <= 2*ctx->nres ; i++)
    {
        ctx->o8opti_bindba[i] = ctx->o8bind[i];
    }
    for (j = 1 ; j <= 32 ; j++)
    {
        ctx->accinf[ctx->itstep][j] = zero;
    }
    ctx->gfn = o8vecn(1,ctx->n,ctx->gradf);

    /* compute new weight of objective function if useful */

    if ( ctx->nres > 0 && ctx->phase >= 0 && ! ctx->ident && ctx->itstep > 1 &&
        ( (ctx->accinf[ctx->itstep-1][10] == -1. && ctx->scf0 == one )
        || ctx->accinf[ctx->itstep-1][10] == 1. ) )
    {

        /* try rescaling the objective function */

        term = zero;
        for (i = 1 ; i <= ctx->nlin+ctx->nonlin ; i++)
        {
             term = max(term,ctx->gresn[i]);
        }
        scfh = term/max(one/ctx->scfmax,ctx->gfn);
        if ( scfh < one/ctx->scfmax ) scfh = one/ctx->scfmax;
        if ( scfh > ctx->scfmax     ) scfh = ctx->scfmax;
        if ( (ctx->fxst-ctx->fx)*scfh+scfh/ctx->scf*(ctx->psist-ctx->psi) >=
            scfh/ctx->scf*ctx->eta*ctx->clow && ctx->lastch <= ctx->itstep-4
            && (scfh < tm1*ctx->scf || scfh > tp1*ctx->scf) )
        {

            /* rescale the objective function if this seems promising and the */
            /* change is significant                                          */

            ctx->clow  = ctx->clow+1;
            term  = scfh/ctx->scf;
            ctx->psi   = ctx->psi*term;
            ctx->psist = ctx->psist*term;
            for (i = 1 ; i <= 2*ctx->nres ; i++)
            {
                ctx->u[i] = ctx->u[i]*term;
            }
            unorm  = unorm*term;
            ctx->scf    = scfh;
            ctx->lastch = ctx->itstep;
            term   = sqrt(term);
            for (i = 1 ; i <= ctx->n ; i++)
            {
                ctx->diag0[i] = term*ctx->diag0[i];
                for (j = 1 ; j <= ctx->n ; j++)
                {
                    ctx->a[j][i] = ctx->a[j][i]*term;
                }
            }
            ctx->matsc = ctx->matsc*term;
            if ( ! ctx->silent ) o8msg(2, ctx);
        }
    }
    ctx->accinf[ctx->itstep][1] = ctx->itstep;
    ctx->accinf[ctx->itstep][2] = ctx->fx;
    ctx->accinf[ctx->itstep][3] = ctx->scf;
    ctx->accinf[ctx->itstep][4] = ctx->psi;
    ctx->accinf[ctx->itstep][5] = ctx->upsi;

    if ( ! ctx->silent ) o8info(1, ctx);

    /* begin solver */

    /* *********************************************** */
    /* QR-decomposition of matrix of binding gradients */
    /* *********************************************** */

    if ( ctx->nr >= 1 )
    {
        o8dec(1,ctx->nr, ctx);
    }
    else
    {
        ctx->rank = 0;
    }
    o8left(ctx->a,ctx->gradf,ctx->o8opti_yy,&term,ctx->n);

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->qgf[i] = ctx->o8opti_yy[ctx->perm[i]];
    }
    o8ht(1,0,1,ctx->rank,ctx->n,ctx->qr,ctx->betaq,ctx->qgf,ctx->o8opti_trvec);

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->qgf[i] = ctx->o8opti_trvec[i];
    }
    if ( ctx->rank != ctx->nr && ! ctx->silent ) o8msg(1, ctx);

    /* ******************************************************* */
    /* compute del as function of x (forcing infeasibility and */
    /* the projected gradient to zero)                         */
    /* ******************************************************* */

    ctx->b2n0 = o8vecn(ctx->rank+1,ctx->n,ctx->qgf);

    sum  = zero;
    for (i = 1 ; i <= ctx->nres ; i++)
    {
        if ( ctx->low[i]==ctx->up[i] )
        {
            term = fabs(ctx->res[2*i]);
            if ( i > ctx->n ) term /= ctx->gresn[i-ctx->n];
            sum += term;
        }
        else
        {
           term = -min(zero,ctx->res[2*i-1]);
           term1 = -min(zero,ctx->res[2*i]);
           if ( i > ctx->n )
           {
              term /= ctx->gresn[i-ctx->n];
              term1 /= ctx->gresn[i-ctx->n];
           }
           sum += term + term1;
        }
    }
    /*  sum is unweighted  infeasibility, using scaled constraints */
    if ( ctx->itstep > 1 && ctx->accinf[ctx->itstep-1][8] >= zero
        && ! etaini && ctx->accinf[ctx->itstep-1][18] >= 0 )
    {
        etaini = TRUE;
        ctx->eta    = (ctx->accinf[ctx->itstep-1][8]/max(one,ctx->gfn)+sum
                 +min(one,slackn)+min(one,fabs(uminsc)))/min(30*ctx->n,ctx->iterma);
        ctx->level  = ctx->eta;
    }
    if ( ctx->itstep > 1 )
    {
        delx = ctx->delmin;
    }
    else
    {
        delx=ctx->del01;
    }
    term = ctx->scf*(ctx->fx0-ctx->fx)+ctx->psi0-ctx->psi;
    if ( ctx->itstep > 1 && term > zero && ctx->scf != zero )
       delx = max(delx,exp(p7*p7*log(term)));
    if ( ctx->scf == zero ) delx = min(ctx->del0*tm4,max(delx,ctx->upsi*tm2));
    delsig = ctx->delmin;

    /* del should be large enough to include constraints hit in */
    /* step before violis comes from unimin                     */

    for (i = 1 ; i <= ctx->violis[0] ; i++)
    {
        j      = ctx->violis[i];
        denom = ( (j+1)/2 > ctx->n ) ? ctx->gresn[(j+1)/2-ctx->n] : ctx->xsc[(j+1)/2] ;
        delsig = max(delsig,ctx->res[j]/denom*(one+tm1));
    }
    ctx->del = min(ctx->del0,max(min(delsig,five*delx),delx));


    if ( ctx->violis[0] == 0 ) ctx->del = min(ctx->del,ctx->del01);

    /* ********************************************* */
    /* if phase = 2 don't loose a binding constraint */
    /* phase = 2 implies o8opti_delist[0] = 0               */
    /* ********************************************* */

    if ( ctx->phase == 2 && ctx->violis[0] == 0 )
    {
        for (i = 1 ; i <= ctx->nres ; i++)
        {
            if ( !( ctx->low[i]==ctx->up[i]) )
            {
              term = zero ;
              if ( ctx->o8bind0[2*i-1] == 1 ) term=fabs(ctx->res[2*i-1]);
              if ( ctx->o8bind0[2*i] == 1 ) term=fabs(ctx->res[2*i]);
              if ( i > ctx->n ) term /= ctx->gresn[i-ctx->n];
              else term /= max(one,ctx->xsc[i]);

              ctx->del = min(ctx->del01,max(ctx->del,term));
            }
        }
    }

    term = ctx->del;
    for (i = 1 ; i <= ctx->o8opti_delist[0] ; i++)
    {
        j     = ctx->o8opti_delist[i];
        denom = ( (j+1)/2 > ctx->n ) ? ctx->gresn[(j+1)/2-ctx->n] : ctx->xsc[(j+1)/2];
        term1 = ctx->res[j]/denom*(one-tm2);
        if ( term1 >= ctx->del*tm2 ) term = min(term,term1);
    }
    ctx->del = term;

    /* if del becomes too large, we may loose complementary slackness */

    if ( ctx->itstep > 1 && ! ctx->ident && ctx->scf != zero )
    {
        compl = zero;
        for (i = 1 ; i <= ctx->nres ; i++)
        {
            if ( !(ctx->low[i] == ctx->up[i] ) )
            {
               term = ctx->res[2*i-1];
               term1 = ctx->res[2*i];
               if ( i > ctx->n )
               {
                 term /= ctx->gresn[i-ctx->n];
                 term1 /= ctx->gresn[i-ctx->n];
               }
               else
               {
                 term /= max(one,ctx->xsc[i]);
                 term1 /= max(one,ctx->xsc[i]);
               }
               term = max ( zero , term-ctx->delmin );
               term1 = max ( zero , term1 - ctx->delmin ) ;
               term *= max(zero,ctx->u[2*i-1]-ctx->smallw);
               term1 *= max(zero,ctx->u[2*i]-ctx->smallw);
               if ( i > ctx->n )
               {
                 term /= ctx->gresn[i-ctx->n];
                 term1 /= ctx->gresn[i-ctx->n];
               }
               else
               {
                 term /= max(one,ctx->xsc[i]);
                 term1 /= max(one,ctx->xsc[i]);
               }

               compl += term+term1 ;
            }
        }
        if ( compl > zero )
        {
           for (i = 1 ; i <= ctx->nres ; i++)
           {
              if ( !(ctx->low[i] == ctx->up[i]) )
              {
                term = ctx->res[2*i-1];
                term1 = ctx->res[2*i];
                if ( i > ctx->n )
                {
                  term /= ctx->gresn[i-ctx->n];
                  term1 /= ctx->gresn[i-ctx->n];
                }
                else
                {
                 term /= max(one,ctx->xsc[i]);
                 term1 /= max(one,ctx->xsc[i]);
                }

                if ( ctx->u[2*i-1] > ctx->smallw && term > tp2*ctx->delmin )
                {
                    ctx->del = max(ctx->delmin*tp2,
                              min(ctx->del,term*(one-tm2)));
                }
                if ( ctx->u[2*i] > ctx->smallw && term1 > tp2*ctx->delmin )
                {
                    ctx->del = max(ctx->delmin*tp2,
                              min(ctx->del,term1*(one-tm2)));
                }
              }
           }
        }
    }
    /* if the current step was singular and not successful,try a greater del */
    /* the same, if qpterm in the last step did signal trouble, but          */
    /* stepsize selection was nevertheless successful                        */

    if ( ctx->itstep > 1 && ctx->accinf[ctx->itstep-1][30] < 0. ) ctx->del = min(tp1*delold,ctx->del0);

    /* ********************************************* */
    /* include nearly binding inequality constraints */
    /* ********************************************* */
    ctx->clist[0] = 0 ;
    /* clist is the list of nonlinear constraints to be included additionally */
    for (i = 1 ; i <= ctx->nres ; i++)
    {
       if ( !(ctx->low[i] == ctx->up[i] ) )
       {
        term = ctx->res[2*i-1];
        term1 = ctx->res[2*i];
        if ( i > ctx->n )
        {
          term /= ctx->gresn[i-ctx->n];
          term1 /= ctx->gresn[i-ctx->n];
        }
        else
        {
          term /=max(one,ctx->xsc[i]);
          term1 /=max(one,ctx->xsc[i]);
        }
        if ( min(term,term1) <= ctx->del && (ctx->o8bind[2*i-1]+ctx->o8bind[2*i] ==0 ) )
        {
          j=2*i-1;
          if ( term1 < term ) j=2*i;
          /* it may be useful to include constraints>0 if near its boundary */
          /* but avoid it, if the constraint was in the old delete-list     */
          /* constraint j is a candidate for inclusion in the active set    */
          /* check previous delete_list
/* inactive
          for ( k = 1 ; k <= o8opti_delist[0] ; k++ )
          {
             if ( j == o8opti_delist[k] ) j = -1;
          }
end inactive */
          if ( j >= 1 )
          /*  that means the value of j was not found in o8opti_delist */
          {
             ctx->o8bind[j]         = 1;
             ctx->aalist[0]       += 1;
             ctx->aalist[ctx->aalist[0]] = j;
             if ( j > 2*ctx->n )
             {
               ctx->alist[0] += 1 ;
               k = (j-2*ctx->n+1)/2 ;
               ctx->alist[ctx->alist[0]] = k ;
               if ( j % 2 == 0 )
               {
                 ctx->gres[0][k] = -one ;
               }
               else
               {
                 ctx->gres[0][k] = one ;
               }
             }
             if ( j > 2*(ctx->n+ctx->nlin) )
             {
               ctx->clist[0]       += 1 ;
               ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin ;
             }
          }
        }
       }
    }
    if ( ctx->clist[0] > 0 )
    {
       escongrad(ctx->clist,ctx->nlin,ctx->x,ctx->gres, ctx); ON_ERR;
       for ( i = 1 ; i <= ctx->clist[0] ; i++ )
       {
         ctx->cgres[ctx->clist[i]+ctx->nlin] += 1 ;
         ctx->val[ctx->clist[i]+ctx->nlin]    = TRUE ;
         ctx->gresn[ctx->clist[i]+ctx->nlin] = max(one,sqrt(o8sc4(1,ctx->n,ctx->clist[i]+ctx->nlin,ctx->gres)));
       }

    }

    rank0 = ctx->rank;
    nr0   = ctx->nr;
    ctx->nr    = ctx->aalist[0] ;

    o8dec(nr0+1,ctx->nr, ctx);

    if ( ctx->rank != ctx->nr && ! ctx->silent ) o8msg(3, ctx);

    o8ht(1,0,rank0+1,ctx->rank,ctx->n,ctx->qr,ctx->betaq,ctx->qgf,ctx->o8opti_trvec);

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->qgf[i] = ctx->o8opti_trvec[i];
    }
    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->o8opti_yy[i] = -ctx->qgf[i]*ctx->scf;
    }
    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->yu[i] = zero;
    }
    /* ********************************************* */
    /* first computation of lagrangian multipliers u */
    /* ********************************************* */

    o8sol(1,ctx->rank,ctx->o8opti_yy,ctx->yu, ctx);

    umin  = zero;
    unorm = zero;
    for (i = 1 ; i <= 2*ctx->nres ; i++)
    {
        ctx->u[i] = zero;
    }
    iumin  = 0;
    uminsc = zero;
    for (i = 1 ; i <= ctx->rank ; i++)
    {
        unorm = max(unorm,fabs(ctx->yu[i]));
        k     = ctx->aalist[ctx->colno[i]];
        ctx->u[k]  = -ctx->yu[i];
        term = one ;
        if ( k > 2*ctx->n ) term=ctx->gresn[(k-2*ctx->n+1)/2];
        if ( !(ctx->low[(k+1)/2] == ctx->up[(k+1)/2]) )
        {
            if ( -ctx->yu[i]/term < uminsc )
            {
                iumin  = k;
                uminsc = -ctx->yu[i]/term;
            }
        }
    }
    if ( ctx->scf != zero )
    {
        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->o8opti_yx[i] = ctx->scf*ctx->gradf[i];
            for (j = 1 ; j <= ctx->nlin+ctx->nonlin ; j++)
            {
                ctx->o8opti_yx[i] = ctx->o8opti_yx[i]-ctx->gres[i][j]*(ctx->u[2*j-1+2*ctx->n]-ctx->u[2*j+2*ctx->n]);
            }
        }
        for (i = 1 ; i <= ctx->n ; i++ )
        {
                ctx->o8opti_yx[i] -= ctx->xsc[i]*(ctx->u[2*i-1]-ctx->u[2*i]) ;
        }
    /*  the l2-norm of the original unscaled gradient of the lagrangian */
        ctx->b2n = o8vecn(1,ctx->n,ctx->o8opti_yx)/ctx->scf;
    }
    else
    {
        ctx->b2n = -one;
    }
    if ( ! ctx->silent ) o8info(2, ctx);
    if ( ! ctx->silent ) o8info(3, ctx);

    /* compute new delta */

    del1 = ctx->del;
    if ( ctx->b2n > zero ) del1 = max(ctx->del,tm1*min(ctx->del0,
    exp(p7*log( fabs(ctx->b2n)/(ctx->gfn+one) + max(zero,-uminsc) + sum)) ));

    /* exclude constraints which were candidates for inactivating in the */
    /* previous step if useful                                           */

    for (i = 1 ; i <= ctx->o8opti_delist[0] ; i++)
    {
        j     = ctx->o8opti_delist[i];
        term1 = ctx->res[j]*(one-tm2) ;
        if ( j > 2*ctx->n )
        {
           term1 /= ctx->gresn[(j-2*ctx->n+1)/2];
        }
        else
        {
           term1 /= max(one,ctx->xsc[(j+1)/2]);
        }
        if ( term1 >= del1*tm2 ) del1 = max(ctx->delmin,min(del1,term1));
    }

    slackn = zero;
    for (i = 1 ; i <= ctx->nres ; i++)
    {
        term = ctx->res[2*i-1];
        term1 = ctx->res[2*i];
        denom = one ;
        if ( i > ctx->n ) denom = ctx->gresn[i-ctx->n];
        slackn +=
           max(zero,term/denom-ctx->delmin)*max(zero,ctx->u[2*i-1]-ctx->smallw)/denom
          +max(zero,term1/denom-ctx->delmin)*max(zero,ctx->u[2*i]-ctx->smallw)/denom;
    }
    if ( ctx->upsi <= ctx->delmin && ctx->b2n <= ctx->epsx*(ctx->gfn+one)
        && ctx->b2n != -one  && uminsc >= -ctx->smallw &&
        slackn <= ctx->delmin*ctx->smallw*ctx->nres )
    {

        /* sufficient accuracy in Kuhn-Tucker conditions */

        ctx->optite = zero;

        return;
    }
    /* include additional constraints if necessary */

    l0 = ctx->aalist[0];
    ctx->clist[0]=0;
    for (i = 1 ; i <= ctx->nres ; i++)
    {
      if ( ctx->low[i] != ctx->up[i] )
      {
        term = ctx->res[2*i-1];
        term1 = ctx->res[2*i];
        if ( i > ctx->n )
        {
          term /= ctx->gresn[i-ctx->n];
          term1 /= ctx->gresn[i-ctx->n];
        }
        else
        {
          term /= max(one,ctx->xsc[i]);
          term1 /= max(one,ctx->xsc[i]);
        }
        if ( min(term,term1) <= del1 && (ctx->o8bind[2*i-1]+ctx->o8bind[2*i] == 0 ))
        {

            if ( term <= term1 )
            {
              ctx->o8bind[2*i-1]         = 1;
              ctx->aalist[0]        = ctx->aalist[0]+1;
              ctx->aalist[ctx->aalist[0]] = 2*i-1;
              if ( i > ctx->n )
              {
                ctx->alist[0] += 1 ;
                ctx->alist[ctx->alist[0]] = i-ctx->n ;
              }
              if ( i > ctx->n+ctx->nlin )
              {
                 ctx->clist[0] +=1 ;
                 ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin;
              }
            }
            else
            {
              ctx->o8bind[2*i]         = 1;
              ctx->aalist[0]        = ctx->aalist[0]+1;
              ctx->aalist[ctx->aalist[0]] = 2*i;

              if ( i > ctx->n )
              {
                ctx->alist[0] += 1 ;
                ctx->alist[ctx->alist[0]] = i-ctx->n ;
              }
              if ( i > ctx->n+ctx->nlin )
              {
                 ctx->clist[0] +=1 ;
                 ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin;
              }
            }
        }
      }
    } /* end for i */

    ctx->del = del1;
    ctx->accinf[ctx->itstep][6]  = ctx->del;
    ctx->accinf[ctx->itstep][7]  = ctx->b2n0;
    ctx->accinf[ctx->itstep][9]  = ctx->aalist[0];
    ctx->accinf[ctx->itstep][10] = -1.;
    ctx->nr = ctx->aalist[0];
    if ( ctx->clist[0] != 0 )
    {
       escongrad(ctx->clist,ctx->nlin,ctx->x,ctx->gres, ctx); ON_ERR;
       for ( i = 1 ; i <= ctx->clist[0] ; i++ )
       {
         ctx->cgres[ctx->clist[i]+ctx->nlin] += 1 ;
         ctx->val[ctx->clist[i]+ctx->nlin]    = TRUE ;
         ctx->gresn[ctx->clist[i]+ctx->nlin] = max(one,sqrt(o8sc4(1,ctx->n,ctx->clist[i]+ctx->nlin,ctx->gres)));
       }
    }

    if ( l0 != ctx->nr )
    {
        rank0 = ctx->rank;

        o8dec(l0+1,ctx->nr, ctx);

        o8ht(1,0,rank0+1,ctx->rank,ctx->n,ctx->qr,ctx->betaq,ctx->qgf,ctx->o8opti_trvec);

        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->qgf[i] = ctx->o8opti_trvec[i];
        }
    }

    if ( ctx->rank != ctx->nr )
    {
        if ( ! ctx->silent ) o8msg(4, ctx);

        goto L400;
    }
    /* ******************************************************** */
    /* second solution for multipliers, rank may have changed ! */
    /* ******************************************************** */

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->o8opti_yy[i] = -ctx->qgf[i]*ctx->scf;
    }
    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->yu[i] = zero;
    }
    o8sol(1,ctx->rank,ctx->o8opti_yy,ctx->yu, ctx);

    /* remember the column interchanges in qr! yu[i] corresponds to */
    /* u[aalist[colno[i]]]                                          */

    umin  = zero;
    unorm = zero;
    for (i = 1 ; i <= 2*ctx->nres ; i++)
    {
        ctx->u[i] = zero;
    }
    iumin  = 0;
    uminsc = zero;
    for (i = 1 ; i <= ctx->rank ; i++)
    {
        unorm = max(unorm,fabs(ctx->yu[i]));
        k     = ctx->aalist[ctx->colno[i]];
        ctx->u[k]  = -ctx->yu[i];
        term = one ;
        term =  ( k > 2*ctx->n )? ctx->gresn[(k-2*ctx->n+1)/2] : ctx->xsc[(k+1)/2];
        if ( !(ctx->low[(k+1)/2] == ctx->up[(k+1)/2]) )
        {
            if ( -ctx->yu[i]/term < uminsc )
            {
                iumin  = k;
                uminsc = -ctx->yu[i]/term;
            }
        }

    }
    if ( ctx->scf != zero )
    {

        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->o8opti_yx[i] = ctx->scf*ctx->gradf[i];
            for (j = 1 ; j <= ctx->nlin+ctx->nonlin ; j++)
            {
                ctx->o8opti_yx[i] -= ctx->gres[i][j]*(ctx->u[2*j-1+2*ctx->n]-ctx->u[2*j+2*ctx->n]);
            }
        }
        for (i = 1 ; i <= ctx->n ; i++ )
        {
                ctx->o8opti_yx[i] -= ctx->xsc[i]*(ctx->u[2*i-1]-ctx->u[2*i]) ;
        }
        ctx->b2n = o8vecn(1,ctx->n,ctx->o8opti_yx)/ctx->scf;
    }
    ctx->accinf[ctx->itstep][8]  = ctx->b2n;
    ctx->accinf[ctx->itstep][11] = umin;

    o8shms(ctx);


    if ( ! ctx->silent )
    {
      o8info(4, ctx);
      if ( l0 != ctx->nr ) o8info(2, ctx);
    }

    ctx->o8opti_delist[0] = 0;
    if ( ctx->phase >= 0 && ctx->b2n != -one )
    {
        if ( fabs(uminsc) >= max(ctx->smallw,fabs(ctx->b2n)/(ctx->gfn+one)*ctx->c1d))
        {
            for (i = 1 ; i <= ctx->nr ; i++)
            {
                k = ctx->aalist[ctx->colno[i]];
                term = one ;
                if ( k > 2*ctx->n ) term = ctx->gresn[(k-2*ctx->n+1)/2];
                if ( !(ctx->low[(k+1)/2] == ctx->up[(k+1)/2]) )
                {
                   if ( -ctx->yu[i]/term <= -ctx->smallw)
                   {
                     ctx->o8opti_delist[0]         = ctx->o8opti_delist[0]+1;
                     ctx->o8opti_delist[ctx->o8opti_delist[0]] = k;
                   }
                }
            }
        }
    }
    /* *********************************************************** */
    /* the new o8opti_delist doesn't influence the current d but only the */
    /* computation of the next del                                 */
    /* *********************************************************** */

    ctx->eqres = TRUE;
    for (i = 1 ; i <= 2*ctx->nres ; i ++)
    {
        ctx->eqres = ctx->eqres && ( ctx->o8bind[i] == ctx->o8bind0[i] );
    }
    /* compute condition number estimators of diag-r and diag of */
    /* Cholesky-decomposition of b                               */

    if ( ctx->nr > 1 )
    {
        term  = zero;
        term1 = one;
        for (i = 1 ; i <= ctx->nr ; i++)
        {
            term  = max(term, fabs(ctx->diag[i]));
            term1 = min(term1,fabs(ctx->diag[i]));
        }
        ctx->accinf[ctx->itstep][13] = term/term1;
    }
    else if ( ctx->nr == 1 )
    {
        ctx->accinf[ctx->itstep][13] = 1.;
    }
    else
    {
        ctx->accinf[ctx->itstep][13] = -1.;
    }
    term  = fabs(ctx->a[1][1]);
    term1 = fabs(ctx->a[1][1]);
    i     = 2;
    while ( i <= ctx->n )
    {
        term  = max(term, fabs(ctx->a[i][i]));
        term1 = min(term1,fabs(ctx->a[i][i]));
        i     = i+1;
    }
    ctx->accinf[ctx->itstep][14] = pow(term/term1,2);

    if ( ! ctx->silent ) o8info(5, ctx);

    /* since a represents the Cholesky-factor, this square */
    slackn = zero;
    for (i = 1 ; i <= ctx->nres ; i++)
    {
        term = ctx->res[2*i-1];
        term1 = ctx->res[2*i];
        denom = (i > ctx->n ) ? ctx->gresn[i-ctx->n] : max(one,ctx->xsc[i]);
        slackn +=
           max(zero,term/denom-ctx->delmin)*max(zero,ctx->u[2*i-1]-ctx->smallw)/denom
          +max(zero,term1/denom-ctx->delmin)*max(zero,ctx->u[2*i]-ctx->smallw)/denom;
    }

    if ( umin >= -ctx->smallw &&
        slackn <= ctx->delmin*ctx->smallw*ctx->nres &&
        ctx->upsi <= ctx->nres*ctx->delmin && ctx->upsi0 <= ctx->nres*ctx->delmin
        && fabs(ctx->fx-ctx->fx0) <= eps*(fabs(ctx->fx)+one) &&
        ctx->b2n != -one &&
        ctx->b2n <= tp2*ctx->epsx*(ctx->gfn+one) )
    {

        csdifx = csdifx+1;
    }
    else
    {
        csdifx = 0;
    }
    if ( ctx->phase >= 0 && (ctx->accinf[ctx->itstep][14] > tp3 || ! ctx->analyt ) && csdifx > ctx->n )
    {
        ctx->optite = four;

        /* to avoid possible slow convergence with singular    */
        /* projected hessian or inaccurate numerical gradients */

        return;
    }
    /* compute damping factor for tangential component if upsi>tau0/2 */

    ctx->scf0 = one;
    if ( ctx->phase >= 0 && ctx->upsi > ctx->tau0*p5 )
         ctx->scf0 = max(one/ctx->scfmax,(two*(ctx->tau0-ctx->upsi)/ctx->tau0)*ctx->upsi*tm1/max(one,ctx->gfn) )/ctx->scf;
    ctx->accinf[ctx->itstep][15] = ctx->scf0;

    /* **************************** */
    /* compute tangential component */
    /* **************************** */

    for (i = ctx->nr+1 ; i <= ctx->n ; i++)
    {
        ctx->o8opti_qtx[i] = ctx->o8opti_yy[i]*ctx->scf0;
    }
    /* o8opti_qtx[nr+1],..,o8opti_qtx[n] is s2 */

    /* **************************************************** */
    /* compute right hand side and vertical component       */
    /* use damping for inactivation direction if very large */
    /* no indirect inactivation if infeasibility large and  */
    /* we are not almost stationary on the current manifold */
    /* **************************************************** */

    fac = one;
    if ( -umin*ctx->c1d > ctx->b2n+ctx->upsi && ctx->b2n != -one ) fac = ctx->c1d;
    if ( ctx->upsi > ctx->tau0*p5 ) fac = zero;
    for (i = 1 ; i <= ctx->nr ; i++)
    {
        k    = ctx->aalist[ctx->colno[i]];
        term = ctx->res[k];
        if ( !(ctx->low[(k+1)/2] == ctx->up[(k+1)/2]) )
        {
          if (  -ctx->yu[i] < zero && term > zero ) term = -term;
          if (  -ctx->yu[i] < zero ) term = term-ctx->yu[i]*fac;
        }
        ctx->o8opti_yx[i] = -term;
    }
    o8solt(1,ctx->nr,ctx->o8opti_yx,ctx->o8opti_qtx, ctx);

    /* o8opti_qtx is transformed direction of descent for phi */

    o8ht(-1,0,1,ctx->nr,ctx->n,ctx->qr,ctx->betaq,ctx->o8opti_qtx,ctx->o8opti_yx);

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->o8opti_qtx[ctx->perm[i]] = ctx->o8opti_yx[i];
    }

    /* solve l(transp)*d = o8opti_qtx, l = a = Cholesky-factor of b */

    o8rght(ctx->a,ctx->o8opti_qtx,ctx->d,&term,ctx->n);

    /* end solver */

    /* compute new penalty weights : regular case */

    clwold = ctx->clow;
    if ( ctx->phase >= 0 ) o8sce(ctx);
    if ( ctx->clow > clwold )
    {

        /* tau_qp depends on the (new) weights */

        term = ctx->w[1];
        for (i = 1 ; i <= 2*ctx->nres ; i++)
        {
            term = max(term,ctx->w[i]);
        }
        ctx->tauqp = max(one,min(ctx->tauqp,term));
    }
    /* compute parameter phase and stopping criterion */

    if ( uminsc < -ctx->smallw )     ctx->phase = min(1,ctx->phase);
    if ( ! ctx->eqres )              ctx->phase = min(0,ctx->phase);
    if ( ctx->eqres && ctx->upsi < ctx->tau0 ) ctx->phase = max(1,ctx->phase);

    /* rescale and project d if appropriate */

    o8cutd(ctx);

    /* compute the directional derivative dirder */

    o8dird(ctx);

    /* terminate if correction is small */

    if ( ctx->dnorm <= ctx->epsx*(ctx->xnorm+ctx->epsx) && ctx->upsi <= ctx->delmin
        && ctx->b2n != -one && uminsc >= -ctx->smallw && ctx->b2n <= ctx->epsx*(ctx->gfn+one) )
    {

        ctx->optite = one;

        return;
    }
    L350:

    /* reenter from the singular case: dirder has been computed already */

    ctx->accinf[ctx->itstep][16] = ctx->xnorm;
    ctx->accinf[ctx->itstep][17] = ctx->dnorm;
    ctx->accinf[ctx->itstep][18] = ctx->phase;

    /* compute stepsize */

    ctx->cfincr = ctx->icf;

    /* if no descent direction is obtained, check whether restarting the method */
    /* might help                                                               */

    if ( ctx->dirder >= zero )
    {

        /* no direction of descent */

        if ( ! ctx->silent ) o8msg(11, ctx);
        ctx->stptrm = -two;
        ctx->sig    = zero;

        goto L360;
    }
    /* if directional derivative correct but very small, terminate */
    /* since no further progress might be possible                 */

    if ( -ctx->dirder <= ctx->epsmac*tp2*(ctx->scf*fabs(ctx->fx)+ctx->psi+one) )
    {
        if ( ctx->upsi > ctx->delmin*ctx->nres )
        {
            ctx->optite = -one;
            ctx->stptrm = -one;
        }
        else
        {
            ctx->optite = two;
            ctx->stptrm = one;
        }
        ctx->sig = zero;

        goto L360;
    }
    /* phase = 2 : we may hope to obtain superlinear convergence */
    /* switch to Maratos-correction is on then                   */
    /* return to phase = 1 if first order correction large       */

    if ( ctx->phase >= 1 && ctx->dnorm <= ctx->smalld*(ctx->xnorm+ctx->smalld) && ctx->scf0 == one &&
        uminsc >= -ctx->smallw && ! ctx->singul) ctx->phase = 2;

    /* return to phase 1 since correction large again */

    if ( ctx->phase == 2 && ctx->dnorm > (ctx->xnorm+ctx->smalld) ) ctx->phase = 1;

    o8smax(ctx);

    /* stmaxl is the maximal stepsize such that point on projected */
    /* ray changes with sigma, but sigla at most                   */

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->dd[i] = zero;
        ctx->x1[i] = ctx->x[i]+ctx->d[i];
    }
    /* compute second order correction of infeasibility if useful    */
    /* if x1 already violates the bounds by viobnd, suppress it      */
    /* also suppress it if becomes too large compared with the first */
    /* order correction                                              */
    viobnd = FALSE ;
    for ( i = 1 ; i<= ctx->n ; i++ )
    {
      if ( (ctx->llow[i] && ctx->x1[i] < ctx->ug[i]-ctx->taubnd) ||
           (ctx->lup[i] && ctx->x1[i] > ctx->og[i]+ctx->taubnd )  )
      { viobnd = TRUE ;
      }
    }

    if ( ctx->phase == 2 && ctx->dnorm > sqrt(ctx->xnorm*ctx->epsmac)  && ! ctx->singul
         && ! viobnd && ctx->nres > 0 )
    {
    /* compute the Maratos correction using a difference approximation */
        if ( ctx->bloc )
        {
          ctx->valid = FALSE ;
          /* user_eval must reset valid */
          user_eval(ctx->x1,-1, ctx); ON_ERR;
        }

        /* only function values, from xtr = xsc*x1 */
        ctx->clist[0] = 0 ;
        for (i = 1 ; i <= ctx->aalist[0] ; i++)
        {
        /* here only the active constraints are evaluated  and */
        /* stored in o8opti_yx                                        */
             ctx->o8opti_yx[i] = zero;
             k = ctx->aalist[i];
             if ( k > 2*(ctx->n+ctx->nlin) )
             /* for the linear constraints the function value at x1 is zero */
             /* anyway                                                      */
             {
               ctx->clist[0] += 1 ;
               ctx->clist[ctx->clist[0]] = (k+1)/2-ctx->n-ctx->nlin;
             }
        }
  /*  evaluate nonlinear active constraints from clist only         */
        for ( j = 1 ; j <= ctx->clist[0] ; j++ )
        {
           ctx->confuerr[ctx->clist[j]] = FALSE ;
           ctx->cres[ctx->clist[j]+ctx->nlin] += 1 ;
        }
        escon(2,ctx->clist,ctx->x1,ctx->o8opti_con,ctx->confuerr, ctx); ON_ERR;
  /*                                                                */

        for ( j = 1 ; j <= ctx->clist[0] ; j++ )
        {
          if ( ctx->confuerr[ctx->clist[j]] ) goto L355;
        }
  /* if no error occured, computation of o8opti_yx is now to be completed */
        for ( i = 1 ; i <= ctx->aalist[0] ; i++ )
        {
           k = ctx->aalist[i];
           if ( k > 2*(ctx->n+ctx->nlin) )
           {
             if ( k % 2 == 0 )
             {
               ctx->o8opti_yx[i] =  ctx->up[(k+1)/2]-ctx->o8opti_con[(k+1)/2-ctx->n-ctx->nlin];
             }
             else
             {
               ctx->o8opti_yx[i] = ctx->o8opti_con[(k+1)/2-ctx->n-ctx->nlin]-ctx->low[(k+1)/2];
             }
           }
        }
        for (i = 1 ; i <= ctx->aalist[0] ; i++)
        {
            ctx->o8opti_yy[i] = -ctx->o8opti_yx[ctx->colno[i]];
        }
        o8solt(1,ctx->nr,ctx->o8opti_yy,ctx->dd, ctx);

        for (i = ctx->nr+1 ; i <= ctx->n ; i++)
        {
            ctx->dd[i] = zero;
        }
        o8ht(-1,0,1,ctx->nr,ctx->n,ctx->qr,ctx->betaq,ctx->dd,ctx->o8opti_yx);

        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->dd[ctx->perm[i]] = ctx->o8opti_yx[i];
        }
        o8rght(ctx->a,ctx->dd,ctx->dd,&term,ctx->n);

        if ( sqrt(term) > p5*ctx->dnorm )
        {

            /* second order correction almost as large as first order one: */
            /* not useful                                                  */

            for (i = 1 ; i <= ctx->n ; i++)
            {
                ctx->dd[i] = zero;
            }
            if ( ! ctx->silent ) o8msg(6, ctx);
        }
    }
    L355:

    if ( ! ctx->silent ) o8info(7, ctx);

    ctx->sig = min(one,ctx->stmaxl);

    o8unim(ctx->sig, ctx);

    L360:

    ctx->cfincr = ctx->icf-ctx->cfincr;
    if ( ! ctx->silent ) o8info(10, ctx);

    /* count successive small steps */

    term = ctx->scf*(ctx->fx0-ctx->fx)+ctx->psi0-ctx->psi;
    if ( fabs(term) <= ctx->epsmac*tp3*(ctx->scf*fabs(ctx->fx)+ctx->psi) )
    {
        csmdph = csmdph+1;
    }
    else
    {
        csmdph = 0;
    }
    /* csmdph counts contiguous small differences of penalty function phi */

    if ( csmdph > max(ctx->n,10) )
    {
        ctx->optite = seven;

        return;
    }
    if ( ctx->sig <= five*tm2 )
    {
        if ( ctx->sig0 <= five*tm2 ) csssig += 1;
    }
    else
    {
        csssig = 0;
    }
    /* csssig counts the number of successive small sig's */

    ctx->accinf[ctx->itstep][21] = ctx->sig;
    ctx->accinf[ctx->itstep][22] = ctx->cfincr;
    ctx->accinf[ctx->itstep][23] = ctx->dirder;
    ctx->accinf[ctx->itstep][24] = ctx->dscal;
    ctx->accinf[ctx->itstep][25] = ctx->cosphi;
    ctx->accinf[ctx->itstep][26] = ctx->violis[0];

    if ( ctx->sig == zero && ctx->stptrm == one && ctx->optite == two )
    {

        /* no further significant progress possible */

        if ( ! ctx->silent ) o8info(17, ctx);

        return;
    }
    if ( ctx->stptrm == one && ctx->sig <= tm4 && ctx->accinf[ctx->itstep][13] > tp4 && ! ctx->singul
        && ctx->nres > 0)
    {

        /* try a regularized step, hopefully this will give a better d */
        /* and larger sig                                              */

        if ( ctx->accinf[ctx->itstep][14] > tp4 ) o8inim(ctx);
        ctx->ident  = TRUE;
        ctx->singul = TRUE;

        goto L400;
    }
    if ( ctx->stptrm < zero )
    {

        /* stepsize selection failed */

        if ( ! ctx->ident )
        {

            /* try restart with a = identity scaled */

            if ( ! ctx->silent ) o8msg(7, ctx);
            ctx->ident     = TRUE;
            ctx->o8opti_delist[0] = 0;
            ctx->violis[0] = 0;
            csreg     = 0;
            csssig    = 0;
            csirup    = 0;

            o8inim(ctx);

            ctx->aalist[0] = nrbas;
            for (i = 1 ; i <= 2*ctx->nres ; i++)
            {
                ctx->o8bind[i] = ctx->o8opti_bindba[i];
            }
            if ( ctx->upsi >= ctx->tau0 )
            {

                goto L100;

            }
            else
            {

                goto L200;
            }
        }
        if ( ! ctx->singul && ctx->ident && ctx->accinf[ctx->itstep][13] > tp4 && ctx->nres > 0 )
        {

            /* try the full SQP-direction               */
            /* this may be the third try for this point */

            ctx->singul = TRUE;
            ctx->ident  = TRUE;

            goto L400;
        }
        if ( ctx->stptrm == -two )
        {
            ctx->optite = -four;

            return;
        }
        if ( ctx->sig == zero && ctx->optite == -one ) return;

        /* unidimensional search unsuccessfully terminated */

        ctx->optite = -two;

        return;
    }
    if ( ctx->singul && ctx->itstep > ctx->n && fabs(ctx->fx-ctx->fx0) <= eps*(fabs(ctx->fx)+one) && ctx->phase >= 0
        && ctx->upsi <= ctx->nres*ctx->delmin && ctx->upsi0 <= ctx->nres*ctx->delmin
        && slackn <= ctx->delmin*ctx->smallw*ctx->nres && ctx->infeas <= ctx->upsi && ! ctx->ident )
    {

        /* since multipliers may be incorrect for infeas != zero be careful */

        ctx->optite = four;

        /* avoid slow progress in case of singular constraints */

        return;
    }
    /* relaxed termination criteria in the singular case */

    if ( ctx->singul && ctx->upsi <= ctx->delmin*ctx->nres && ctx->upsi0 <= ctx->delmin*ctx->nres
         && ctx->b2n != -one && ctx->b2n <= (ctx->gfn+one)*ctx->epsx*tp2 && ctx->phase >= 0
         && slackn <= ctx->delmin*ctx->smallw*ctx->nres && ctx->infeas <= ctx->upsi )
    {

        /* since multipliers may be incorrect for infeas != zero be careful */

        ctx->optite = three;

        return;
    }
    k = 0;
    for (i = 1 ; i <= ctx->n ; i++)
    {
        if ( fabs(ctx->difx[i]) >= ctx->epsx*(fabs(ctx->x[i])+tm2) ) k = 1;
    }
    if ( k == 0 )
    {
        cschgx = cschgx+1;
    }
    else
    {
        cschgx = 0;
    }
    if ( cschgx > ctx->nreset && ctx->singul )
    {

        /* very slow progress in x in the singular case. terminate */

        ctx->optite = five;

        return;
    }
    /* new value of x has been accepted */

    ctx->xnorm = o8vecn(1,ctx->n,ctx->x);
    ctx->ident = FALSE;

    o8egph(ctx->gphi0, ctx);

    for (i = 1 ; i <= ctx->nonlin ; i++)
    {
      ctx->val[i+ctx->nlin] = FALSE;
    }
    ctx->val[0] = FALSE ;
    /* in bloc mode , all constraints are evaluated in the user interface */
    /* the subsequent escongrad simply is then move from fugrad to gres   */


  /* ****************************************                               */
    if ( ctx->bloc )
    {
      ctx->valid = FALSE ;
      /* user_eval must reset valid */
      user_eval(ctx->x,2, ctx); ON_ERR;

    }
  /* ****************************************                               */


    /* evaluate gradients only, since function values are already */
    /* valid from unidimensional minimization                     */
    /* argument is xtr = xsc*x                                    */

    if ( ctx->phase >= 0  )
    {
        ctx->val[0] = TRUE;
        esgradf(ctx->x,ctx->gradf, ctx); ON_ERR;
        ctx->icgf += 1;
    }
  /* evaluate gradients of nonlinear constraints  at the new point   */
  /* in case of a bloc evaluation this is simply a move from fugrad  */
    ctx->clist[0] = 0 ;
    for ( i = 1 ; i <= ctx->aalist[0] ; i++ )
    {
      k = ctx->aalist[i];
      if ( k > 2*(ctx->n+ctx->nlin) )
      {
         ctx->clist[0] += 1 ;
         ctx->clist[ctx->clist[0]] = (k+1)/2-ctx->n-ctx->nlin;
         if ( k % 2 == 0 )
         {
           ctx->gres[0][(k+1)/2-ctx->n] = -one ;
         }
         else
         {
           ctx->gres[0][(k+1)/2-ctx->n] = one ;
         }
      }
    }
    if ( ctx->clist[0] != 0 )
    {
  /*  this evaluates the gradients of the nonlinear constraints which were    */
  /*  active at the old point also at the new point (necessary for the update)*/

       escongrad(ctx->clist,ctx->nlin,ctx->x,ctx->gres, ctx); ON_ERR;
       for ( i = 1 ; i <= ctx->clist[0] ; i++ )
       {
         ctx->cgres[ctx->clist[i]+ctx->nlin] += 1 ;
         ctx->val[ctx->clist[i]+ctx->nlin]    = TRUE ;
         ctx->gresn[ctx->clist[i]+ctx->nlin] = max(one,sqrt(o8sc4(1,ctx->n,ctx->clist[i]+ctx->nlin,ctx->gres)));
       }
    }
    o8egph(ctx->gphi1, ctx);

    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->o8opti_yx[i] = ctx->x[i]-ctx->x0[i];
        ctx->o8opti_yy[i] = ctx->gphi1[i]-ctx->gphi0[i];
    }
    /* since a represents the Cholesky-factor, this sqrt */

    term = sqrt(o8vecn(1,ctx->n,ctx->o8opti_yy)/o8vecn(1,ctx->n,ctx->o8opti_yx));
    if ( term != zero && ctx->phase >= 0 ) ctx->matsc = max(one/ctx->scfmax,min(ctx->scfmax,term/2));

    /* current scaling of identity in case of restart */

    ctx->aalist[0] = 0;
    ctx->clist[0] = 0 ;
    ctx->alist[0] = 0 ;
    /* clist is the list of nonlinear active constraints whose gradients */
    /* need to be evaluated                                              */
    for (i = 1 ; i <= ctx->nres ; i++)
    {
        ctx->u0[2*i-1]    = ctx->u[2*i-1];
        ctx->o8bind0[2*i-1] = ctx->o8bind[2*i-1];
        ctx->res0[2*i-1]  = ctx->res[2*i-1];
        ctx->u0[2*i]    = ctx->u[2*i];
        ctx->o8bind0[2*i] = ctx->o8bind[2*i];
        ctx->res0[2*i]  = ctx->res[2*i];
        if ( ctx->low[i] == ctx->up[i] )
        {
            ctx->aalist[0] += 1 ;
            ctx->aalist[ctx->aalist[0]] = 2*i-1 ;
            ctx->o8bind[2*i-1] = 1;
            ctx->o8bind[2*i]   = 0;
            if ( i > ctx->n+ctx->nlin  && ! ctx->val[i-ctx->n] )
            {
              ctx->clist[0] += 1 ;
              ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin ;
            }
            if ( i > ctx->n )
            {
              ctx->alist[0] +=1 ;
              ctx->alist[ctx->alist[0]] = i-ctx->n;
            }
        }
        else
        {
            ctx->o8bind[2*i-1] = 0;
            ctx->o8bind[2*i]   = 0;
            term = ctx->res[2*i-1];
            term1 = ctx->res[2*i] ;
            if ( i > ctx->n )
            {
              term /= ctx->gresn[i-ctx->n];
              term1 /= ctx->gresn[i-ctx->n];
            }
            else
            {
              term /= max(one,ctx->xsc[i]);
              term1 /= max(one,ctx->xsc[i]);
            }
            if ( min(term,term1) <= ctx->delmin )
            {
  /* an active inequality constraint at the new point    */
  /* may be the gradient has been evaluated already (for computing the update)*/
              if ( term <= term1 )
              {
                 ctx->o8bind[2*i-1] = 1;
                 ctx->aalist[0] += 1;
                 ctx->aalist[ctx->aalist[0]] = 2*i-1 ;
                 if ( i > ctx->n )
                 {
                   ctx->alist[0] +=1 ;
                   ctx->alist[ctx->alist[0]] = i-ctx->n ;
                   ctx->gres[0][i-ctx->n] = one ;
                   if ( i > ctx->n+ctx->nlin && ! ctx->val[i-ctx->n] )
                   {
                     ctx->clist[0] += 1;
                     ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin ;
                   }
                 }
              }
              else
              {
                 ctx->o8bind[2*i] = 1 ;
                 ctx->aalist[0] +=1 ;
                 ctx->aalist[ctx->aalist[0]] = 2*i ;
                 if ( i > ctx->n )
                 {
                   ctx->alist[0] +=1 ;
                   ctx->alist[ctx->alist[0]] = i-ctx->n ;
                   ctx->gres[0][i-ctx->n] = -one ;
                   if ( i > ctx->n+ctx->nlin && ! ctx->val[i-ctx->n] )
                   {
                     ctx->clist[0] += 1;
                     ctx->clist[ctx->clist[0]] = i-ctx->n-ctx->nlin ;
                   }
                 }
              }
            } /* end an active constraint */
        } /* end inequality case */
    } /* end for i */

    if ( ctx->clist[0] != 0 )
    {
  /*  this evaluates the gradients of the nonlinear constraints which were    */
  /*  active at the new point but not at the old point                        */

       escongrad(ctx->clist,ctx->nlin,ctx->x,ctx->gres, ctx); ON_ERR;
       for ( i = 1 ; i <= ctx->clist[0] ; i++ )
       {
         ctx->cgres[ctx->clist[i]+ctx->nlin] += 1 ;
         ctx->val[ctx->clist[i]+ctx->nlin]    = TRUE ;
         ctx->gresn[ctx->clist[i]+ctx->nlin] = max(one,sqrt(o8sc4(1,ctx->n,ctx->clist[i]+ctx->nlin,ctx->gres)));
       }
    }

    /* **** o8bind now corresponds to the state of the new point               */
    /* **** but there may be gradients evaluated at the new point            */
    /* **** not yet put to bind                                              */
    /* ****                                                                  */
    /* **** update the unprojected quasi-Newton-matrix anyway                */
    /* ****                                                                  */

    if ( ctx->scf != zero )
    {
        if ( csirup > ctx->nreset || csssig > ctx->nreset || csreg  > ctx->nreset )
        {
            csreg  = 0;
            csssig = 0;
            csirup = 0;

            o8inim(ctx);

        }
        else
        {

            o8bfgs(ctx);

            /* for projected update:  if ( ! silent ) o8info(13) */
            /* for Pantoja&Mayne update:                         */

            if ( ! ctx->silent ) o8info(14, ctx);
        }
    }
    /* proceed */

    if ( ctx->accinf[ctx->itstep][27] == one )
    {
        if ( ctx->itstep > 1 &&
            ctx->accinf[ctx->itstep-1][29] != zero && ctx->accinf[ctx->itstep][29] != zero )
        {

            /* c ount s uccessive ir regular up dates */

            csirup = csirup+1;
        }
        else
        {
            csirup = 0;
        }
    }
    /* accinf[itstep][27] = 1   update Pantoja&Mayne                   */
    /*                    = 0   noupdate                               */
    /*                    = -1  restart                                */
    /*                    = 2   normal BFGS (nr = 0)                   */
    /*                    = 3   Powell's modified BFGS (nr = 0)        */
    /* accinf[itstep][28] = modification term tk/ den2/den1  resp.     */
    /* accinf[itstep][29] = modification factor th/xsik resp.          */
    /* csirup counts the number of successive irregular updating steps */

    if ( ctx->phase == -1 )
    {

        goto L100;

    }
    else
    {

        goto L200;
    }
    L400:

    ctx->singul = TRUE;
    ctx->phase  = min(ctx->phase,0);
    ctx->accinf[ctx->itstep][10] = one;

    /* try to compute a descent direction using                       */
    /* an extended quadratic program with                             */
    /* individual slack variable for any constraint                   */
    /* compute damping factor for tangential component if upsi>tau0/2 */
    /* by rescaling f if possible                                     */

    ctx->scf0 = one;
    if ( ctx->phase >= 0 && ctx->upsi > ctx->tau0*p5 )
    {
        scfh = max(one/ctx->scfmax,
               min(ctx->scfmax,(two*(ctx->tau0-ctx->upsi)/ctx->tau0)*ctx->upsi*ctx->tau/max(one,ctx->gfn) ) );
        if ( (ctx->fxst-ctx->fx)*scfh+scfh/ctx->scf*(ctx->psist-ctx->psi) >=
             scfh/ctx->scf*ctx->eta*ctx->clow && ctx->lastch <= ctx->itstep-4
             && (scfh < tm1*ctx->scf || scfh > tp1*ctx->scf) )
        {

            /* rescale the objective function if this seems promising and */
            /* the change is significant                                  */

            ctx->clow  = ctx->clow+1;
            term  = scfh/ctx->scf;
            ctx->scf0  = term ;
            ctx->psi   = ctx->psi*term;
            ctx->psist = ctx->psist*term;
            for (i = 1 ; i <= 2*ctx->nres ; i++)
            {
                ctx->u[i] = ctx->u[i]*term;
            }
            unorm  = unorm*term;
            ctx->scf    = scfh;
            ctx->lastch = ctx->itstep;
            ctx->accinf[ctx->itstep][15] = ctx->scf;
            term = sqrt(term);
            for ( i = 1 ; i <= ctx->n ; i++)
            {
                ctx->diag0[i] = term*ctx->diag0[i];
                for ( j = 1 ; j <= ctx->n ; j++)
                {
                    ctx->a[j][i] = ctx->a[j][i]*term;
                }
            }
            ctx->matsc = ctx->matsc*term;
            if ( ! ctx->silent ) o8msg(2, ctx);
        }
    }
    /* slack is upsi at most */

    ctx->accinf[ctx->itstep][32] = ctx->upsi;
    if ( ! ctx->silent ) o8info(15, ctx);

    ctx->accinf[ctx->itstep][13] = -one;
    term  = fabs(ctx->a[1][1]);
    term1 = term;
    i     = 2;
    while ( i <= ctx->n )
    {
        term  = max(term ,fabs(ctx->a[i][i]));
        term1 = min(term1,fabs(ctx->a[i][i]));
        i     = i+1;
    }
    ctx->accinf[ctx->itstep][14] = pow(term/term1,2);
    if ( ! ctx->silent ) o8info(5, ctx);
    clwold = ctx->clow;

    /* save for restart */

    tauqp0 = ctx->tauqp;
    for (i = 1 ; i <= 2*ctx->nres ; i++)
    {
        ctx->u[i] = zero;
    }
    /* no second order correction here */
    for (i = 1 ; i <= ctx->n ; i++)
    {
        ctx->dd[i] = zero;
    }
    /* solve full qp */
    o8qpdu(ctx);

    if ( ctx->dnorm == zero && ctx->qpterm == 1 && ctx->optite == three ) return;
    if ( ctx->dnorm <= ctx->epsx*(min(ctx->xnorm,one)+ctx->epsx) && ctx->qpterm < 0 )
    {   /* QP solver indicated trouble */

        /* may be it failed because of illconditioning */

        if ( ctx->upsi >= ctx->nres*ctx->delmin && qpnew )
        {

            /* restarting the method has been done already: game is over */

            ctx->optite = -one;
            if ( ! ctx->silent ) o8info(18, ctx);

            return;
        }
        if ( qpnew )
        {
            ctx->optite = ctx->qpterm-five;
            if ( ! ctx->silent ) o8info(18, ctx);

            return;
        }
        /* try a = id */

        qpnew = TRUE;
        for (i = 1 ; i <= 2*ctx->nres ; i++)
        {
            ctx->w[i] = one;
        }
        ctx->lastch = ctx->itstep;

        o8inim(ctx);

        ctx->ident = TRUE;

        /* formerly tauqp = tauqp0 */

        ctx->tauqp    = one;
        ctx->aalist[0] = nrbas;
        for (i = 1 ; i <= 2*ctx->nres ; i++)
        {
            ctx->o8bind[i] = ctx->o8opti_bindba[i];
        }
        if ( ctx->scf == zero )
        {

            goto L100;

        }
        else
        {

            goto L200;
        }
    }
    ctx->accinf[ctx->itstep][11] = zero;
    ctx->o8opti_delist[0] = 0;
    umin      = zero;

    /* b2n is defined also internally in o8qpdu */

    o8shms(ctx);

    if ( (ctx->qpterm >= 0 || ctx->qpterm == -3) && ctx->scf != zero )
    {
        unorm = fabs(ctx->u[1]);
        for (i = 2 ; i <= 2*ctx->nres ; i++)
        {
            unorm = max(unorm,fabs(ctx->u[i]));
        }
        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->o8opti_yx[i] = ctx->scf*ctx->gradf[i];
            for (j = 1 ; j <= ctx->nlin+ctx->nonlin ; j++)
            {
                ctx->o8opti_yx[i] -= ctx->gres[i][j]*(ctx->u[2*j-1+2*ctx->n]-ctx->u[2*j+2*ctx->n]);
            }
        }
        for (i = 1 ; i <= ctx->n ; i++ )
        {
            ctx->o8opti_yx[i] -= ctx->xsc[i]*(ctx->u[2*i-1]-ctx->u[2*i]) ;
        }
        ctx->b2n = o8vecn(1,ctx->n,ctx->o8opti_yx)/ctx->scf;
    }
    else
    {
        ctx->b2n = -one;

        /* signals "undefined" here */
    }
    if ( ! ctx->silent ) o8info(2, ctx);
    if ( ! ctx->silent ) o8info(16, ctx);

    /* to avoid termination */

    if ( ctx->b2n == -one ) ctx->b2n = ctx->epsmac/ctx->tolmac;
    if ( ctx->qpterm >= 0 && ctx->dnorm <= tm2*ctx->epsx*(ctx->epsx+min(one,ctx->xnorm)))
    {
        if ( ctx->upsi <= ctx->nres*ctx->delmin )
        {
            ctx->optite = six;
        }
        else
        {
            ctx->optite = -five;
        }
        return;
    }
    /* check whether QPsolver terminated unsuccessfully */

    if ( ctx->qpterm < 0 )
    {

        /* we have a unfeasible solution for QP. try it */
        /* but don't complain if it fails               */

        if ( ! ctx->silent ) o8msg(10, ctx);
    }
    if ( ctx->clow > clwold )
    {
        term = one;
        for (i = 1 ; i <= 2*ctx->nres ; i++)
        {
            term = max(term,ctx->w[i]);
        }
        ctx->tauqp = max(one,term);
    }
    if ( ctx->tauqp > pow(ctx->taufac,3)*tauqp0 ) ctx->tauqp = pow(ctx->taufac,3)*tauqp0;

    /* no change of tauqp otherwise */

    ctx->b2n0   = ctx->b2n;
    umin   = zero;
    uminsc = zero;
    if ( ctx->qpterm >= 1 )
    {
      slackn = zero;
      for (i = 1 ; i <= ctx->nres ; i++)
      {
        term = ctx->res[2*i-1];
        term1 = ctx->res[2*i];
        denom = one ;
        if ( i > ctx->n ) denom = ctx->gresn[i-ctx->n];
        slackn +=
            max(zero,term/denom-ctx->delmin)*max(zero,ctx->u[2*i-1]-ctx->smallw)/denom
           +max(zero,term1/denom-ctx->delmin)*max(zero,ctx->u[2*i]-ctx->smallw)/denom;
      }

    }
    else
    {

        /* slack is undefined, since multipliers are undefined */
        /* use this value to prevent premature termination     */

        slackn = one;
    }
    goto L350;
}

void o8inim(TLContext *ctx)
{
/* ************************************************************************ */
/*      initialize the quasi Newton update with a multiple of the identity  */
/* ************************************************************************ */

     INTEGER i,j;

    for (i = 1 ; i <= ctx->n ; i++)
    {
        for (j = 1 ; j <= ctx->n ; j++)
        {
            ctx->a[j][i] = zero;
        }
        ctx->a[i][i]  = ctx->matsc;
        ctx->diag0[i] = ctx->matsc;
    }
    ctx->accinf[ctx->itstep][27] = -one;
    ctx->accinf[ctx->itstep][14] = one;
    if ( ! ctx->silent ) o8info(20, ctx);

    return;
}
void o8dird(TLContext *ctx)
{
/* **************************************************************************** */
/*          compute the directional derivative of the L1-penalty-function       */
/* **************************************************************************** */

     INTEGER  i;
     DOUBLE   term,term1;

    /* compute directional derivative of Zangwill function */

    L100:

    ctx->dirder = o8sc1(1,ctx->n,ctx->gradf,ctx->d)*ctx->scf;

    for (i = 1 ; i <= ctx->nres ; i++)
    {
       if ( i <= ctx->n )
       {
       /*  bound constraints                                    */
          if ( ctx->low[i] == ctx->up[i] )
          /*    equality constraint type h(x)=x(i)-low(i)=0     */
          {
            if ( ctx->res[2*i-1] <= -tp3*ctx->epsmac )
            {
               ctx->dirder -=  ctx->d[i]*ctx->w[2*i-1];
            }
            else if ( ctx->res[2*i-1] >= tp3*ctx->epsmac )
            {
               ctx->dirder += ctx->d[i]*ctx->w[2*i-1];
            }
            else
            {
               ctx->dirder += fabs(ctx->d[i])*ctx->w[2*i-1];
            }
          }
          else
          {
            if ( ctx->o8bind[2*i-1] == 1 )
            /*  active lower bound */
            {
              term = ctx->w[2*i-1]*ctx->d[i] ;
              if ( fabs(ctx->res[2*i-1]) <= tp3*ctx->epsmac )
              {
                ctx->dirder -=  min(zero,term);
              }
              else
              {
                if ( ctx->res[2*i-1] < -tp3*ctx->epsmac )
                {
                  term = min ( term , -ctx->res[2*i-1]*ctx->w[2*i-1] );
            /* penalty function can decrease at most by  -res[2*i-1]*w[2*i-1] */
                  ctx->dirder -= term ;
                }
              }
            }
            if ( ctx->o8bind[2*i] == 1 )
            {
              /* active upper bound */
              term = -ctx->d[i]*ctx->w[2*i];
              if ( fabs(ctx->res[2*i]) <= tp3*ctx->epsmac )
              {
                ctx->dirder -= min(zero,term);
              }
              else
              {
                if ( ctx->res[2*i] < -tp3*ctx->epsmac )
                {
                  term = min ( term , -ctx->res[2*i]*ctx->w[2*i] );
                  ctx->dirder -= term ;
                }
              }
            }
          }
       }
       else
       {
          term  = o8sc3(1,ctx->n,i-ctx->n,ctx->gres,ctx->d)*ctx->gres[0][i-ctx->n];
          if ( ctx->low[i] == ctx->up[i] )
          {
            if ( ctx->res[2*i-1] <= -tp3*ctx->epsmac )
            {
               ctx->dirder -=  term*ctx->w[2*i-1];
            }
            else if ( ctx->res[2*i-1] >= tp3*ctx->epsmac )
            {
               ctx->dirder += term*ctx->w[2*i-1];
            }
            else
            {
               ctx->dirder += fabs(term)*ctx->w[2*i-1];
            }
          }
          else
          {
            if ( ctx->o8bind[2*i-1] == 1 )
            {
              term = ctx->w[2*i-1]*term ;
              if ( fabs(ctx->res[2*i-1]) <= tp3*ctx->epsmac )
              {
                ctx->dirder -= min(zero,term);
              }
              else
              {
                if ( ctx->res[2*i-1] < -tp3*ctx->epsmac )
                {
                  term = min ( term , -ctx->res[2*i-1]*ctx->w[2*i-1] );
                  ctx->dirder -= term ;
                }
              }
            }
            if ( ctx->o8bind[2*i] == 1 )
            {
              term = term*ctx->w[2*i];
              if ( fabs(ctx->res[2*i]) <= tp3*ctx->epsmac )
              {
                ctx->dirder -= min(zero,term);
              }
              else
              {
                if ( ctx->res[2*i] < -tp3*ctx->epsmac )
                {
                  term = min ( term , -ctx->res[2*i]*ctx->w[2*i] );
                  ctx->dirder -= term ;
                }
              }
            }
          }
       }
    }
    return;
}

/* **************************************************************************** */
/*                      cut d if appropriate and rescale                        */
/* **************************************************************************** */
void o8cutd(TLContext *ctx)
{

     INTEGER  i;
     DOUBLE   term,term1;

    ctx->xnorm  = o8vecn(1,ctx->n,ctx->x);
    term   = ctx->beta*(ctx->xnorm+one);
    ctx->dnorm  = o8vecn(1,ctx->n,ctx->d);
    ctx->d0norm = o8vecn(1,ctx->n,ctx->d0);
    ctx->dscal  = one;
    if ( ctx->dnorm*ctx->d0norm != zero )
    {
        ctx->cosphi = o8sc1(1,ctx->n,ctx->d,ctx->d0)/(ctx->d0norm*ctx->dnorm);
    }
    else
    {
        ctx->cosphi = zero;
    }
    if ( ctx->dnorm > term )
    {

        /* d too long: rescale */

        term1 = term/ctx->dnorm;
        ctx->dnorm = term;
        ctx->dscal = term1;
        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->d[i]  = ctx->d[i]*term1;
            ctx->dd[i] = ctx->dd[i]*pow(term1,2);
        }
    }
    /* since we project the ray with respect to the bounds, be sure */
    /* to compute the directional derivative correctly              */
    /* therefore correct d and dd appropriately                     */

    for (i = 1 ; i <= ctx->n ; i++)
    {
        if ( ctx->llow[i] && ctx->x[i]+ctx->sigsm*ctx->d[i] <= ctx->ug[i] )
        {
            ctx->d[i]  = zero;
            ctx->dd[i] = max(zero,ctx->dd[i]);
        }
        if ( ctx->lup[i] && ctx->x[i]+ctx->sigsm*ctx->d[i] >= ctx->og[i] )
        {
            ctx->d[i]  = zero;
            ctx->dd[i] = min(zero,ctx->dd[i]);
        }
    }
    ctx->dnorm = o8vecn(1,ctx->n,ctx->d);

    return;
}

/* **************************************************************************** */
/* compute maximum stepsize stmaxl such that projection on the box of lower     */
/* and upper bounds changes for sig in [0,stmaxl],if such exists                */
/* **************************************************************************** */
void o8smax(TLContext *ctx) {

     INTEGER  i;
     LOGICAL  exis;
    /* exis is true if there exists some sigmaxl, such that the projection */
    /* of x+sigma*d on the feasible box does not change for sigma>=sigmaxl */
    /* that means no coordinate changes any more                           */
    exis = TRUE;

    for (i = 1 ; i <= ctx->n ; i++) {
        exis = exis &&( ( ctx->d[i] == zero )
            || ( ctx->lup[i]  && ctx->d[i] > zero )
            || ( ctx->llow[i] && ctx->d[i] < zero ) );
    }
    if ( exis )
    {
        ctx->stmaxl = ctx->sigsm;
        for (i = 1 ; i <= ctx->n ; i++) {
            if ( ctx->llow[i] && ctx->d[i] < zero ) {
                if ( -ctx->d[i]*ctx->sigla >= ctx->x[i]-ctx->ug[i] ) {
                    ctx->stmaxl = max(ctx->stmaxl,(ctx->x[i]-ctx->ug[i])/(-ctx->d[i]));
                } else {
                    ctx->stmaxl = ctx->sigla;
                }
            }
            if ( ctx->lup[i] && ctx->d[i] > zero ) {
                if ( ctx->d[i]*ctx->sigla >= ctx->og[i]-ctx->x[i] ) {
                    ctx->stmaxl = max(ctx->stmaxl,(ctx->og[i]-ctx->x[i])/ctx->d[i]);
                } else {
                    ctx->stmaxl = ctx->sigla;
                }
            }
        }
    } else {
        ctx->stmaxl = ctx->sigla;
    }
    /* but never use stepsize larger than sigla */

    ctx->stmaxl = min(ctx->sigla,ctx->stmaxl);

    return;
}

/* **************************************************************************** */
/*        restore the best point found so far to be the current new point       */
/* **************************************************************************** */
void o8rest(TLContext *ctx) {

     INTEGER  j;

    ctx->phi1  = ctx->phimin;
    ctx->psi1  = ctx->psimin;
    ctx->upsi1 = ctx->upsim;
    ctx->sig   = ctx->sigmin;
    ctx->fx1   = ctx->fmin_;
    for (j = 1 ; j <= ctx->n ; j++) {
        ctx->x1[j] = ctx->xmin[j];
    }
    for (j = 1 ; j <= 2*ctx->nres ; j++) {
        ctx->res1[j] = ctx->resmin[j];
    }
    return;
}

/* **************************************************************************** */
/*             save the best point found so far in ...min variables             */
/* **************************************************************************** */
void o8save(TLContext *ctx) {

     INTEGER  i;

    ctx->phimin = ctx->phi1;
    ctx->upsim  = ctx->upsi1;
    ctx->psimin = ctx->psi1;
    ctx->fmin_   = ctx->fx1;
    ctx->sigmin = ctx->sig;
    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->xmin[i] = ctx->x1[i];
    }
    for (i = 1 ; i <= 2*ctx->nres ; i++) {
        ctx->resmin[i] = ctx->res1[i];
    }
    return;
}

/* **************************************************************************** */
/*                    evaluate the functions at the new point                   */
/* **************************************************************************** */
void o8eval(DOUBLE sigact,DOUBLE *sigres,LOGICAL *reject,LOGICAL *error, TLContext *ctx)
{

     INTEGER  i,j;
     DOUBLE   term , denom ;
    /* liste as a formal placeholder, not used here since type of call to */
    /* escon is 1                                                         */
     INTEGER   liste[2] ;
     LOGICAL eval_err ;


    liste[0] = 0 ;
    liste[1] = 0 ;
    ctx->sig = sigact;
    for (i = 1 ; i <= ctx->n ; i ++)
    {
        ctx->x1[i] = ctx->x[i]+ctx->sig*(ctx->d[i]+ctx->sig*ctx->dd[i]);

        /* project with respect to the box-constraints */

        ctx->x1[i] = max(ctx->x1[i],ctx->ug[i]);
        ctx->x1[i] = min(ctx->x1[i],ctx->og[i]);
    }
    *reject = FALSE;
    *error  = FALSE;
    *sigres = ctx->sig;
    ctx->upsi1   = zero;
    ctx->psi1    = zero;

/**********                                            *******************/
    if ( ctx->bloc )
    {
      ctx->valid = FALSE ;
      /* user_eval must reset valid */
      user_eval(ctx->x1,-1, ctx); ON_ERR;
    }
/********** only function values, from xtr = x1*xsc    *******************/

/********** compute res1 = restrictions at x1     *******************/
    for ( i = 1 ; i <= ctx->n ; i++ )
    {
       ctx->res1[2*i-1] = ctx->x1[i]-ctx->ug[i];
       ctx->res1[2*i  ] = ctx->og[i]-ctx->x1[i];
    }
    for ( i = 1 ; i <= ctx->nlin ; i++ )
    {
       term = o8sc3(1,ctx->n,i,ctx->gres,ctx->x1);
       ctx->cres[i] += 1 ;
       ctx->res1[2*(i+ctx->n)-1] = term - ctx->low[ctx->n+i];
       ctx->res1[2*(i+ctx->n)  ] = ctx->up[ctx->n+i]-term;
    }
    for ( i = 1 ; i <= ctx->nonlin ; i++ )
    {
      ctx->confuerr[i] = FALSE ;
    }
    escon(1,liste,ctx->x1,ctx->o8eval_con,ctx->confuerr, ctx); ON_ERR;
    /* for call type 1, liste is dummy */
    eval_err = FALSE ;
    for (i = 1 ; i <= ctx->nonlin ; i++ )
    {
       ctx->cres[ctx->nlin+i] += 1 ;
       eval_err = eval_err || ctx->confuerr[i] ;
    }
    if ( eval_err )
    {
      *error = TRUE ;
      return ;
    }
    for ( i = 1 ; i <= ctx->nonlin ; i++ )
    {
       ctx->res1[2*(ctx->n+ctx->nlin+i)-1] = ctx->o8eval_con[i]-ctx->low[i+ctx->n+ctx->nlin];
       ctx->res1[2*(ctx->n+ctx->nlin+i)  ] = ctx->up[i+ctx->n+ctx->nlin]-ctx->o8eval_con[i];
    }

    for ( i = 1 ; i <= ctx->nres ; i++ )
    {
      if ( ctx->low[i] == ctx->up[i] )
      {
         term = fabs(ctx->res1[2*i-1]);
         ctx->upsi1 = ctx->upsi1+term;
         if ( ctx->upsi1 > ctx->tau0 && ctx->phase != -1 )
         {
            *reject = TRUE;
            return;
         }

         ctx->psi1 = ctx->psi1+term*ctx->w[2*i-1];
         denom = ( i<=ctx->n ) ? max(one,ctx->xsc[i]) : ctx->gresn[i-ctx->n];
         /*  only the odd numbered constraint is used for equalities */
         if ( ctx->res1[2*i-1]*ctx->res[2*i-1] < zero && ctx->sig <= one &&
              (fabs(ctx->res[2*i-1])/denom >= tp3*ctx->epsmac
               || fabs(ctx->res1[2*i-1])/denom >= tp3*ctx->epsmac ) )
            *sigres = min(*sigres,ctx->sig*ctx->res[2*i-1]/(ctx->res[2*i-1]-ctx->res1[2*i-1]));
      }
      else
      {
         term = -min(zero,ctx->res1[2*i-1]);
         if ( ctx->res1[2*i-1] < -ctx->delmin && ctx->o8bind[2*i-1] == 0 )
         {
           ctx->violis[0] +=1 ;
           ctx->violis[ctx->violis[0]] = 2*i-1 ;
         }

         ctx->upsi1 = ctx->upsi1+term;
         if ( ctx->upsi1 > ctx->tau0 && ctx->phase != -1 )
         {
            *reject = TRUE;
            return;
         }
         ctx->psi1 = ctx->psi1+term*ctx->w[2*i-1];
         denom = ( i <= ctx->n) ? max(one, ctx->xsc[i]) : ctx->gresn[i-ctx->n];
         if ( ctx->res1[2*i-1]*ctx->res[2*i-1] < zero && ctx->sig <= one &&
              ( ctx->o8bind[2*i-1] == 0 ||
              (ctx->o8bind[2*i-1] == 1 && (fabs(ctx->res[2*i-1])/denom >= tp3*ctx->epsmac
              || fabs(ctx->res1[2*i-1])/denom >= tp3*ctx->epsmac ) ) ) )
            *sigres = min(*sigres,ctx->sig*ctx->res[2*i-1]/(ctx->res[2*i-1]-ctx->res1[2*i-1]));

         term = -min(zero,ctx->res1[2*i]);
         ctx->upsi1 = ctx->upsi1+term;
         if ( ctx->res1[2*i] < -ctx->delmin && ctx->o8bind[2*i] == 0 )
         {
           ctx->violis[0] += 1;
           ctx->violis[ctx->violis[0]] = 2*i ;
         }

         if ( ctx->upsi1 > ctx->tau0 && ctx->phase != -1 )
         {
            *reject = TRUE;
            return;
         }
         ctx->psi1 = ctx->psi1+term*ctx->w[2*i];

         if ( ctx->res1[2*i]*ctx->res[2*i] < zero && ctx->sig <= one && ( ctx->o8bind[2*i] == 0 ||
            (ctx->o8bind[2*i] == 1 && (fabs(ctx->res[2*i])/denom >= tp3*ctx->epsmac
            || fabs(ctx->res1[2*i])/denom >= tp3*ctx->epsmac ) ) ) )
            *sigres = min(*sigres,ctx->sig*ctx->res[2*i]/(ctx->res[2*i]-ctx->res1[2*i]));

      }
    }
       /* violis is the list of constraints                            */
       /* not binding which have been hit during unidimensional search */



    if ( ctx->phase != -1 )
    {
        ctx->ffuerr = FALSE;

        esf(ctx->x1,&ctx->fx1, ctx); ON_ERR;
        ctx->icf +=  1 ;
        if ( ctx->ffuerr )
        {
            *error = TRUE;

            return;
        }
    }
    else
    {
        ctx->fx1 = zero;
    }
    ctx->phi1 = ctx->scf*ctx->fx1+ctx->psi1;

    return;
}

/* **************************************************************************** */
/*          determination of stepsize by an Armijo-like test for descent        */
/* **************************************************************************** */
void o8unim(DOUBLE sig1th, TLContext *ctx) {

    /* sig1th the first proposed stepsize for searching on the arc              */
    /* if sig = one did'nt work                                                 */

    /* n = number of variables                                                  */
    /* x = current point                                                        */
    /* d = direction of descent. dd = second order correction                   */
    /* x,d = input                                                              */
    /* x0,d0 etc. information from previous step                                */

    /* xnorm,dnorm = euclidean length of x and d                                */
    /* stptrm = 1 on success , = -1 or = -2 otherwise                           */
    /* sig    =  computed stepsize                                              */
    /* it is assumed that one is asymptotically optimal                         */
    /* sigsm = smallest acceptable stepsize                                     */
    /* sigla = largest  acceptable stepsize                                     */
    /* alpha = smallest feasible reduction factor for stepsize                  */
    /* delta = multiplier for derivative should be smaller than .25             */
    /* beta  = maximum feasible increase of x-norm for sig = 1                  */
    /* theta = bound for cos(angle(current direction, previous direction))      */
    /*         if overridden, stepsize larger than one is tried                 */

    /* ************************************************************************ */

     INTEGER  i,l,j;
     DOUBLE   term,maxphi;
     DOUBLE   sigres ,diff;
     LOGICAL  desc,descre,sminfe,lainfe,reject,error;


    ctx->o8unim_step[1] = p5;
    ctx->o8unim_step[2] = twom2;
    for (i = 3 ; i <= ctx->nstep ; i++) {
        ctx->o8unim_step[i] = tm1;
    }
    /* projection of d, rescaling and computing dirder has been done already */

    l         = 0;
    error     = FALSE;
    ctx->phi       = ctx->scf*ctx->fx+ctx->psi;
    ctx->sig       = sig1th;
    ctx->violis[0] = 0;
    if ( ! ctx->silent ) o8info(8, ctx);

    L100:

    l = l+1;
    if ( l > ctx->nstep ) {
        if ( error && ! ctx->silent ) o8msg(24, ctx);
        ctx->stptrm = -one;
        ctx->sig    = zero;

        return;
    }
    /* compute a new x and test for descent */

    o8eval(ctx->sig,&sigres,&reject,&error, ctx);

    if ( error ) {
        if ( ctx->sig > one ) {

            o8rest(ctx);

            goto L200;

        } else {
            if ( ! ctx->silent ) o8msg(25, ctx);
            ctx->sig = ctx->o8unim_step[l]*ctx->sig;

            goto L100;
        }
    }
    if ( reject ) {
        if ( ctx->sig > one ) {

            o8rest(ctx);

            goto L200;

        } else {
            ctx->sig = ctx->o8unim_step[l]*ctx->sig;

            goto L100;
        }
    }
    if ( ! ctx->silent ) o8info(9, ctx);

    /* new function value */

    if ( ctx->sig > one ) {
        if ( ctx->phi1 >= ctx->phimin ) {

            /* phi does'nt decrease further */

            o8rest(ctx);

            goto L200;

        } else {
            if ( ctx->sig < ctx->stmaxl ) {

                o8save(ctx);

                ctx->sig = min(ctx->stmaxl,ctx->sig+ctx->sig);

                goto L100;

            } else {

                goto L200;
            }
        }
    }
    if ( ctx->lastch >= ctx->itstep-3 || ctx->phase != 2 || ctx->singul ) {

        /* require monotonic behaviour */

        diff = ctx->phi-ctx->phi1;
    } else {
        maxphi = ctx->phi;
        for (j = 1 ; j <= 3 ; j++) {
            maxphi = max(ctx->scf*ctx->accinf[ctx->itstep-j][2]+ctx->accinf[ctx->itstep-j][4],maxphi);
        }
        diff = maxphi-ctx->phi1;
    }
    desc   = diff >= min(-ctx->sig*ctx->delta*ctx->dirder,ctx->level);
    descre = ctx->upsi - ctx->upsi1 >= ctx->sig*pow(ctx->delta,2)*ctx->upsi/ctx->tauqp;
    sminfe = ctx->upsi <= ctx->tau0*p5 && ctx->upsi1 <= ctx->tau0;
    lainfe = ctx->upsi > ctx->tau0*p5;

    if ( desc && ( sminfe || ( lainfe && descre ) ) ) {

        /* Goldstein-Armijo descent test satisfied */

        if ( ctx->sig == one && ( (ctx->cosphi >= ctx->theta && ctx->sig0 >= one
             && (ctx->phase+1)*(ctx->phase-2) != 0
             && ! ctx->singul) || diff >= -ctx->sig*ctx->delta1*ctx->dirder )
             && ctx->stmaxl > one && ctx->upsi < ctx->tau0*p5 ) {

            /* 1 >= delta1 >> delta > 0 */

            /* try stepsize larger than one */
            /* save the current point as the best one */

            o8save(ctx);

            ctx->sig = min(ctx->stmaxl,ctx->sig+ctx->sig);

            goto L100;
        }
        if ( ctx->sig <= one && ctx->upsi > ctx->tau0*p5 && ctx->upsi1 > ctx->upsi ) goto L300;

        goto L200;

    } else {

        goto L300;
    }
    L200:

    /* accept new x, save old values */

    ctx->fx0    = ctx->fx;
    ctx->fx     = ctx->fx1;
    ctx->upsi0  = ctx->upsi;
    ctx->upsi   = ctx->upsi1;
    ctx->psi0   = ctx->psi;
    ctx->psi    = ctx->psi1;
    ctx->stptrm = one;
    ctx->sig0   = ctx->sig;
    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->x0[i]   = ctx->x[i];
        ctx->d0[i]   = ctx->d[i];
        ctx->x[i]    = ctx->x1[i];
        ctx->difx[i] = ctx->x[i]-ctx->x0[i];
    }
    ctx->d0norm = ctx->dnorm;
    ctx->x0norm = ctx->xnorm;
    for (i = 1 ; i <= 2*ctx->nres ; i++) {
        ctx->res[i] = ctx->res1[i];
    }
    return;

    /* continue reducing sig */

    L300:

    if ( sigres < ctx->sig ) {
        ctx->sig = min(p5*ctx->sig,max(ctx->o8unim_step[l]*ctx->sig,sigres));
    } else {
        term = (diff-ctx->dirder*ctx->sig)*two;
        if ( term > ctx->epsmac*(ctx->scf*fabs(ctx->fx)+ctx->psi) ) {
            ctx->sig = min(p5*ctx->sig,max(ctx->o8unim_step[l]*ctx->sig,-ctx->dirder*pow(ctx->sig,2)/term));
        } else {
            ctx->sig = ctx->o8unim_step[l]*ctx->sig;
        }
    }


    if ( ctx->sig*max(one,ctx->dnorm) >= ctx->sigsm ) goto L100;
    ctx->stptrm = -one;
    ctx->sig    = zero;

    return;
}

/* **************************************************************************** */
/*                scalar product of two vectors or parts of vectors             */
/* **************************************************************************** */
DOUBLE o8sc1(INTEGER i,INTEGER j,DOUBLE a[],DOUBLE b[])
{

    /* multiply two vectors */

     INTEGER  k;
     DOUBLE   s;

    if ( i > j )
    {
        return (zero);
    }
    else
    {
        s = zero;
        for (k = i ; k <= j ; k++)
        {
            s += a[k]*b[k];
        }
        return (s);
    }
}

/* **************************************************************************** */
/*                    multiply row j of matrix a with vector b                  */
/* **************************************************************************** */
DOUBLE o8sc2(INTEGER n,INTEGER m,INTEGER j,DOUBLE **a,DOUBLE b[]) {

     DOUBLE   s;
     INTEGER  i;

    s = zero;
    for (i = n ; i <= m ; i++)
    {
        s = s+a[j][i]*b[i];
    }
    return (s);
}

/* **************************************************************************** */
/*          multiply column j section (n to m) of matrix a with vector b        */
/* **************************************************************************** */
DOUBLE o8sc3(INTEGER n,INTEGER m,INTEGER j,DOUBLE **a,DOUBLE b[])
{

     DOUBLE   s;
     INTEGER  i;

    s = zero;
    for (i = n ; i <= m ; i++) {
        s = s+a[i][j]*b[i];
    }
    return (s);
}
/*************************************************************************/
DOUBLE o8sc3b(INTEGER n,INTEGER m,INTEGER j,DOUBLE **a,DOUBLE b[])
{

     DOUBLE   s;
     INTEGER  i;

    s = zero;
    for (i = n ; i <= m ; i++) {
        s = s+a[i][j]*b[i];
    }
    return (s);
}

DOUBLE o8sc4(INTEGER n,INTEGER m,INTEGER j,DOUBLE **a)
{

     DOUBLE   s;
     INTEGER  i;

    s = zero;
    for (i = n ; i <= m ; i++) {
        s += pow(a[i][j],2);
    }
    return (s);
}

DOUBLE o8sc3_(INTEGER n,INTEGER m,INTEGER j,DOUBLE **a,DOUBLE b[]) {

     DOUBLE   s;
     INTEGER  i;

    s = zero;
    for (i = n ; i <= m ; i++) {
        s = s+a[i][j]*b[i];
    }
    return (s);
}

/* **************************************************************************** */
/* subprogram for structured output of a submatrix a[ma][na]                    */
/* on channel lognum in fix or float format with heading "head".                */
/* uses a fixed format string with 70 print columns                             */
/* **************************************************************************** */
void o8mdru(DOUBLE **a,INTEGER ma,INTEGER na,char head[],
            FILE *lognum,LOGICAL fix) {

     INTEGER  i,j,jo,ju;
     INTEGER  anz;

    fprintf(lognum,"\n%40s\n", head);
    anz = 4;
    jo  = 0;
    while ( jo < na ) {
        ju = jo+1;
        jo = min(ju+anz-1,na);

        fprintf(lognum,"\nrow/column");
        for (j = ju ; j <= jo ; j++) {
            fprintf(lognum,"      %3i      ", j);
            if ( (j-ju+1) % 4 == 0 || j == jo ) fprintf(lognum,"\n");
        }
        for (i = 1 ; i <= ma ; i++) {
            if ( fix ) {
                fprintf(lognum,"   %4i   ",i);
                for (j = ju ; j <= jo ; j++) {
                    fprintf(lognum,"%14.7f ", a[i][j]);
                    if ( (j-ju+1) % 4 == 0 || j == jo ) fprintf(lognum,"\n");
                }
            } else {
                fprintf(lognum,"   %4i   ",i);
                for (j = ju ; j <= jo ; j++) {
                    fprintf(lognum," %13.6e ", a[i][j]);
                    if ( (j-ju+1) % 4 == 0 || j == jo ) fprintf(lognum,"\n");
                }
            }
        }
    }
    return;
}
void o8mdru_(DOUBLE **a,INTEGER ma,INTEGER na,char head[],
             FILE *lognum,LOGICAL fix) {

     INTEGER  i,j,jo,ju;
     INTEGER  anz;

    fprintf(lognum,"\n%40s\n", head);
    anz = 4;
    jo  = 0;
    while ( jo < na ) {
        ju = jo+1;
        jo = min(ju+anz-1,na);

        fprintf(lognum,"\nrow/column");
        for (j = ju ; j <= jo ; j++) {
            fprintf(lognum,"      %3i      ", j);
            if ( (j-ju+1) % 4 == 0 || j == jo ) fprintf(lognum,"\n");
        }
        for (i = 1 ; i <= ma ; i++) {
            if ( fix ) {
                fprintf(lognum,"   %4i   ",i);
                for (j = ju ; j <= jo ; j++) {
                    fprintf(lognum,"%14.7f ", a[i][j]);
                    if ( (j-ju+1) % 4 == 0 || j == jo ) fprintf(lognum,"\n");
                }
            } else {
                fprintf(lognum,"   %4i   ",i);
                for (j = ju ; j <= jo ; j++) {
                    fprintf(lognum," %13.6e ", a[i][j]);
                    if ( (j-ju+1) % 4 == 0 || j == jo ) fprintf(lognum,"\n");
                }
            }
        }
    }
    return;
}

/* **************************************************************************** */
/*                        compute gradient of lagrangian                        */
/* **************************************************************************** */
void o8egph(DOUBLE gphi[], TLContext *ctx) {

     INTEGER  i,j,l,k;

    for (i = 1 ; i <= ctx->n ; i++)
    {
         gphi[i] = ctx->gradf[i] * ctx->scf;
         for (j = 1 ; j <= ctx->aalist[0] ; j++)
         {
             l = ctx->aalist[j];
             k = (l+1)/2;
             if( ctx->low[k] == ctx->up[k] )
             {
               if ( k > ctx->n )
               {
                 gphi[i] -= ctx->gres[i][k-ctx->n]*ctx->u[l] ;
               }
               else
               {
                 gphi[i] -= ctx->xsc[k]*ctx->u[l];
                 /* since an equality constraint is always odd numbered */
               }
             }
             else
             {
                if( ctx->u[l] > zero )
                {
                  if ( k > ctx->n )
                  {
                    gphi[i] = gphi[i]-ctx->gres[i][k-ctx->n]*ctx->gres[0][k-ctx->n]*ctx->u[l];
                  }
                  else
                  {
                    if ( l%2 == 0 )
                    {
                       gphi[i] += ctx->xsc[k]*ctx->u[l];
                    }
                    else
                    {
                       gphi[i] -= ctx->xsc[k]*ctx->u[l];
                    }
                  }
                }
             }

            /* include constraints, whose multipliers are of correct sign only */
         }
    }
    return;
}

/* detection and elimination of redundant equality constraints             */
/* this is done making them always inactive inequality constraints         */
/* by redefining low and up                                                */
void o8elim(TLContext *ctx) {
    INTEGER  n1,n2;
    INTEGER  i,j,k,l,i1,icur,ipiv;
    DOUBLE   sum,term,dalpha,dbeta,qrii;
    DOUBLE   curle;

    ctx->optite = 0 ; /* indicates success */
    icur = 0 ;
    for ( i=1 ; i<= ctx->n+ctx->nlin ; i++ )
    {
      /* search for linear equality constraints */
      if ( ctx->low[i] == ctx->up[i] )
      {
        icur += 1;
        if ( i <= ctx->n )
        {
          for ( j = 1 ; j <= ctx->n ; j++ )
          {
            ctx->o8elim_qri[j] = zero ;
          }
          ctx->o8elim_qri[i] = ctx->xsc[i]  ;
        }
        else
        {
          for ( j = 1 ; j <= ctx->n ; j++ )
          {
             ctx->o8elim_qri[j] = ctx->gres[j][i-ctx->n] ;
          }
        }
        ctx->colle[icur] = o8vecn(1,ctx->n,ctx->o8elim_qri);
        ctx->colno[icur] = i; /* global constraint number */
        ctx->o8elim_col[icur] = icur ; /* local column number */
        ctx->o8elim_rhsscal[icur] = one/ctx->colle[icur];
        for ( j = 1 ; j <= ctx->n ; j++ )
        {
           ctx->qr[j][icur] = ctx->o8elim_qri[j]/ctx->colle[icur] ;
        }
        ctx->colle[icur] = one;
        /* here colno points to the indices of the linear equality constraints */
      }
    }
    if ( icur == 0 ) return ;
    /* there are no linear equality constraints */
    /* otherwise try to determine the rank of the current qr */
    n1  = 1 ;
    n2  = icur;
    ctx->rank = 0 ;
    for ( i = n1 ; i <= n2 ; i++)
    {
       /* search for pivot column */
       ipiv  = i;
       curle = ctx->colle[i];
       for (j = i+1 ; j <= n2 ; j++)
       {
         if ( ctx->colle[j] > curle )
         {
                curle = ctx->colle[j];
                ipiv = j ;
         }
       }
       /* interchange columns explicitly, if necessary   */
       if ( ipiv != i )
       {
          j           = ctx->o8elim_col[i];
          ctx->o8elim_col[i]    = ctx->o8elim_col[ipiv];
          ctx->o8elim_col[ipiv] = j;
          term        = ctx->colle[i];
          ctx->colle[i]    = ctx->colle[ipiv];
          ctx->colle[ipiv] = term;
          for (k = 1 ; k <= ctx->n ; k++)
          {
             term        = ctx->qr[k][i];
             ctx->qr[k][i]    = ctx->qr[k][ipiv];
             ctx->qr[k][ipiv] = term;
          }
       }
       sum = zero;
       for (j = i ;  j <= ctx->n ;  j++)
       {
         term   = ctx->qr[j][i];
         ctx->o8elim_qri[j] = term;
         sum    = term*term+sum;
       }

       if ( sum <= pow(ctx->rho,2) )
       {
         /* set tiny values to zero */
         for (j = i ; j <= n2 ; j++)
         {
           ctx->colle[j] = zero;
           for (k = i ; k <= ctx->n ; k++)
           {
              ctx->qr[k][j] = zero;
           }
         }
         ctx->rank = i-1;
         break;
       }
       qrii   = ctx->o8elim_qri[i];
       dalpha = -sqrt(sum);
       if ( qrii < zero ) dalpha = -dalpha;
       dbeta    = one/(sum-qrii*dalpha);
       ctx->diag[i]  = dalpha;
       ctx->betaq[i] = dbeta;
       ctx->o8elim_qri[i]   = qrii-dalpha;
       ctx->qr[i][i] = ctx->o8elim_qri[i];
       ctx->rank     = i;
       i1       = i+1;
       for (j = i1 ; j <= n2 ; j++)
       {
         sum = dbeta*o8sc3b(i,ctx->n,j,ctx->qr,ctx->o8elim_qri);
         for (k = i ; k <= ctx->n ; k++)
         {
           ctx->qr[k][j] = ctx->qr[k][j]-sum*ctx->o8elim_qri[k];
         }
         ctx->colle[j] = ctx->colle[j]-pow(ctx->qr[i][j],2);
       }
    }
    /* test */
    /* printf("berechneter rang = %d \n",rank); */
    /* testende */
    if ( ctx->rank == n2 ) return ;
    if ( ! ctx->silent ) o8msg(28, ctx);
    /* the linear equality constraints are sufficiently independent*/
    /* otherwise, check first for compatibility of the (almost) dependent */
    /*  now
       R= [R11 , R12 ; O  O ]
       in matlab notation, where R11 is rank times rank
       R is stored in diag and above the diagonal in qr
    */
    /* constraints : solve transpose(R11)*y=P*b , */
    for ( i = 1 ; i <= ctx->rank ; i++ )
    {
       sum = ctx->low[ctx->colno[ctx->o8elim_col[i]]]*ctx->o8elim_rhsscal[ctx->o8elim_col[i]];
       /* the permuted right hand side, low[i]=up[i] */
       for ( j = i-1 ; j>=1 ; j-- )
       {
         sum -= ctx->qr[j][i]*ctx->o8elim_y[j] ;
       }
       ctx->o8elim_y[i] = sum/ctx->diag[i] ;
    }
    for ( i = ctx->rank +1 ; i <= n2 ; i++ )
    {
       sum  = ctx->low[ctx->colno[ctx->o8elim_col[i]]]*ctx->o8elim_rhsscal[ctx->o8elim_col[i]];
       for ( j = 1 ; j<= ctx->rank ; j++ )
       {
         term = ctx->qr[j][i]*ctx->o8elim_y[j] ;
         sum -= term ;
       }
       if ( fabs(sum) >= two*ctx->n*ctx->rho )
       {
         ctx->optite = 8 ;
         if ( ! ctx->silent ) o8msg(27, ctx);
         return ;
       }
    }

    for ( i = ctx->rank +1 ; i <= n2 ; i++ )
    {
      ctx->low[ctx->colno[ctx->o8elim_col[i]]] = -ctx->big ;
      ctx->up[ctx->colno[ctx->o8elim_col[i]]]  =  ctx->big ;
    }
    /* inhibit use of dependent but compatible linear equality constraints */
    return;
}


/* **************************************************************************** */
/* QR-decomposition of matrix of gradients of binding constraints               */
/* this set may be expanded using multiple calls to o8dec.                      */
/* No exit on singular r-factor here. Information on                            */
/* the decompostion is stored in betaq and in and below the                     */
/* diagonal of qr. r-factor is stored in diag (diagonal ) and                   */
/* above the diagonal of qr. cscal is the column scaling of the                 */
/* original matrix. Column pivoting is done here and is stored in colno         */
/* **************************************************************************** */
void o8dec(INTEGER nlow,INTEGER nrl, TLContext *ctx) {


     INTEGER  n1,n2;
     INTEGER  i,j,k,l,i1,i2,ipiv;
     DOUBLE   sum,term,dalpha,dbeta,qrii,dl,fac;
     DOUBLE   curle;


    if ( nlow > nrl ) return;
    if ( nlow == 1 ) ctx->rank = 0;
    dl = one/(ctx->n+ctx->n+ctx->n);
    for ( i = nlow ; i <= nrl ; i++) {
        ctx->diag[i]  = zero;
        ctx->betaq[i] = zero;
        ctx->colno[i] = i;
        if ( ctx->aalist[i] <= 2*ctx->n )
        {
          /* a bound constraint */
          k = (ctx->aalist[i]+1)/2;
          /* component x[k]     */
          for ( j = 1 ; j <= ctx->n ; j++ )
          {
            ctx->o8dec_qri[j] = zero ;
          }
          ctx->o8dec_qri[k] = ( ctx->aalist[i] % 2 == 0 ) ? -ctx->xsc[k] : ctx->xsc[k] ;
        }
        else
        {
          k = (ctx->aalist[i]-2*ctx->n+1)/2 ;
          fac = ctx->gres[0][k];
          for (j = 1 ; j <= ctx->n ; j++)
          {
            ctx->o8dec_qri[j] = ctx->gres[j][k]*fac;
          }
        }
        o8left(ctx->a,ctx->o8dec_qri,ctx->o8dec_qri0,&sum,ctx->n);

        if ( sum == zero ) {
            ctx->cscal[i] = one;
            ctx->colle[i] = zero;
            for (j = 1 ; j <= ctx->n ; j++) {
                ctx->qr[j][i] = zero;
            }
        } else {
            for (j = 1 ; j <= ctx->n ; j++) {
                ctx->o8dec_qri[j] = ctx->o8dec_qri0[ctx->perm[j]];
            }
            term     = one/sqrt(max(sum,pow(ctx->rho,2)));
            ctx->cscal[i] = term;
            if ( nlow > 1 ) {

                o8ht(1,0,1,ctx->rank,ctx->n,ctx->qr,ctx->betaq,ctx->o8dec_qri,ctx->o8dec_qri0);

                for (j = 1 ; j <= ctx->n ; j++) {
                    ctx->o8dec_qri[j] = ctx->o8dec_qri0[j];
                }
            }
            for (j = 1 ; j <= ctx->n ; j++) {
                ctx->qr[j][i] = ctx->o8dec_qri[j]*term;
            }
            /* colle : length of remaining column squared */

            ctx->colle[i] = pow(o8vecn(ctx->rank+1,ctx->n,ctx->o8dec_qri)*term,2);
        }
    }
    if ( nlow > 1 && ctx->rank < nlow-1 ) {

        /* shift zero block to the right */

        i1 = nlow-1-ctx->rank;
        i2 = nrl-nlow+1;
        for (i = 1 ; i <= min(i1,i2) ; i++) {
            ipiv        = ctx->rank+i;
            k           = nrl-i+1;
            term        = ctx->betaq[k];
            ctx->betaq[k]    = ctx->betaq[ipiv];
            ctx->betaq[ipiv] = term;
            j           = ctx->colno[k];
            ctx->colno[k]    = ctx->colno[ipiv];
            ctx->colno[ipiv] = j;
            term        = ctx->colle[k];
            ctx->colle[k]    = ctx->colle[ipiv];
            ctx->colle[ipiv] = term;
            for (j = 1 ; j <= ctx->n ; j++) {
                term        = ctx->qr[j][k];
                ctx->qr[j][k]    = ctx->qr[j][ipiv];
                ctx->qr[j][ipiv] = term;
             }
         }
    }
    if ( nlow > 1 ) {
        n1 = ctx->rank+1;
        n2 = n1+nrl-nlow;
    } else {
        n1 = nlow;
        n2 = nrl;
    }
    for ( i = n1 ; i <= n2 ; i++) {

        /* search for pivot column */

        ipiv  = i;
        curle = ctx->colle[i];
        for (j = i+1 ; j <= n2 ; j++) {
            if ( ctx->colle[j] > curle ) {
                curle = ctx->colle[j];
            }
        }
        for (j = n2 ; j >= i ; j--) {
            if ( ctx->colle[j] >= curle/three ) ipiv = j;
        }
        /* interchange columns explicitly, if necessary   */
        /* make interchanges continuous with respect to x */

        if ( ipiv != i ) {
            j           = ctx->colno[i];
            ctx->colno[i]    = ctx->colno[ipiv];
            ctx->colno[ipiv] = j;
            term        = ctx->colle[i];
            ctx->colle[i]    = ctx->colle[ipiv];
            ctx->colle[ipiv] = term;
            for (k = 1 ; k <= ctx->n ; k++) {
                term        = ctx->qr[k][i];
                ctx->qr[k][i]    = ctx->qr[k][ipiv];
                ctx->qr[k][ipiv] = term;
            }
        }
        sum = zero;
        for (j = i ;  j <= ctx->n ;  j++) {
            term   = ctx->qr[j][i];
            ctx->o8dec_qri[j] = term;
            sum    = term*term+sum;
        }
        if ( sum <= pow(ctx->rho,2) ) {

            /* set tiny values to zero */

            for (j = i ; j <= n2 ; j++) {
                ctx->colle[j] = zero;
                for (k = i ; k <= ctx->n ; k++) {
                    ctx->qr[k][j] = zero;
                }
            }
            ctx->rank = i-1;

            return;
        }
        qrii   = ctx->o8dec_qri[i];
        dalpha = -sqrt(sum);
        if ( fabs(qrii) <= -dalpha*dl ) {
            term = zero;
            for (j = i+1 ; j <= ctx->n ; j++) {
                if ( fabs(ctx->o8dec_qri[j]) > term ) {
                    term = fabs(ctx->o8dec_qri[j]);
                    l    = j;
                }
            }
            k        = ctx->perm1[i];
            ctx->perm1[i] = ctx->perm1[l];
            ctx->perm1[l] = k;
        }
        if ( qrii < zero ) dalpha = -dalpha;
        dbeta    = one/(sum-qrii*dalpha);
        ctx->diag[i]  = dalpha;
        ctx->betaq[i] = dbeta;
        ctx->o8dec_qri[i]   = qrii-dalpha;
        ctx->qr[i][i] = ctx->o8dec_qri[i];
        ctx->rank     = i;
        i1       = i+1;
        for (j = i1 ; j <= n2 ; j++) {
            sum = dbeta*o8sc3b(i,ctx->n,j,ctx->qr,ctx->o8dec_qri);
            for (k = i ; k <= ctx->n ; k++) {
                ctx->qr[k][j] = ctx->qr[k][j]-sum*ctx->o8dec_qri[k];
            }
            ctx->colle[j] = ctx->colle[j]-pow(ctx->qr[i][j],2);
        }
    }
    return;
}

/* **************************************************************************** */
/*                  application of Householder transformations                  */
/* **************************************************************************** */
void o8ht(INTEGER id,INTEGER incr,INTEGER is1,INTEGER is2,INTEGER m,
          DOUBLE **a,DOUBLE beta[],DOUBLE b[],DOUBLE c[]) {

    /* application of Householder transformations stored     */
    /* in the lower or strict lower (if incr = 0 or 1 resp.) */
    /* triangle of a and in the vector beta on b giving c.   */
    /* only columns is1 to is2 are used in forward manner    */
    /* if id > 0,backwards otherwise.                        */
    /* rows is1 to m of c are changed only                   */
    /* a stands here for the actual qr                       */

     DOUBLE   sum;
     INTEGER  i,j,k,it;

    for (i = 1 ; i <= m ; i++) {
        c[i] = b[i];
    }
    if(is1 > m)   return;
    if(is2 < is1) return;
    for (i = is1 ; i <= is2 ; i++) {
        it = i;
        if(id < 0) it = is2-it+is1;

        /* it = index of transformation */

        j   = it+incr;
        sum = beta[it]*o8sc3b(j,m,it,a,c);
        for (k = j ; k <= m ; k++) {
            c[k] = c[k]-sum*a[k][it];
        }
    }
    return;
}

/* **************************************************************************** */
/* solve triangular system r*x = b, r defined by Householder-QR-                */
/* decomposition decomp (with column scaling)                                   */
/* **************************************************************************** */
void o8sol(INTEGER nlow,INTEGER nup,DOUBLE b[],DOUBLE x[], TLContext *ctx) {

     DOUBLE   sum;
     INTEGER  i,j;

    for (i = nup ; i >= nlow ; i--) {
        sum = zero;
        for (j = i+1 ; j <= nup ; j++) {
            sum = sum+ctx->qr[i][j]*ctx->o8sol_xl[j];
        }
        ctx->o8sol_xl[i] = (b[i]-sum)/ctx->diag[i];
    }
    for (i = nlow ; i <= nup ; i++) {
        x[i] = ctx->o8sol_xl[i]*ctx->cscal[ctx->colno[i]];
    }
    /* there must follow interchange of x as given by colno */
    /* e.g. xx(colno[i]) = x[i]                             */

    return;
}

/* **************************************************************************** */
/* solve triangular system r(transpose)*x = b, r defined by                     */
/* Householder-QR-decomposition decomp (with column scaling]                    */
/* **************************************************************************** */
void o8solt(INTEGER nlow,INTEGER nup,DOUBLE b[],DOUBLE x[], TLContext *ctx) {

     INTEGER  i,j;
     DOUBLE   sum;

    for (i = nlow ; i <= nup ; i++) {

        /* b has been permuted already ! */

        x[i] = b[i]*ctx->cscal[ctx->colno[i]];
    }
    for (i = nlow ; i <= nup ; i++) {
        sum = zero;
        for (j = nlow ; j <= i-1 ; j++) {
            sum = sum+ctx->qr[j][i]*x[j];
        }
        x[i] = (x[i]-sum)/ctx->diag[i];
    }
    return;
}

/* **************************************************************************** */
/* lenght of vector (a,b). numerically stable version with                      */
/* overflow / underflow saveguard                                               */
/* **************************************************************************** */
DOUBLE o8dsq1(DOUBLE a,DOUBLE b) {

     DOUBLE   a1,b1;
     DOUBLE   res;

    a1 = fabs(a);
    b1 = fabs(b);
    if ( a1 > b1 ) {
        res = a1*sqrt(one+pow(b1/a1,2));
    } else {
        if ( b1 > a1 ) {
            res = b1*sqrt(one+pow(a1/b1,2));
        } else {
            res = a1*sqrt(two);
        }
    }
    return (res);
}

/* **************************************************************************** */
/*                  computes the upper triangular Cholesky-factor               */
/* **************************************************************************** */
void o8upd(DOUBLE **r,DOUBLE z[],DOUBLE y[],INTEGER n,LOGICAL *fail, TLContext *ctx) {

     INTEGER  i,j,i1;
     DOUBLE   yl,zl,wl,wn1,ai,bi,h;

    /* o8upd computes the upper triangular Cholesky-factor               */
    /* r1 of r(transpose)*r+z*z(transpose)-y*y(transpose)                */
    /* and restores it in r. The strict lower triangle of r              */
    /* remains unchanged.                                                */
    /* fail = TRUE if the decomposition does'nt exist, stop on dimension */
    /* error, fail = false on success.                                   */

    *fail = FALSE;

    /* save subdiagonal */

    for (i = 1 ; i <= n-1 ; i++) {
        ctx->o8upd_sdiag[i]  = r[i+1][i];
        r[i+1][i] = zero;
    }
    /* step one: include z*z(transpose) */

    zl = zero;
    for (i = 1 ; i <= n ; i++) {
        zl = zl + pow(z[i],2);
    }
    if ( zl != zero ) {

        /* solve w(transpose)*r = z(transpose) */

        o8left(r,z,ctx->o8upd_w,&wl,n);

        wl = sqrt(wl+one);

        /* u[2]*u[3]*...*u[n]*w = ( norm(w),0,..,0)(transpose) */
        /* u[i] rotations                                      */

        for (i = n ; i >= 2 ; i--) {
            if ( ctx->o8upd_w[i] != zero ) {
                i1        = i-1;
                ai        = ctx->o8upd_w[i1];
                bi        = ctx->o8upd_w[i];
                ctx->o8upd_w[i1]     = o8dsq1(ai,bi);
                ai        = ai/ctx->o8upd_w[i1];
                bi        = -bi/ctx->o8upd_w[i1];
                r[i][i1]  = bi*r[i1][i1];
                r[i1][i1] = ai*r[i1][i1];
                for (j = i ; j <= n ; j++) {
                    h        = ai*r[i1][j] - bi*r[i][j];
                    r[i][j]  = bi*r[i1][j] + ai*r[i][j];
                    r[i1][j] = h;
                }
            }
        }
        /* r = d*r, d = diag(wl,1,...,1), r now Hessenberg */

        for (i = 1 ; i <= n ; i++) {
            r[1][i] = r[1][i]*wl;
        }
        /* r = u[n-1]*...*u[1]*r now upper triangular, */
        /* u[i] givens-rotations                       */

        for (i = 1 ; i <= n-1 ; i++) {
            i1 = i+1;
            ai = r[i][i];
            bi = -r[i1][i];
            h  = o8dsq1(ai,bi);
            if ( h != zero ) {
                ai       = ai/h;
                bi       = bi/h;
                r[i][i]  = h;
                r[i1][i] = zero;
                for (j = i+1 ; j <= n ; j++) {
                    h        = ai*r[i][j] - bi*r[i1][j];
                    r[i1][j] = bi*r[i][j] + ai*r[i1][j];
                    r[i][j]  = h;
                }
            }
        }
    }
    /* step two : include -y*y(transpose) */

    yl = zero;
    for (i = 1 ; i <= n ; i++) {
        yl = yl + pow(y[i],2);
    }
    if ( yl != zero ) {

        o8left(r,y,ctx->o8upd_w,&wl,n);

        if ( wl >= one ) {
            *fail = TRUE;
        } else {
            wl  = sqrt(fabs(one-wl));
            wn1 = wl;

            /* ************************************************ */
            /*   ( r(new), 0 )                  (    r  , w )   */
            /*   (-----------)  =  u[1]*...u[n]*(-----------)   */
            /*   (y(transp),1)                  ((0,..,0),wl)   */
            /* ************************************************ */

            for (i = n ; i >= 1 ; i--) {
                ai  = wn1;
                bi  = ctx->o8upd_w[i];
                wn1 = o8dsq1(ai,bi);
                if ( wn1 != zero ) {
                    ai      = ai/wn1;
                    bi      = bi/wn1;
                    ctx->o8upd_rn1[i]  = bi*r[i][i];
                    r[i][i] = ai*r[i][i];
                    for (j = i+1 ; j <= n ; j++) {
                        h       = ai*r[i][j] - bi*ctx->o8upd_rn1[j];
                        ctx->o8upd_rn1[j]  = bi*r[i][j] + ai*ctx->o8upd_rn1[j];
                        r[i][j] = h;
                    }
                }
            }
        }
    }
    /* restore subdiagonal */

    for ( i = 1 ;  i <= n-1 ;  i++) {
        r[i+1][i] = ctx->o8upd_sdiag[i];
    }
    return;
}

/* **************************************************************************** */
/*                                    o8rght                                    */
/* **************************************************************************** */
void o8rght(DOUBLE **a,DOUBLE b[],DOUBLE y[],DOUBLE *yl,INTEGER n) {

     INTEGER  i,j;
     DOUBLE   h;

    /* o8rght assumes that the Cholesky-factor of a         */
    /* a = r(transpose)*r is stored in the upper half of a. */
    /* b is a right hand side. o8rght solves                */
    /* r*y = b                                              */
    /* yl = pow(norm(y),2)                                  */

    *yl = zero;
    for (i = n ; i >= 1 ; i--) {
        h = b[i];
        for (j = i+1 ; j <= n ; j++) {
            h = h - a[i][j]*y[j];
        }
        h    = h/a[i][i];
        y[i] = h;
        *yl  = pow(h,2) + *yl;
    }
    return;
}

/* **************************************************************************** */
/*                                  o8left                                      */
/* **************************************************************************** */
void o8left(DOUBLE **a,DOUBLE b[],DOUBLE y[],DOUBLE *yl,INTEGER n) {

     INTEGER  i,j;
     DOUBLE   h;

    /* o8left assumes that the Cholesky-factor of a         */
    /* a = r(transpose)*r is stored in the upper half of a. */
    /* b is a right hand side. o8left solves                */
    /* r(transpose)*y = b                                   */
    /* yl = pow(norm(y),2)                                  */

    *yl = zero;
    for (i = 1 ; i <= n ; i++) {
        h = b[i];
        for (j = 1 ; j <= i-1 ; j++) {
            h = h - a[j][i]*y[j];
        }
        h    = h/a[i][i];
        y[i] = h;
        *yl  = pow(h,2) + *yl;
    }
    return;
}

/* **************************************************************************** */
/*                      euclidean norm of x , avoid overflow                    */
/* **************************************************************************** */
DOUBLE o8vecn(INTEGER nl,INTEGER nm,DOUBLE x[]) {

     INTEGER  i;
     DOUBLE   xm,h;

    if ( nm < nl ) {

        return (zero);
    }
    xm = fabs(x[nl]);
    for (i = nl+1 ; i <= nm ; i++) {
        xm = max(xm,fabs(x[i]));
    }
    if ( xm == zero ) {

        return (zero);

    } else {
        h = zero;
        for (i = nl ; i <= nm ; i++) {
            h = h+pow(x[i]/xm,2);
        }
        return (xm*sqrt(h));
    }
}

/* ************************************************************************* */
/*  solution of extended quadratic program                                   */
/*                                                                           */
/*  scf*gradf(x)*dir+(1/2)*dir*a*dir+summe(tauqp*sl[i]+( my/2)*pow(sl[i],2) )*/
/*  minimal subject to                                                       */
/*  sl[i] >= 0, i = 1,...,nr  (the slacks)                                   */
/*  (gres[.][j]*dir+res[j])+vz*sl[j] = 0, j = 1,...,nh vz = -sign(res[j])    */
/*  (gres[.][aalist[j]])*dir+res[aalist[j]])+sl[j] >= 0, j = nh+1,....,nr    */
/*  the weight tauqp is adapted during solution                              */
/*  the quasi-Newton-matrix a is taken from o8comm.h                         */
/*  a is regularized if not sufficiently well conditioned                    */
/*  the resulting dir=xd[1+nr],...,xd[n+nr] is a direction of descent for    */
/*  the Zangwill function of the corresponding nonlinear                     */
/*  optimization problem                                                     */
/*  f(x) = min, res[j] = 0, j = 1,..nh, res[j] >= 0 , j = nh+1,nres          */
/*  at the currrent point x if the weight tauqp is chosen appropriately      */
/*  the quadratic programming problem is solved using the method             */
/*  of Goldfarb and Idnani                                                   */
/*  variables are stored in xd (solution) and ud (multipliers)               */
/*  in the following order xd = ( sl[j], j = 1,nr; dir = direct. of desc.)   */
/*  ud = ( multipliers for sl[i] >= 0 , i = 1,..,nr ;                        */
/*  multipliers for the equality constraints ,                               */
/*  multipliers for the general inequality constraints )                     */
/* ***************************************************************************/
void o8qpdu(TLContext *ctx)
{

     DOUBLE   infe1,s1,s2,tiny,
                    my,zz,ss,su,t,t1,t2,f,fmax,psid,c1,c2,cdiag,term,
                    su1,su2,condr,infiny,term1,term2,
                    diff0;
     INTEGER  i,j,k,ip,l,incr,nosucc;
     LOGICAL  wlow;

    ctx->ndual = ctx->n+ctx->nr;  /* nr=aalist[0] */

    /* number of equality constraints in QP-problem */
    incr = ctx->nr ;
    ctx->mi = 0 ;
    ctx->me = 0 ;
    /* compute the index lists of the QP's equality constraints
       eqlist and inequality constraints iqlist from aalist and
       low and up */
   for ( i = 1; i <= ctx->aalist[0] ; i++ )
   {
     l = (ctx->aalist[i]+1)/2;
     if ( ctx->low[l] == ctx->up[l] )
     {
       ctx->me += 1 ;
       ctx->o8qpdu_eqlist[ctx->me] = ctx->aalist[i] ;
     }
     else
     {
       ctx->mi += 1 ;
       ctx->o8qpdu_iqlist[ctx->mi+incr] = ctx->aalist[i]+ctx->nr ;
     }
   }
   for ( i = 1 ; i <= ctx->nr ; i++ )
   {
       ctx->o8qpdu_iqlist[i] = i ;   /* the slacks of the QP model */
   }
   ctx->mi += ctx->nr ;

   /* QP inequality constraints = active constraints - */
   /* equality-constraints + slack's                   */

   infiny = ctx->epsmac/ctx->tolmac;
   if ( ctx->analyt )
   {
        tiny = 2*ctx->nr*ctx->epsmac*tp3;
   }
   else
   {
       tiny = 2*ctx->nr*max(ctx->epsdif,ctx->epsmac*tp3);
   }

   ctx->qpterm = 0;
   for (i = 1 ; i <= ctx->nr ; i++)
   {

        /* check gradients of active constraints against zero */
        ctx->o8qpdu_mult [i] = one ;
        /* for a zero gradient the slack cannot be diminished:*/
        /*   set mult[i] = 0                                  */
        l = ctx->aalist[i] ;
        if ( l > 2*ctx->n )
        {  /* not a bound */
           for (j = 1 ; j <= ctx->n ; j++)
           {
             ctx->o8qpdu_y[j] = ctx->gres[j][(l-2*ctx->n+1)/2];
           }
           if ( o8vecn(1,ctx->n,ctx->o8qpdu_y) == zero )
           {
             ctx->o8qpdu_mult[i] = zero;
             if ( ! ctx->silent ) o8msg(8, ctx);
           }
        }
   }
   /* restart point in case of increase of tauqp */

    L10:
    nosucc = 0 ;
    /* initialize matrices j and r */

    for (i = 1 ; i <= ctx->ndual ; i++)
    {
        ctx->ddual[i] = zero;
        for (j = 1 ; j <= ctx->ndual ; j++)
        {
            ctx->r[j][i]  = zero;
            ctx->xj[j][i] = zero;
        }
    }
    ctx->rnorm = one;
    ctx->rlow  = one;
    term1 = zero;
    for (i = 1 ; i <= 2*ctx->nres ; i++)
    {
        ctx->u[i] = zero;
        if ( ctx->w[i] > term1 ) term1 = ctx->w[i];
    }
    ctx->accinf[ctx->itstep][19] = ctx->clow;
    ctx->accinf[ctx->itstep][20] = term1;
    ctx->accinf[ctx->itstep][31] = ctx->tauqp;
    for (i = 1 ; i <= 2*ctx->nr ; i++)
    {   /* the multipliers in the QP */
        ctx->ud[i] = zero;
    }
    c1 = fabs(ctx->a[1][1]);
    for (i = 1 ; i <= ctx->n ; i++)
    {
        c1 = max(c1,fabs(ctx->a[i][i]));
    }
    c1 = c1*tp1;

    /* we require much more regularity of a in the singular case */

    for (i = 1 ; i <= ctx->n ; i++)
    {
        if ( fabs(ctx->a[i][i]) < sqrt(ctx->rho1)*c1 ) ctx->a[i][i] = sqrt(ctx->rho1)*c1;
    }
    /* invert the Cholesky-factor and store in
       the right lower block of dimension n of xj ( Idnanis j-matrix) */

    o8rinv(ctx->n,ctx->a,ctx->ndual,ctx->xj);

    c1   = fabs(ctx->a[1][1]);
    incr = ctx->nr;
    c2   = fabs(ctx->xj[1+incr][1+incr]);
    for (i = 1 ; i <= ctx->n ; i++)
    {
        c1 = max(c1,fabs(ctx->a[i][i]));
        c2 = max(c2,fabs(ctx->xj[i+incr][i+incr]));
    }
    my = zero;
    for (i = 1 ; i <= ctx->n ; i++)
    {
        for (j = i ; j <= ctx->n ; j++)
        {
             my = my+pow(ctx->a[i][j],2);
        }
    }
    my    = my/ctx->n;
    cdiag = one/sqrt(my);
    for ( i = 1 ; i <= incr ; i++)
    {
        ctx->xj[i][i] = cdiag;
        /* the quadratic part for the slacks       */
        /* has same order of magnitude as the part */
        /* coming from the original Lagrangian     */
    }
    for (i = 1 ; i <= ctx->ndual ; i++)
    {
        if ( i >  incr ) ctx->o8qpdu_g0[i] = ctx->gradf[i-incr]*ctx->scf;
        if ( i <= incr ) ctx->o8qpdu_g0[i] = ctx->tauqp;
        /* the linear part for the slacks */
    }
    /* compute unconstrained solution */
    /* the Cholesky-factor of a is stored in the upper triangle */

    for (i = 1 ; i <= ctx->n ; i++)
    {
        su = zero;
        for (j = 1 ; j <= i-1 ; j++)
        {
            su = su+ctx->a[j][i]*ctx->o8qpdu_y[j+incr];
        }
        ctx->o8qpdu_y[i+incr] = (ctx->o8qpdu_g0[i+incr]-su)/ctx->a[i][i];
    }
    for (i = ctx->n ; i >= 1 ; i--)
    {
        su = zero;
        for (j = i+1 ; j <= ctx->n ; j++)
        {
            su = su+ctx->a[i][j]*ctx->o8qpdu_y[j+incr];
        }
        ctx->o8qpdu_y[i+incr] = (ctx->o8qpdu_y[i+incr]-su)/ctx->a[i][i];
    }
    for (i = 1 ; i <= incr ; i++)
    {

        /* initially assume the slacks being zero */

        ctx->o8qpdu_y[i] = zero;
    }
    for (i = 1 ; i <= ctx->ndual ; i++)
    {
        ctx->o8qpdu_xd[i] = -ctx->o8qpdu_y[i];
    }
    /* unconstrained minimizer of the QP: slacks come first */

    f = p5*o8sc1(1,ctx->ndual,ctx->o8qpdu_g0,ctx->o8qpdu_xd);
    fmax = f ;
    /* define the initial working set: all slacks are at their */
    /* lower bounds. iq = dimension of the QP's working set    */

    ctx->iq = ctx->nr;

    for (i = 1 ; i <= ctx->iq ; i++)
    {
        ctx->o8qpdu_ai[i]   = i;
        ctx->r[i][i] = one;
        ctx->ud[i]   = ctx->tauqp;
    }
    /* slacks are at zero, multipliers for the slacks at tauqp */
    ctx->rnorm = one;
    ctx->rlow  = one;

    /* introduction of equality constraints                       */
    /* these are characterized by a negative index in ai and never*/
    /* considered for inactivation. they also play no role for the*/
    /* dual step size t1                                          */


    for (i = 1 ; i <= ctx->me ; i++)
    {
        for ( j = 1 ; j <= ctx->iq ; j ++)
        {
            ctx->ud1[j] = ctx->ud[j];
        }
        L20:

        ctx->ud1[ctx->iq+1] = zero;

        /* an equality constraint is indicated by the negative index */

        ctx->o8qpdu_ai[ctx->iq+1] = -ctx->o8qpdu_eqlist[i] ;
        l = (ctx->o8qpdu_eqlist[i]+1)/2 ;
        /* store the gradient of this constraint in np */
        for (j = 1 ; j <= ctx->n ; j++)
        {
          if ( l > ctx->n )
          {  /* sign is 1 for equality constraints */
            ctx->o8qpdu_cei[j+incr] = ctx->gres[j][l-ctx->n];
          }
          else
          {
            ctx->o8qpdu_cei[j+incr] = zero ;
          }
        }
        for (j = 1 ; j <= incr ; j++)
        {
            ctx->o8qpdu_cei[j] = zero;
        }
        ctx->o8qpdu_cei[i] = one;
        if ( ctx->res[ctx->o8qpdu_eqlist[i]] > zero ) ctx->o8qpdu_cei[i] = -one;
        if ( l <= ctx->n )
        {
          ctx->o8qpdu_cei[l+incr] = ctx->xsc[l] ;   /* a fixed primal variable */
        }
        for (j = 1 ; j <= ctx->ndual ; j++)
        {
            ctx->np[j] = ctx->o8qpdu_cei[j];
        }

        o8zup(ctx->o8qpdu_z, ctx);

        if ( ctx->iq != 0 ) o8rup(ctx->o8qpdu_vr, ctx);

        /* |z| = 0? */

        zz   = o8vecn(1,ctx->ndual,ctx->o8qpdu_z);
        term = o8sc1(1,ctx->ndual,ctx->o8qpdu_z,ctx->np);

        if ( zz >= tiny*ctx->rnorm && term > zero )
        {
            t2 = (-o8sc1(1,ctx->ndual,ctx->np,ctx->o8qpdu_xd)-ctx->res[ctx->o8qpdu_eqlist[i]])/term;
        }
        else if ( (-o8sc1(1,ctx->ndual,ctx->np,ctx->o8qpdu_xd)-ctx->res[ctx->o8qpdu_eqlist[i]]) >= zero )
        {
            t2 = infiny;
        }
        else
        {
            t2 = -infiny;
        }
        /* for an equality constraint t2 may be positive or negative*/

        if ( ctx->iq != 0 ) o8rup(ctx->o8qpdu_vr, ctx);
        l = 0;
        if ( t2 > zero )
        {
            t1 = infiny;
            for (k = 1 ; k <= ctx->iq ; k++)
            {
                if( ctx->o8qpdu_vr[k] > zero && ctx->o8qpdu_ai[k] > 0 )
                {
                    if( ctx->ud1[k]/ctx->o8qpdu_vr[k] < t1 )
                    {
                        t1 = ctx->ud1[k]/ctx->o8qpdu_vr[k];
                    }
                }
            }
            t = min(t1,t2);
        }
        else
        {
            t1 = infiny;
            for ( k = 1 ; k <= ctx->iq ; k++)
            {
                if( ctx->o8qpdu_vr[k] < zero && ctx->o8qpdu_ai[k] > 0 )
                {
                    if( ctx->ud1[k]/fabs(ctx->o8qpdu_vr[k]) < t1 )
                    {
                        t1 = ctx->ud1[k]/fabs(ctx->o8qpdu_vr[k]);
                    }
                }
            }
            t1 = -t1;
            t  = max(t1,t2);

            /* t now negative */
        }
        /* add constraint , otherwise we must first delete some */
        /* inequality constraint with zero multiplier           */
        /* first delete then add!                               */

        if ( fabs(t)  >= infiny )
        {
          ctx->qpterm = -2 ;
          if ( ! ctx->silent ) o8msg(20, ctx);
          goto L2000;
        }
        if ( fabs(t2) >= infiny )
        {

            /* purely dual step */

            for (k = 1 ; k <= ctx->iq ; k++)
            {
                ctx->ud1[k] = ctx->ud1[k]+t*(-ctx->o8qpdu_vr[k]);
                if ( ctx->ud1[k] < zero && ctx->o8qpdu_ai[k] > 0 ) ctx->ud1[k] = zero;
                /*  ud1[k] < 0 is a roundoff effect */
            }
            ctx->ud1[ctx->iq+1] = ctx->ud1[ctx->iq+1]+t;
            ctx->o8qpdu_qpdel[0]  = 0;
            for (j = 1 ; j <= ctx->iq ; j++)
            {
                if ( ctx->ud1[j] <= tiny && ctx->o8qpdu_ai[j] > 0 )
                {
                    ctx->o8qpdu_qpdel[0]        = ctx->o8qpdu_qpdel[0]+1;
                    ctx->o8qpdu_qpdel[ctx->o8qpdu_qpdel[0]] = ctx->o8qpdu_ai[j];
                }
            }
            for (k = 1 ; k <= ctx->o8qpdu_qpdel[0] ; k++)
            {
                l      = ctx->o8qpdu_qpdel[k];
                ctx->o8qpdu_iai[l] = l;

                o8dlcd(ctx->o8qpdu_ai,l, ctx);
            }
            goto L20;
        }
        /* primal and dual step */
        for (k = 1 ; k <= ctx->ndual ; k++)
        {
            ctx->o8qpdu_xd[k] = ctx->o8qpdu_xd[k]+t*ctx->o8qpdu_z[k];
        }
        for (k = 1 ; k <= ctx->iq ; k++)
        {
            ctx->ud1[k] = ctx->ud1[k]+t*(-ctx->o8qpdu_vr[k]);
            if ( ctx->ud1[k] < zero && ctx->o8qpdu_ai[k] > 0 ) ctx->ud1[k] = zero;
        }
        ctx->ud1[ctx->iq+1] = ctx->ud1[ctx->iq+1]+t;

        f = f+t*o8sc1(1,ctx->ndual,ctx->o8qpdu_z,ctx->np)*(p5*t+ctx->ud1[ctx->iq+1]);
        if ( f <= fmax*(one+ctx->epsmac)+ctx->epsmac )
        {
          nosucc += 1 ;
          if ( nosucc > ctx->qpitma )
          {
            ctx->qpterm = - 3;
            if ( ! ctx->silent ) o8msg(26, ctx);
            goto L2000;
          }
        }
        else
        {
          nosucc = 0 ;
        }
        fmax = max( f , fmax ) ;

        if ( fabs(t2-t1) <= tiny )
        {
            ctx->o8qpdu_qpdel[0] = 0;
            for (j = 1 ; j <= ctx->iq ; j++)
            {
                if ( ctx->ud1[j] <= tiny && ctx->o8qpdu_ai[j] > 0 )
                {
                    ctx->o8qpdu_qpdel[0]        = ctx->o8qpdu_qpdel[0]+1;
                    ctx->o8qpdu_qpdel[ctx->o8qpdu_qpdel[0]] = ctx->o8qpdu_ai[j];
                }
            }
            for (k = 1 ; k <= ctx->o8qpdu_qpdel[0] ; k++)
            {
                l      = ctx->o8qpdu_qpdel[k];
                ctx->o8qpdu_iai[l] = l;

                o8dlcd(ctx->o8qpdu_ai,l, ctx);
            }
            ctx->o8qpdu_ai[ctx->iq+1] = -ctx->o8qpdu_eqlist[i] ;

            o8adcd(ctx);

        }
        else if ( t == t2 )
        {
            ctx->o8qpdu_ai[ctx->iq+1] = -ctx->o8qpdu_eqlist[i] ;

            o8adcd(ctx);

        }
        else
        {
            ctx->o8qpdu_qpdel[0] = 0;
            for (j = 1 ; j <= ctx->iq ; j++)
            {
                if ( ctx->ud1[j] <= tiny && ctx->o8qpdu_ai[j] > 0 )
                {
                    ctx->o8qpdu_qpdel[0]        = ctx->o8qpdu_qpdel[0]+1;
                    ctx->o8qpdu_qpdel[ctx->o8qpdu_qpdel[0]] = ctx->o8qpdu_ai[j];
                }
            }
            for (k = 1 ; k <= ctx->o8qpdu_qpdel[0] ; k++)
            {
                l      = ctx->o8qpdu_qpdel[k];
                ctx->o8qpdu_iai[l] = l;
                o8dlcd(ctx->o8qpdu_ai,l, ctx);
            }
            goto L20;
        }
        for (j = 1 ; j <= ctx->iq ; j++)
        {
            ctx->ud[j] = ctx->ud1[j];
        }
    /* end of loop over equality constraints */
    }

    /* set iai = k\ai */

    for (i = 1 ; i <= ctx->mi ; i++)
    {
        ctx->o8qpdu_iai[i] = i;
    }
    /* step 1 */

    L50:

    /* ai = QP - working set , iai[i] = 0 if i in ai */

    for (i = 1 ; i <= ctx->iq ; i++)
    {
        ip = ctx->o8qpdu_ai[i];
        if ( ip > 0 ) ctx->o8qpdu_iai[ip] = 0;
    }
    /* s[o8qpdu_xd] = ci(trans)*o8qpdu_xd+ci0 >= 0 ? */

    psid = zero;

    /* psid : the measure of infeasibility */

    for (i = 1 ; i <= ctx->mi ; i++)
    {

        /* iaexcl: if = 0, exclude from addition in this cycle */

        ctx->o8qpdu_iaexcl[i] = 1;
        su        = zero;
        k         =  0 ;
        /* numbers of inequality constraints:                           */
        /* i = 1,...,nr corresponds to the constraints v >= 0, u_a >= 0 */
        /* i = nr+1,....,mi to the regularized general inequalities     */

        if ( i > ctx->nr )
        {
            /* an original inequality constraint */
            k = (ctx->o8qpdu_iqlist[i]-ctx->nr+1)/2 ;
            if ( k > ctx->n )
            {  /* not a bound, gradient stored in gres, sign is in gres[0][] */
              for (j = 1 ; j <= ctx->n ; j++)
              {
                ctx->o8qpdu_cii[j+incr] = ctx->gres[j][k-ctx->n]*ctx->gres[0][k-ctx->n];
              }
              for (j = 1 ; j <= incr ; j++)
              {
                ctx->o8qpdu_cii[j] = zero;
              }
              ctx->o8qpdu_cii[ctx->me+i-incr] = one;
              ctx->o8qpdu_ci0[i]         = ctx->res[ctx->o8qpdu_iqlist[i]-ctx->nr];
            }
            else
            {
              for ( j = 1 ; j <= ctx->ndual ; j++ )
              {
                ctx->o8qpdu_cii[j] = zero ;
              }
              ctx->o8qpdu_cii[ctx->me+i-incr] = one ;
              if ( (ctx->o8qpdu_iqlist[i]-ctx->nr) % 2 == 0 )
              {
                ctx->o8qpdu_cii[k+incr] = -ctx->xsc[k];
                ctx->o8qpdu_ci0[i] = ctx->res[ctx->o8qpdu_iqlist[i]-ctx->nr];
              }
              else
              {
                ctx->o8qpdu_cii[k+incr] = ctx->xsc[k];
                ctx->o8qpdu_ci0[i]      = ctx->res[ctx->o8qpdu_iqlist[i]-ctx->nr];
              }
            }

        }
        else
        { /* a QP slack variable */
          for (j = 1 ; j <= ctx->ndual ; j++)
          {
             ctx->o8qpdu_cii[j] = zero;
          }
          ctx->o8qpdu_ci0[i] = zero;
          ctx->o8qpdu_cii[i] = one;
        }
/* end computation of data of mi-th QP constraint data */

        su   = o8sc1(1,ctx->ndual,ctx->o8qpdu_cii,ctx->o8qpdu_xd)+ctx->o8qpdu_ci0[i];
        ctx->o8qpdu_s[i] = su;
        psid = psid+min(zero,su);
    }

    for (i = 1 ; i <= ctx->iq ; i++) {
        ctx->o8qpdu_udold[i] = ctx->ud[i];
        ctx->o8qpdu_aiold[i] = ctx->o8qpdu_ai[i];
    }
    for (i = 1 ; i <= ctx->ndual ; i++) {
        ctx->o8qpdu_xdold[i] = ctx->o8qpdu_xd[i];
    }
    L60:
    ss = zero;
    ip = 0;

    /* introduce most violated inequality constraint */

    for (i = 1 ; i <= ctx->mi ; i++) {
        if( ctx->o8qpdu_s[i] < ss && ctx->o8qpdu_iai[i] != 0 && ctx->o8qpdu_iaexcl[i] != 0 ) {
            ss = ctx->o8qpdu_s[i];
            ip = i;
        }
    }
    if ( ctx->iq > 1 ) {
        condr = ctx->rnorm/ctx->rlow;
    } else {
        condr = one;
    }

    if ( fabs(psid) <= tiny*(c1*c2+condr) || ip == 0) {

        /* successful termination of QP-solver for current tauqp */

        if ( fabs(psid) > tiny*(c1*c2+condr) && ! ctx->silent ) o8msg(10, ctx);
        ctx->qpterm = 1;
        ctx->accinf[ctx->itstep][30] = one;
        ctx->accinf[ctx->itstep][13] = condr;
        ctx->accinf[ctx->itstep][14] = c1*c2;
        for (i = 1 ; i <= ctx->n ; i++) {
            ctx->d[i] = ctx->o8qpdu_xd[i+incr];
        }
        /* new : dnorm added */

        ctx->dnorm  = o8vecn(1,ctx->n,ctx->d);
        ctx->infeas = zero;

        for (i = 1 ; i <= incr ; i++) {
            ctx->infeas = ctx->infeas+fabs(ctx->o8qpdu_xd[i]);
        }
        /* L1-norm of slack variables */

        ctx->accinf[ctx->itstep][31] = ctx->tauqp;
        ctx->accinf[ctx->itstep][32] = ctx->infeas;
        wlow = FALSE;
        su1  = zero;
        su2  = zero;
        for (i = 1 ; i <= ctx->iq ; i++)
        {
            if ( ctx->o8qpdu_ai[i] < 0 )
            {
                ctx->u[-ctx->o8qpdu_ai[i]] = ctx->ud[i];
            }
            else
            {
                if ( ctx->o8qpdu_ai[i] > ctx->nr ) ctx->u[ctx->o8qpdu_iqlist[ctx->o8qpdu_ai[i]]-ctx->nr] = ctx->ud[i];
            }
        }

        term1 = zero;
        for (j = 1 ; j <= ctx->n ; j++) {
            ctx->np[j] = ctx->gradf[j]*ctx->scf;
        }
        for (i = 1 ; i <= ctx->nres ; i++)
        {
          if ( i > ctx->n )
          {
            for (j = 1 ; j <= ctx->n ; j++)
            {
                ctx->np[j] = ctx->np[j]-ctx->gres[j][i-ctx->n]*(ctx->u[2*i-1]-ctx->u[2*i]);
            }
          }
          else
          {
            ctx->np[i] = ctx->np[i]-ctx->xsc[i]*(ctx->u[2*i-1]-ctx->u[2*i]);
          }
        }

        ctx->b2n = o8vecn(1,ctx->n,ctx->np);

        if ( ctx->scf != zero ) ctx->b2n = ctx->b2n/ctx->scf;

        /* correction in the original variables */

        infe1 = zero;
        for (i = 1 ; i <= ctx->nr ; i++) {
            infe1 = infe1+fabs(ctx->o8qpdu_xd[i])*ctx->o8qpdu_mult[i];
        }
        if ( ctx->upsi <= ctx->delmin*ctx->nres
            && ctx->b2n <= (ctx->gfn+one)*ctx->epsx*tp2 && ctx->phase >= 0
            && ctx->infeas <= ctx->delmin*ctx->nres ) {
        /* dual feasibility is automatic here */
        /* since multipliers may be incorrect for infeas != zero be careful */
        /* we consider the problem as successfully solved with reduced      */
        /* requirements. we terminate here                                  */

            for (i = 1 ; i <= ctx->n ; i++)
            {
                ctx->d[i] = zero;
            }
            ctx->dnorm  = zero;
            ctx->optite = three;
            ctx->dirder = zero ;
            ctx->qpterm = 1 ;
            return;
        }
        /* there may be an additional increase of tauqp necessary again */
        if ( infe1 > (one-ctx->delta1/ctx->tauqp)*ctx->upsi &&
            ( o8vecn(1,ctx->n,ctx->d) <= min(infe1,pow(infe1,2))*tp1
            || ctx->upsi > ctx->tau0*p5 ) )
        {

        /* further increase tauqp ! */

            for (i = 1 ; i <= 2*ctx->nres ; i++)
            {
                ctx->u[i]     = zero;
                ctx->slack[i] = zero;
            }
            if ( ! ctx->silent ) o8msg(17, ctx);
            if ( ctx->tauqp*ctx->taufac > ctx->taumax ) {
                if ( ! ctx->silent ) o8msg(5, ctx);
                ctx->qpterm = -1;
                ctx->accinf[ctx->itstep][30] = ctx->qpterm;
                ctx->accinf[ctx->itstep][31] = ctx->tauqp;
                ctx->accinf[ctx->itstep][32] = ctx->infeas;
                for (i = 1 ; i <= ctx->n ; i++) {
                    ctx->d[i] = zero;
                }
                ctx->dnorm = zero;

                return;

            } else {
                ctx->tauqp = ctx->tauqp*ctx->taufac;

                goto L10;
            }
        }
        /* compute new weights for the penalty-function */

        L500:

        for (i = 1 ; i <= 2*ctx->nres ; i++)
        {
            ctx->slack[i] = zero;
        }
        for (i = 1 ; i <= ctx->nr ; i++)
        {
            ctx->slack[ctx->aalist[i]] = ctx->o8qpdu_xd[i];
        }
        wlow = FALSE;
        for (i = 1 ; i <= ctx->nres ; i++)
        {
            ctx->w1[2*i-1] = ctx->w[2*i-1];
            ctx->w1[2*i] = ctx->w[2*i] ;
            if ( ctx->low[i] == ctx->up[i] )
            {
               if ( fabs(ctx->slack[2*i-1]) > fabs(ctx->res[2*i-1])+tiny )
               {
                    ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
               }
               else
               {
                    ctx->w1[2*i-1] = ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau;
               }
            }
            else
            {
               if ( ctx->o8bind[2*i-1] == 0 )
               {
                  ctx->w1[2*i-1] = max(ctx->w[2*i-1]*p8,ctx->tau);
               }
               else
               {
                  if ( ctx->res[2*i-1] >= zero && ctx->slack[2*i-1] <= tiny )
                    ctx->w1[2*i-1] =
                    max(ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau,(fabs(ctx->u[2*i-1])+ctx->w1[2*i-1])*p5);
                  if ( ctx->res[2*i-1] >= zero && ctx->slack[2*i-1] > tiny )
                    ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
                  if ( ctx->res[2*i-1] < zero && ctx->slack[2*i-1] <= -ctx->res[2*i-1]+tiny )
                    ctx->w1[2*i-1] =
                    max(ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau,(fabs(ctx->u[2*i-1])+ctx->w1[2*i-1])*p5);
                  if ( ctx->res[2*i-1] < zero && ctx->slack[2*i-1] >  -ctx->res[2*i-1]+tiny )
                    ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
               }
               if ( ctx->o8bind[2*i] == 0 )
               {
                  ctx->w1[2*i] = max(ctx->w[2*i]*p8,ctx->tau);
               }
               else
               {
                  if ( ctx->res[2*i] >= zero && ctx->slack[2*i] <= tiny )
                    ctx->w1[2*i] =
                    max(ctx->ny*fabs(ctx->u[2*i])+ctx->tau,(fabs(ctx->u[2*i])+ctx->w1[2*i])*p5);
                  if ( ctx->res[2*i] >= zero && ctx->slack[2*i] > tiny )
                    ctx->w1[2*i] = fabs(ctx->u[2*i]);
                  if ( ctx->res[2*i] < zero && ctx->slack[2*i] <= -ctx->res[2*i]+tiny )
                    ctx->w1[2*i] =
                    max(ctx->ny*fabs(ctx->u[2*i])+ctx->tau,(fabs(ctx->u[2*i])+ctx->w1[2*i])*p5);
                  if ( ctx->res[2*i] < zero && ctx->slack[2*i] >  -ctx->res[2*i]+tiny )
                    ctx->w1[2*i] = fabs(ctx->u[2*i]);
               }
            }
            if ( ctx->w1[2*i-1] < ctx->w[2*i-1] || ctx->w1[2*i] < ctx->w[2*i] ) wlow = TRUE;
        }
        if ( wlow )
        {
            s1 = zero;
            s2 = zero;
            for (i = 1 ; i <= ctx->nres ; i++)
            {
                if ( ctx->low[i] == ctx->up[i]  )
                {
                    s1 += ctx->w1[2*i-1]*fabs(ctx->resst[2*i-1]);
                    s2 += ctx->w1[2*i-1]*fabs(ctx->res[2*i-1]);
                }
                else
                {
                    s1 -= min(zero,ctx->resst[2*i-1])*ctx->w1[2*i-1];
                    s2 -= min(zero,ctx->res[2*i-1]  )*ctx->w1[2*i-1];
                    s1 -= min(zero,ctx->resst[2*i])*ctx->w1[2*i];
                    s2 -= min(zero,ctx->res[2*i]  )*ctx->w1[2*i];

                }
            }
            diff0 = (ctx->fxst-ctx->fx)*ctx->scf+(s1-s2);
            if ( diff0 >= ctx->eta*ctx->clow && ctx->itstep-ctx->lastdw >= max(5,min(ctx->n/10,20)) ) {

                /* accept new (diminished ) weights */

                ctx->lastdw = ctx->itstep;
                ctx->lastch = ctx->itstep;
                ctx->level  = diff0/ctx->iterma;
                ctx->psist  = s1;
                ctx->psi    = s2;
                for (i = 1 ; i <= 2*ctx->nres ; i++) {
                    if ( ctx->w1[i] != ctx->w[i] ) ctx->lastch = ctx->itstep;
                    ctx->w[i] = ctx->w1[i];
                }
                ctx->clow = ctx->clow+one;
                if ( ctx->clow > ctx->itstep/10 ) {

                    /* additional increase of eta */

                    ctx->eta = ctx->eta*onep3;
                    if ( ! ctx->silent ) o8info(11, ctx);
                }
                if ( ! ctx->silent ) o8info(12, ctx);

                goto L1000;
            }
        }
        /* we cannot accept new weights */
        /* reset weights                */

        for (i = 1 ; i <= ctx->nres ; i++  )
        {
            ctx->w1[2*i-1] = ctx->w[2*i-1];
            ctx->w1[2*i  ] = ctx->w[2*i] ;
            if ( ctx->low[i] == ctx->up[i] )
            {
                if ( ctx->slack[2*i-1] > fabs(ctx->res[2*i-1]) )
                   ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
                if ( ctx->slack[2*i-1] <= fabs(ctx->res[2*i-1]) )
                {
                  if ( ctx->w[2*i-1] <= fabs(ctx->u[2*i-1])
                       && fabs(ctx->u[2*i-1]) <= ctx->w[2*i-1]+ctx->tau )
                  {
                      ctx->w1[2*i-1] = ctx->w[2*i-1]+two*ctx->tau;
                  }
                  else
                  {
                      ctx->w1[2*i-1] = max(ctx->w[2*i-1],ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau);
                  }
                }
            }
            else
            {
                if ( ctx->slack[2*i-1] > -min(-tiny,ctx->res[2*i-1]) && ctx->o8bind[2*i-1] == 1)
                {
                   ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
                }
                else if(  ctx->o8bind[2*i-1] == 1 &&
                          ctx->slack[2*i-1] <= -min(-tiny,ctx->res[2*i-1])
                          && ctx->u[2*i-1] <= ctx->w[2*i-1]+ctx->tau && ctx->w[2*i-1] >= ctx->u[2*i-1] )
                {

                    ctx->w1[2*i-1] = ctx->w[2*i-1]+two*ctx->tau;
                }
                else if ( ctx->o8bind[2*i-1] == 1 )
                {
                    ctx->w1[2*i-1] = max(ctx->w[2*i-1],ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau);
                }
                if ( ctx->slack[2*i] > -min(-tiny,ctx->res[2*i]) && ctx->o8bind[2*i] == 1 )
                {
                   ctx->w1[2*i] = fabs(ctx->u[2*i]);
                }
                else if ( ctx->o8bind[2*i] == 1 && ctx->slack[2*i] <= -min(-tiny,ctx->res[2*i])
                            && ctx->u[2*i] <= ctx->w[2*i]+ctx->tau && ctx->w[2*i] >= ctx->u[2*i] )
                {

                    ctx->w1[2*i] = ctx->w[2*i]+two*ctx->tau;
                }
                else if ( ctx->o8bind[2*i] == 1 )
                {
                    ctx->w1[2*i] = max(ctx->w[2*i],ctx->ny*fabs(ctx->u[2*i])+ctx->tau);
                }

            }
        }
        term1 = zero;
        for (i = 1 ; i <= 2*ctx->nres ; i++) {
            if ( ctx->w1[i] > ctx->w[i] || ctx->w1[i] < ctx->w[i] ) ctx->lastch = ctx->itstep;
            if ( ctx->w1[i] > ctx->w[i] ) ctx->lastup = ctx->itstep;
            if ( ctx->w1[i] < ctx->w[i] ) ctx->lastdw = ctx->itstep;
            ctx->w[i]  = ctx->w1[i];
            term1 = max(term1,ctx->w[i]);
        }
        s1 = zero;
        s2 = zero;
        for (i = 1 ; i <= ctx->nres ; i++) {
            if ( ctx->low[i] == ctx->up[i] )
            {
                s1 += ctx->w[2*i-1]*fabs(ctx->resst[2*i-1]);
                s2 += ctx->w[2*i-1]*fabs(ctx->res[2*i-1]);
            } else {
                s1 -= ctx->w[2*i-1]*min(zero,ctx->resst[2*i-1]);
                s2 -= ctx->w[2*i-1]*min(zero,ctx->res[2*i-1]);
                s1 -= ctx->w[2*i]*min(zero,ctx->resst[2*i]);
                s2 -= ctx->w[2*i]*min(zero,ctx->res[2*i]);

            }
        }
        ctx->psist = s1;
        ctx->psi   = s2;
        if ( ! ctx->silent ) o8info(12, ctx);
        ctx->accinf[ctx->itstep][20] = term1;
        ctx->accinf[ctx->itstep][19] = ctx->clow;

        goto L1000;
    }
   /************************************************************/
   /**     continue QP solver                                 **/
   /************************************************************/

    if ( ip > ctx->nr )
    {
    /* compute gradient of current QP inequality constraint */
    /* this is a linearized original constraint */
        k = (ctx->o8qpdu_iqlist[ip]-ctx->nr+1)/2 ;
        /*  k is in {1,...,nres} */
        if ( k > ctx->n )
        {   /* a general constraint */
          for (j = 1 ; j <= ctx->n ; j++)
          {
            ctx->o8qpdu_cii[j+incr] = ctx->gres[j][k-ctx->n]*ctx->gres[0][k-ctx->n];
          }
          for (j = 1 ; j <= incr ; j++)
          {
            ctx->o8qpdu_cii[j] = zero;
          }
          ctx->o8qpdu_cii[ctx->me+ip-ctx->nr] = one;
          ctx->o8qpdu_ci0[ip]       = ctx->res[ctx->o8qpdu_iqlist[ip]-ctx->nr];
        }
        else
        {  /* an active bound constraint */
           for ( j = 1 ; j <= ctx->ndual ; j++ )
           {
             ctx->o8qpdu_cii[j] = zero ;
           }
           ctx->o8qpdu_cii[ctx->me+ip-ctx->nr] = one ;
           ctx->o8qpdu_ci0[ip      ] = ctx->res[ctx->o8qpdu_iqlist[ip]-ctx->nr];
           ctx->o8qpdu_cii[k+incr  ] = ( (ctx->o8qpdu_iqlist[ip]-ctx->nr) %2 ==0  ) ? -ctx->xsc[k] : ctx->xsc[k] ;
        }

    }
    else
    {
        for (j = 1 ; j <= ctx->ndual ; j++)
        {
            ctx->o8qpdu_cii[j] = zero;
        }
        ctx->o8qpdu_ci0[ip] = zero;
        ctx->o8qpdu_cii[ip] = one;
    }
    for (i = 1 ; i <= ctx->ndual ; i++) {
        ctx->np[i] = ctx->o8qpdu_cii[i];
    }

    for (i = 1 ; i <= ctx->iq ; i++) {
        ctx->ud1[i] = ctx->ud[i];
    }
    ctx->ud1[ctx->iq+1] = zero;
    ctx->o8qpdu_ai[ctx->iq+1]  = ip;     /* this is in {1,..,mi} */

    L100:

    /* step 2a */

    o8zup(ctx->o8qpdu_z, ctx);

    if ( ctx->iq != 0 ) o8rup(ctx->o8qpdu_vr, ctx);

    l  = 0;
    t1 = infiny;
    for (k = 1 ; k <= ctx->iq ; k++) {
        if(ctx->o8qpdu_ai[k] > 0 && ctx->o8qpdu_vr[k] > zero) {
            if ( ctx->ud1[k]/ctx->o8qpdu_vr[k] < t1 ) {
                t1 = ctx->ud1[k]/ctx->o8qpdu_vr[k];
            }
        }
    }
    /* |z| = 0? */

    /* old      zz = o8sc1(1,ndual,o8qpdu_z,o8qpdu_z) */

    zz   = o8vecn(1,ctx->ndual,ctx->o8qpdu_z);
    term = o8sc1(1,ctx->ndual,ctx->o8qpdu_z,ctx->np);

    /* old      if (zz != zero && term > zero ) { */

    if ( zz >= tiny*ctx->rnorm && term > zero ) {
        t2 = -ctx->o8qpdu_s[ip]/term;
    } else {
        t2 = infiny;
    }
    t = min(t1,t2);

    if ( t >= infiny )
    {
      ctx->qpterm = -2 ;
      if ( ! ctx->silent ) o8msg(20, ctx);
      goto L2000;
    }


    if( t2 >= infiny ) {
        for (k = 1 ; k <= ctx->iq ; k++) {
            ctx->ud1[k] = ctx->ud1[k]+t*(-ctx->o8qpdu_vr[k]);
            if ( ctx->ud1[k] < zero && ctx->o8qpdu_ai[k] > 0 ) ctx->ud1[k] = zero;
        }
        ctx->ud1[ctx->iq+1] = ctx->ud1[ctx->iq+1]+t;
        ctx->o8qpdu_qpdel[0]  = 0;
        for (i = 1 ; i <= ctx->iq ; i++) {
            if ( ctx->ud1[i] <= tiny && ctx->o8qpdu_ai[i] > 0 ) {
                ctx->o8qpdu_qpdel[0]        = ctx->o8qpdu_qpdel[0]+1;
                ctx->o8qpdu_qpdel[ctx->o8qpdu_qpdel[0]] = ctx->o8qpdu_ai[i];
            }
        }
        for (k = 1 ; k <= ctx->o8qpdu_qpdel[0] ; k++) {
            l      = ctx->o8qpdu_qpdel[k];
            ctx->o8qpdu_iai[l] = l;

            o8dlcd(ctx->o8qpdu_ai,l, ctx);
        }
        goto L100;
    }
    for (k = 1 ; k <= ctx->ndual ; k++) {
        ctx->o8qpdu_xd[k] = ctx->o8qpdu_xd[k]+t*ctx->o8qpdu_z[k];
    }
    for (k = 1 ; k <= ctx->iq ; k++) {
        ctx->ud1[k] = ctx->ud1[k]+t*(-ctx->o8qpdu_vr[k]);
        if ( ctx->ud1[k] < zero && ctx->o8qpdu_ai[k] > 0 ) ctx->ud1[k] = zero;
    }
    ctx->ud1[ctx->iq+1] = ctx->ud1[ctx->iq+1]+t;

    f = f+t*o8sc1(1,ctx->ndual,ctx->o8qpdu_z,ctx->np)*(p5*t+ctx->ud1[ctx->iq+1]);
    if ( f <= fmax*(one+ctx->epsmac)+ctx->epsmac )
    {
      nosucc += 1 ;
      if ( nosucc > ctx->qpitma )
      {
        ctx->qpterm = - 3;
        if ( ! ctx->silent ) o8msg(26, ctx);
        goto L2000;
      }
    }
    else
    {
      nosucc = 0 ;
    }
    fmax = max( f , fmax ) ;

    if ( t2 <= t1-tiny ) {

        /* ddual is computed by o8zup */

        if ( o8vecn(ctx->iq+1,ctx->ndual,ctx->ddual) < ctx->epsmac*ctx->rnorm ) {

            /* degeneracy: adding this constraint gives a singular working set */
            /* theoretically impossible, but due to roundoff this may occur.   */
            /* mark this constraint and try to add another one                 */

            ctx->iptr = ip;
            ctx->iqtr = ctx->iq;
            for (i = 1 ; i <= ctx->iq ; i++) {
                ctx->aitr[i] = ctx->o8qpdu_ai[i];
            }
            ctx->sstr  = ss;
            ctx->riitr = o8vecn(ctx->iq+1,ctx->ndual,ctx->ddual);

            if ( ! ctx->silent ) o8msg(19, ctx);
            ctx->o8qpdu_iaexcl[ip] = 0;
            for (i = 1 ; i <= ctx->mi ; i++) {
                ctx->o8qpdu_iai[i] = i;
            }
            for (i = 1 ; i <= ctx->iq ; i++) {
                ctx->o8qpdu_ai[i] = ctx->o8qpdu_aiold[i];
                if (ctx->o8qpdu_ai[i] > 0 ) ctx->o8qpdu_iai[ctx->o8qpdu_ai[i]] = 0;
                ctx->ud1[i] = ctx->o8qpdu_udold[i];
            }
            for (i = 1 ; i <= ctx->ndual ; i++) {
                ctx->o8qpdu_xd[i] = ctx->o8qpdu_xdold[i];
            }
            goto L60;
        }
        /* add constraint, l-pair */

        o8adcd(ctx);

        ctx->o8qpdu_iai[ip] = 0;
        for (i = 1 ; i <= ctx->iq ; i++) {
            ctx->ud[i] = ctx->ud1[i];
        }
        goto L50;
    }
    su = zero;
    if ( ip > ctx->nr )
    {

      /* an original linearized inequality constraint */
      k = (ctx->o8qpdu_iqlist[ip]-ctx->nr+1)/2 ;
      /*  k is in {1,...,nres} */
      if ( k > ctx->n )
      {   /* a general constraint */
         for (j = 1 ; j <= ctx->n ; j++)
         {
           ctx->o8qpdu_cii[j+incr] = ctx->gres[j][k-ctx->n]*ctx->gres[0][k-ctx->n];
         }
         for (j = 1 ; j <= incr ; j++)
         {
           ctx->o8qpdu_cii[j] = zero;
         }
         ctx->o8qpdu_cii[ctx->me+ip-ctx->nr] = one;
         ctx->o8qpdu_ci0[ip]       = ctx->res[ctx->o8qpdu_iqlist[ip]-ctx->nr];
      }
      else
      {  /* an active bound constraint */
         for ( j = 1 ; j <= ctx->ndual ; j++ )
         {
           ctx->o8qpdu_cii[j] = zero ;
         }
         ctx->o8qpdu_cii[ctx->me+ip-ctx->nr] = one ;
         ctx->o8qpdu_ci0[ip      ] = ctx->res[ctx->o8qpdu_iqlist[ip]-ctx->nr];
         ctx->o8qpdu_cii[k+incr  ] = ( (ctx->o8qpdu_iqlist[ip]-ctx->nr) %2 ==0  ) ? -ctx->xsc[k] : ctx->xsc[k] ;
      }
      ctx->o8qpdu_s[ip] = o8sc1(1,ctx->ndual,ctx->o8qpdu_cii,ctx->o8qpdu_xd)+ctx->o8qpdu_ci0[ip];
    }
    else
    {
        /* a slack constraint */

        ctx->o8qpdu_s[ip] = ctx->o8qpdu_xd[ip];
    }
    /* now t = t1 */

    ctx->o8qpdu_qpdel[0] = 0;
    for (i = 1 ; i <= ctx->iq ; i++) {
        if ( ctx->ud1[i] <= tiny && ctx->o8qpdu_ai[i] > 0 ) {
            ctx->o8qpdu_qpdel[0]        = ctx->o8qpdu_qpdel[0]+1;
            ctx->o8qpdu_qpdel[ctx->o8qpdu_qpdel[0]] = ctx->o8qpdu_ai[i];
        }
    }
    for (k = 1 ; k <= ctx->o8qpdu_qpdel[0] ; k++) {
        l      = ctx->o8qpdu_qpdel[k];
        ctx->o8qpdu_iai[l] = l;

        o8dlcd(ctx->o8qpdu_ai,l, ctx);
    }
    if ( t2 <= t1+tiny ) {
        if ( o8vecn(ctx->iq+1,ctx->ndual,ctx->ddual) < ctx->epsmac*ctx->rnorm ) {

            /* degeneracy */

            ctx->iptr = ip;
            ctx->iqtr = ctx->iq;
            for (i = 1 ; i <= ctx->iq ; i++) {
                ctx->aitr[i] = ctx->o8qpdu_ai[i];
            }
            ctx->sstr  = ss;
            ctx->riitr = o8vecn(ctx->iq+1,ctx->ndual,ctx->ddual);

            if ( ! ctx->silent ) o8msg(19, ctx);
            ctx->o8qpdu_iaexcl[ip] = 0;
            for (i = 1 ; i <= ctx->mi ; i++) {
                ctx->o8qpdu_iai[i] = i;
            }
            for (i = 1 ; i <= ctx->iq ; i++) {
                ctx->o8qpdu_ai[i] = ctx->o8qpdu_aiold[i];
                if ( ctx->o8qpdu_ai[i] > 0 ) ctx->o8qpdu_iai[ctx->o8qpdu_ai[i]] = 0;
                ctx->ud1[i] = ctx->o8qpdu_udold[i];
            }
            for (i = 1 ; i <= ctx->ndual ; i++) {
                ctx->o8qpdu_xd[i] = ctx->o8qpdu_xdold[i];
            }
            goto L60;
        }
        /* add constraint, l-pair */

        o8adcd(ctx);

        ctx->o8qpdu_iai[ip] = 0;
        for (i = 1 ; i <= ctx->iq ; i++) {
            ctx->ud[i] = ctx->ud1[i];
        }
        goto L50;

    } else {

        goto L100;
    }
    /* this is the exit point of o8qpdu                                     */
    /* we either may have successful or unsuccessful termination here       */
    /* the latter with qpterm = -2 or -3, in which case it may nevertheless */
    /* be possible to use the computed d. -2 or -3 exit is theoretically    */
    /* impossible but may occur due to roundoff effects.                    */
    /* we check the directional derivative of the penalty-function now      */

    L1000:

    /* cut and rescale d if appropriate */

    o8cutd(ctx);

    /* compute the directional derivative dirder */

    o8dird(ctx);

    if ( ctx->dirder >= zero || ( -ctx->dirder <= ctx->epsmac*tp2*(ctx->scf*fabs(ctx->fx)+ctx->psi+one) &&
        ctx->infeas > max(ctx->upsi,ctx->nres*ctx->delmin) ) ) {
        if ( ! ctx->silent ) o8msg(18, ctx);
        if ( ctx->tauqp <= ctx->taumax/ctx->taufac ) {
            ctx->tauqp = ctx->tauqp*ctx->taufac;

            goto L10;

        } else {
            if ( ! ctx->silent ) o8msg(5, ctx);
            ctx->qpterm = -1;
            ctx->accinf[ctx->itstep][30] = ctx->qpterm;
            ctx->accinf[ctx->itstep][31] = ctx->tauqp;
            ctx->accinf[ctx->itstep][32] = ctx->infeas;
            for (i = 1 ; i <= ctx->n ; i++) {
                ctx->d[i] = zero;
            }
            ctx->dnorm = zero;
        }
    }
    return;

    L2000:

    /* QP infeasible ( in this application impossible , theoretically) */


    ctx->accinf[ctx->itstep][30] = -two;
    ctx->accinf[ctx->itstep][13] = condr;
    ctx->accinf[ctx->itstep][14] = c1*c2;
    for (i = 1 ; i <= ctx->n ; i++) {
        ctx->d[i] = ctx->o8qpdu_xd[i+incr];
    }
    ctx->dnorm = o8vecn(1,ctx->n,ctx->d);
    su1   = zero;
    for (i = 1 ; i <= incr ; i++) {
        su1 = su1+fabs(ctx->o8qpdu_xd[i]);
    }
    /* L1-norm of slack variables */

    ctx->accinf[ctx->itstep][32] = su1;
    wlow = FALSE;
    su1  = zero;
    su2  = zero;
    for (i = 1 ; i <= ctx->iq ; i++) {
        if ( ctx->o8qpdu_ai[i] < 0 ) {
            ctx->u[-ctx->o8qpdu_ai[i]] = ctx->ud[i];
        } else {
            if ( ctx->o8qpdu_ai[i] > ctx->nr ) ctx->u[ctx->o8qpdu_iqlist[ctx->o8qpdu_ai[i]]-ctx->nr] = ctx->ud[i];
            /*  o8qpdu_iqlist[ai[i]] = aalist[k]+nr for some k */
        }
    }
    /* compute Lagrangian violation */
    term1 = zero;
    for (j = 1 ; j <= ctx->n ; j++) {
        ctx->np[j] = ctx->gradf[j]*ctx->scf;
    }
    for (i = 1 ; i <= ctx->nres ; i++)
    {
      if ( i > ctx->n )
      {
        for (j = 1 ; j <= ctx->n ; j++)
        {
          ctx->np[j] = ctx->np[j]-ctx->gres[j][i-ctx->n]*(ctx->u[2*i-1]-ctx->u[2*i]);
        }
      }
      else
      {
          ctx->np[i] -= ctx->xsc[i]*(ctx->u[2*i-1]-ctx->u[2*i]) ;
      }
      ctx->w1[2*i-1] = ctx->w[2*i-1];
      ctx->w1[2*i  ] = ctx->w[2*i] ;
      if ( ctx->low[i] == ctx->up[i]  )
      {
        if ( ctx->slack[2*i-1] > fabs(ctx->res[2*i-1]) ) ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
        if ( ctx->slack[2*i-1] <= fabs(ctx->res[2*i-1]) )
        {
           if ( ctx->w[2*i-1] <= fabs(ctx->u[2*i-1])
                && fabs(ctx->u[2*i-1]) <= ctx->w[2*i-1]+ctx->tau )
           {
               ctx->w1[2*i-1] = ctx->w[2*i-1]+two*ctx->tau;
           }
           else
           {
               ctx->w1[2*i-1] = max(ctx->w[2*i-1],ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau);
           }
        }
        su1 += fabs(ctx->res[2*i-1]  )*ctx->w1[2*i-1];
        su2 += fabs(ctx->resst[2*i-1])*ctx->w1[2*i-1];
      }
      else
      {
        if ( ctx->slack[2*i-1] > -min(-tiny,ctx->res[2*i-1]) && ctx->o8bind[2*i-1] == 1 )
        {
           ctx->w1[2*i-1] = fabs(ctx->u[2*i-1]);
        }
        else if ( ctx->o8bind[2*i-1] == 1 && ctx->slack[2*i-1] <= -min(-tiny,ctx->res[2*i-1])
                  && ctx->u[2*i-1] <= ctx->w[2*i-1]+ctx->tau && ctx->w[2*i-1] >= ctx->u[2*i-1] )
        {
           ctx->w1[2*i-1] = ctx->w[2*i-1]+two*ctx->tau;
        }
        else if ( ctx->o8bind[2*i-1] == 1 )
        {
           ctx->w1[2*i-1] = max(ctx->w[2*i-1],ctx->ny*fabs(ctx->u[2*i-1])+ctx->tau);
        }
        su1 -= ctx->w1[2*i-1]*min(zero,ctx->res[2*i-1]);
        su2 -= ctx->w1[2*i-1]*min(zero,ctx->resst[2*i-1]);

        if ( ctx->slack[2*i] > -min(-tiny,ctx->res[2*i]) && ctx->o8bind[2*i] == 1 )
        {
           ctx->w1[2*i] = fabs(ctx->u[2*i]);
        }
        else if ( ctx->o8bind[2*i] == 1 && ctx->slack[2*i] <= -min(-tiny,ctx->res[2*i])
                  && ctx->u[2*i] <= ctx->w[2*i]+ctx->tau && ctx->w[2*i] >= ctx->u[2*i] )
        {
           ctx->w1[2*i] = ctx->w[2*i]+two*ctx->tau;
        }
        else if ( ctx->o8bind[2*i] == 1 )
        {
           ctx->w1[2*i] = max(ctx->w[2*i],ctx->ny*fabs(ctx->u[2*i])+ctx->tau);
        }
        su1 -= ctx->w1[2*i]*min(zero,ctx->res[2*i]);
        su2 -= ctx->w1[2*i]*min(zero,ctx->resst[2*i]);

      }
      if ( ctx->w[i] != ctx->w1[i] ) ctx->lastch = ctx->itstep;
      ctx->w[i]  = ctx->w1[i];
      term1 = max(term1,ctx->w[i]);
    }
    ctx->psist = su2;
    ctx->psi   = su1;

    ctx->b2n   = sqrt(o8sc1(1,ctx->n,ctx->np,ctx->np));

    if ( ctx->scf != zero ) ctx->b2n = ctx->b2n/ctx->scf;
    if ( wlow ) {
        ctx->clow   = ctx->clow+one;
        ctx->lastch = ctx->itstep;
        ctx->lastdw = ctx->itstep;
    }
    if ( ! ctx->silent ) o8info(12, ctx);
    ctx->accinf[ctx->itstep][19] = ctx->clow;
    ctx->accinf[ctx->itstep][20] = term1;
    ctx->accinf[ctx->itstep][31] = ctx->tauqp;
        if ( ctx->upsi <= ctx->delmin*ctx->nres
            && ctx->b2n <= (ctx->gfn+one)*ctx->epsx*tp2 && ctx->phase >= 0
            && ctx->infeas <= ctx->delmin*ctx->nres )
    {

      /* since multipliers may be incorrect for infeas != zero be careful */
      /* we consider the problem as successfully solved with reduced      */
      /* requirements                                                     */

            for (i = 1 ; i <= ctx->n ; i++) {
                ctx->d[i] = zero;
            }
            ctx->dnorm  = zero;
            ctx->optite = three;
            ctx->qpterm = 1 ;   /* new */
            return;
    }

    goto L1000;
}

/* **************************************************************************** */
/*                  compute updated projected gradient (primal)                 */
/* **************************************************************************** */
void o8zup(DOUBLE z[], TLContext *ctx) {

     INTEGER  i,j;
     DOUBLE   su;

    /* d = j(trans) *np */

    for (i = 1 ; i <= ctx->ndual ; i++) {
        su = zero;
        for (j = 1 ; j <= ctx->ndual ; j++) {
            su = su+ctx->xj[j][i]*ctx->np[j];
        }
        ctx->ddual[i] = su;
    }
    /* computation of z */

    for (i = 1 ; i <= ctx->ndual ; i++) {
        z[i] = zero;
        for (j = ctx->iq+1 ; j <= ctx->ndual ; j++) {
            z[i] = z[i]+ctx->xj[i][j]*ctx->ddual[j];
        }
    }
    return;
}

/* **************************************************************************** */
/*                    compute correction of dual multipliers                    */
/* **************************************************************************** */
void o8rup(DOUBLE rv[], TLContext *ctx) {

     DOUBLE   s;
     INTEGER  i,j;

    for (i = ctx->iq ; i >= 1 ; i--) {
        s = zero;
        for (j = i+1 ; j <= ctx->iq ; j++) {
            s = s+ctx->r[i][j]*rv[j];
        }
        rv[i] = (ctx->ddual[i]-s)/ctx->r[i][i];
    }
    return;
}

/* **************************************************************************** */
/*                          delete constraint nr. l                             */
/* **************************************************************************** */
void o8dlcd(INTEGER ai[],INTEGER l, TLContext *ctx) {

     INTEGER  qq,i,j,k;
     DOUBLE   t1,t2,cc,ss,h,c1,s1,xny;

    for (i = 1 ; i <= ctx->iq ; i++) {
        if ( ai[i] == l ) {
            qq = i;

            goto L10;
         }
    }
    L10:

    for (i = qq ; i <= ctx->iq-1 ; i++) {
        ai[i]  = ai[i+1];
        ctx->ud1[i] = ctx->ud1[i+1];
        for (j = 1 ; j <= ctx->ndual ; j++) {
            ctx->r[j][i] = ctx->r[j][i+1];
        }
    }
    L20:

    ai[ctx->iq]    = ai[ctx->iq+1];
    ctx->ud1[ctx->iq]   = ctx->ud1[ctx->iq+1];
    ai[ctx->iq+1]  = 0;
    ctx->ud1[ctx->iq+1] = zero;
    for (j = 1 ; j <= ctx->iq ; j++) {
        ctx->r[j][ctx->iq] = zero;
    }
    ctx->iq = ctx->iq-1;

    if ( ctx->iq == 0 ) goto L100;

    for (j = qq ; j <= ctx->iq ; j++) {
        cc = ctx->r[j][j];
        ss = ctx->r[j+1][j];
        h  = o8dsq1(cc,ss);

        if ( h == zero ) goto L90;

        c1 = cc/h;
        s1 = ss/h;
        ctx->r[j+1][j] = zero;
        if ( c1 < zero ) {
            ctx->r[j][j] = -h;
            c1      = -c1;
            s1      = -s1;
        } else {
            ctx->r[j][j] = h;
        }
        xny = s1/(one+c1);
        for (k = j+1 ; k <= ctx->iq ; k++) {
            t1        = ctx->r[j][k];
            t2        = ctx->r[j+1][k];
            ctx->r[j][k]   = t1*c1+t2*s1;
            ctx->r[j+1][k] = xny*(t1+ctx->r[j][k])-t2;
        }
        for (k = 1 ; k <= ctx->ndual ; k++) {
            t1         = ctx->xj[k][j];
            t2         = ctx->xj[k][j+1];
            ctx->xj[k][j]   = t1*c1+t2*s1;
            ctx->xj[k][j+1] = xny*(ctx->xj[k][j]+t1)-t2;
        }
        L90:;
    }
    L100:

    ctx->rnorm = one;
    ctx->rlow  = one;

    /* in order to avoid a compiler error of hp in +op3 mode */

    if ( ctx->iq >= 1 ) {
        ctx->rnorm = fabs(ctx->r[1][1]);
        ctx->rlow  = fabs(ctx->r[1][1]);
        i     = 1;
        while ( i < ctx->iq ) {
            i     = i+1;
            ctx->rnorm = max(ctx->rnorm,fabs(ctx->r[i][i]));
            ctx->rlow  = min(ctx->rlow, fabs(ctx->r[i][i]));
        }
    }
    return;
}

/* **************************************************************************** */
/*                  add constraint whose gradient is given by np                */
/* **************************************************************************** */
void o8adcd(TLContext *ctx) {

     INTEGER  i,j,k;
     DOUBLE   cc,ss,h,s1,c1,t1,t2,xny;

    for (j = ctx->ndual ; j >= ctx->iq+2 ; j--) {
        cc = ctx->ddual[j-1];
        ss = ctx->ddual[j];
        h  = o8dsq1(cc,ss);

        if ( h == zero ) goto L20;

        ctx->ddual[j] = zero;
        s1       = ss/h;
        c1       = cc/h;
        if ( c1 < zero ) {
            c1         = -c1;
            s1         = -s1;
            ctx->ddual[j-1] = -h;
        } else {
            ctx->ddual[j-1] = h;
        }
        xny = s1/(one+c1);
        for (k = 1 ; k <= ctx->ndual ; k++) {
            t1         = ctx->xj[k][j-1];
            t2         = ctx->xj[k][j];
            ctx->xj[k][j-1] = t1*c1+t2*s1;
            ctx->xj[k][j]   = xny*(t1+ctx->xj[k][j-1])-t2;
        }
        L20:;
    }
    ctx->iq = ctx->iq+1;
    for (i = 1 ; i <= ctx->iq ; i++) {
        ctx->r[i][ctx->iq] = ctx->ddual[i];
    }
    ctx->rnorm = one;
    ctx->rlow  = one;

    /* in order to avoid a compiler error of hp in +op3 mode */

    if ( ctx->iq >= 1 ) {
        ctx->rnorm = fabs(ctx->r[1][1]);
        ctx->rlow  = fabs(ctx->r[1][1]);
        i     = 1;
        while ( i < ctx->iq ) {
            i     = i+1;
            ctx->rnorm = max(ctx->rnorm,fabs(ctx->r[i][i]));
            ctx->rlow  = min(ctx->rlow, fabs(ctx->r[i][i]));
        }
    }
    return;
}

/* **************************************************************************** */
/*          computes the inverse of the upper triangular matrix part            */
/* **************************************************************************** */
void o8rinv(INTEGER n,DOUBLE **a,INTEGER ndual,DOUBLE **x) {

    /* computes the inverse of the upper triangular matrix part */
    /* of a and stores it in the upper triangle of the          */
    /* right lower minor of x                                   */
    /* actual dimension of a is n and of x ndual                */

     INTEGER  l,j,k,incr;
     DOUBLE   su;

    incr = ndual-n;

    /* incr = nr */

    for (j = n ; j >= 1 ; j--) {

        /* we assume a being sufficiently regular here. given in this */
        /* application. See top of o8qpdu                             */

        x[j+incr][j+incr] = one/a[j][j];
        for (k = j-1 ; k >= 1 ; k--) {
            su = zero;
            for (l = k+1 ; l <= j ; l++) {
                su = su+a[k][l]*x[l+incr][j+incr];
            }
            x[k+incr][j+incr] = -su/a[k][k];
        }
    }
    return;
}

/* **************************************************************************** */
/* suite of function interfaces, for performing scaling and                     */
/* external evaluations                                                         */
/* if bloc = TRUE then it is assumed that prior to calls to es<xyz>             */
/* valid function information is computed (via a call of user_eval)             */
/* and stored in fu and fugrad , setting valid = TRUE afterwards                */
/* **************************************************************************** */

/* **************************************************************************** */
/*                              objective function                              */
/* **************************************************************************** */
void esf(DOUBLE x[],DOUBLE *fx, TLContext *ctx)
{

     INTEGER  i;

    if ( ctx->bloc )
    {
        if ( ctx->valid )
        {
            *fx = ctx->fu[0];
        }
        else
        {
	        ctx->OPSlvErrorCode = OPSLV1_ERR_BLOC_CALL_FUNC_INFO_INVALID;
	        ctx->OPSlvErrorInfo = 0;
            sprintf(ctx->ErrorText,"bloc-call, function info invalid");
            return;
            /*fprintf(stderr,"donlp2: bloc-call, function info invalid\n");
            exit(1);*/
        }
    }
    else
    {
        for (i = 1 ; i <= ctx->n ; i++)
        {
            ctx->xtr[i] = x[i]*ctx->xsc[i];
        }
        ef(ctx->xtr,fx, ctx);

    }
    return;
}

/* **************************************************************************** */
/*                        gradient of objective function                        */
/* **************************************************************************** */
void esgradf(DOUBLE x[],DOUBLE gradf[], TLContext *ctx)
{

     INTEGER  j;
     DOUBLE   d1,d2,d3,sd1,sd2,sd3,fhelp,fhelp1,fhelp2,
                    fhelp3,fhelp4,fhelp5,fhelp6,xincr,xhelp,floc;

    if ( ctx->bloc )
    {
        if ( ctx->valid )
        {
            for (j = 1 ; j <= ctx->n ; j++)
            {
                gradf[j] = ctx->xsc[j]*ctx->fugrad[j][0];
            }
            return;
        }
        else
        {
	        ctx->OPSlvErrorCode = OPSLV1_ERR_BLOC_CALL_FUNC_INFO_INVALID;
	        ctx->OPSlvErrorInfo = 1;
            sprintf(ctx->ErrorText,"bloc call with function info invalid");
			return;
            /*fprintf(stderr,"donlp2: bloc call with function info invalid\n");
            exit(1);*/
        }
    }
    else
    {
        for (j = 1 ; j <= ctx->n ; j++)
        {
            ctx->xtr[j] = ctx->xsc[j]*x[j];
        }
        if ( ctx->analyt )
        {

             egradf(ctx->xtr,gradf, ctx);

        }
        else
        {
            if ( ctx->difftype == 1 )
            {
                ctx->deldif = min(tm1*sqrt(ctx->epsfcn),tm2);

                ef(ctx->xtr,&floc, ctx);

                for (j = 1 ; j <= ctx->n ; j++)
                {
                    xhelp = ctx->xtr[j];
                    xincr = min(min(tm2,ctx->deldif*fabs(xhelp)+ctx->deldif),ctx->taubnd);
                    if ( xhelp >= zero )
                    {
                        ctx->xtr[j] = xhelp+xincr;
                    }
                    else
                    {
                        ctx->xtr[j] = xhelp-xincr;
                    }
                    ef(ctx->xtr,&fhelp, ctx);

                    gradf[j] = (fhelp-floc)/(ctx->xtr[j]-xhelp);
                    ctx->xtr[j]   = xhelp;
                }
            }
            else if ( ctx->difftype == 2 )
            {
                ctx->deldif = min(tm1*pow(ctx->epsfcn,one/three),tm2);
                for (j = 1 ; j <= ctx->n ; j++)
                {
                    xhelp  = ctx->xtr[j];
                    xincr  = min(min(tm2,ctx->deldif*fabs(xhelp)+ctx->deldif),ctx->taubnd);
                    ctx->xtr[j] = xhelp+xincr;

                    ef(ctx->xtr,&fhelp1, ctx);

                    ctx->xtr[j] = xhelp-xincr;

                    ef(ctx->xtr,&fhelp2, ctx);

                    gradf[j] = (fhelp1-fhelp2)/(xincr+xincr);
                    ctx->xtr[j]   = xhelp;
                }
            }
            else   /* difftype ==3 */
            {
                ctx->deldif = min(tm1*pow(ctx->epsfcn,one/seven),tm2);
                for (j = 1 ; j <= ctx->n ; j++)
                {
                   xhelp  = ctx->xtr[j];
                   xincr  = min(min(tm2,ctx->deldif*fabs(xhelp)+ctx->deldif),ctx->taubnd/four);
                   ctx->xtr[j] = xhelp-xincr;

                   ef(ctx->xtr,&fhelp1, ctx);

                   ctx->xtr[j] = xhelp+xincr;

                   ef(ctx->xtr,&fhelp2, ctx);

                   xincr  = xincr+xincr;
                   d1     = xincr;
                   ctx->xtr[j] = xhelp-xincr;

                   ef(ctx->xtr,&fhelp3, ctx);

                   ctx->xtr[j] = xhelp+xincr;

                   ef(ctx->xtr,&fhelp4, ctx);

                   xincr  = xincr+xincr;
                   d2     = xincr;
                   ctx->xtr[j] = xhelp-xincr;

                   ef(ctx->xtr,&fhelp5, ctx);

                   ctx->xtr[j] = xhelp+xincr;

                   ef(ctx->xtr,&fhelp6, ctx);

                   ctx->xtr[j]   = xhelp;
                   d3       = xincr+xincr;
                   sd1      = (fhelp2-fhelp1)/d1;
                   sd2      = (fhelp4-fhelp3)/d2;
                   sd3      = (fhelp6-fhelp5)/d3;
                   sd3      = sd2-sd3;
                   sd2      = sd1-sd2;
                   sd3      = sd2-sd3;
                   gradf[j] = sd1+p4*sd2+sd3/c45;
                }
            }
        }
        for (j = 1 ; j <= ctx->n ; j++)
        {
            gradf[j] = ctx->xsc[j]*gradf[j];
        }
    }
    return;
}

/********************************************************************/

void escon( INTEGER call_type , INTEGER liste[], DOUBLE x[], DOUBLE constr[],
            LOGICAL errliste[], TLContext *ctx)
{
INTEGER j    ;

     if ( ctx->bloc )
     {
        if ( ctx->valid )
        {
            if ( call_type == 1 )
            {
               for ( j = 1 ; j <= ctx->nonlin ; j++ )
               {
                 constr[j] = ctx->fu [j] ;
                 errliste[j] = ctx->confuerr[j] ;
               }
            }
            else
            {
               for ( j = 1 ; j <= liste[0] ; j++ )
               {
                 constr[liste[j]] = ctx->fu[liste[j]] ;
                 errliste[liste[j]] = ctx->confuerr[liste[j]] ;
               }
            }
        }
        else
        {
	        ctx->OPSlvErrorCode = OPSLV1_ERR_BLOC_CALL_FUNC_INFO_INVALID;
	        ctx->OPSlvErrorInfo = 2;
            sprintf(ctx->ErrorText,"bloc call with function info invalid");
			return;
            /*fprintf(stderr,"donlp2: bloc call with function info invalid\n");
            exit(1);*/
        }
     }
     else
     {
        for (j = 1 ; j <= ctx->n ; j++)
        {
            ctx->xtr[j] = x[j]*ctx->xsc[j];
        }
        econ(call_type,liste,ctx->xtr,constr,errliste, ctx);
     }
     return;
}
void  escongrad(INTEGER liste[], INTEGER shift , DOUBLE x[],
 DOUBLE **grad_constr, TLContext *ctx)
/********************************************************************/
/*  evaluate gradients of nonlinear constraints from liste for the  */
/*  internally scaled x                                             */
/********************************************************************/
{
/* econgrad : the gradient for the external unscaled x */
 INTEGER i,j   ;
 DOUBLE   d1,d2,d3,sd1,sd2,sd3,
                xincr,xhelp;

/*  after rescaling x , evaluate the gradients of the nonlinear constraints */
/*  given by liste                                                          */

    if ( ctx->bloc )
    {
        if ( ctx->valid )
        {
           for ( i = 1 ; i <= liste[0] ; i++ )
           {
             for (j = 1 ; j <= ctx->n ; j++)
             {
                grad_constr[j][shift+liste[i]] = ctx->xsc[j]*ctx->fugrad[j][liste[i]];
             }
           }
           return;
        }
        else
        {
	        ctx->OPSlvErrorCode = OPSLV1_ERR_BLOC_CALL_FUNC_INFO_INVALID;
	        ctx->OPSlvErrorInfo = 3;
            sprintf(ctx->ErrorText,"bloc call with function info invalid");
			return;
            /*fprintf(stderr,"donlp2: bloc call with function info invalid\n");
            exit(1);*/
        }
    }
    else
    {
       for (j = 1 ; j <= ctx->n ; j++)
       {
          ctx->xtr[j] = x[j]*ctx->xsc[j];
       }
       if ( ctx->analyt )
       {

            econgrad(liste,shift,ctx->xtr,grad_constr, ctx);
            for ( j = 1 ; j <= ctx->n ; j++ )
            {
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                grad_constr[j][shift+liste[i]] *= ctx->xsc[j] ;
              }
            }
       }
       else
       {
          if ( ctx->difftype == 1 )
          {
            ctx->deldif = min(tm1*sqrt(ctx->epsfcn),tm2);
            econ(2,liste,ctx->xtr,ctx->escongrad_fhelp1,ctx->escongrad_errloc, ctx);
            for ( i = 1 ; i <= liste[0] ; i++ )
            {
              if ( ctx->escongrad_errloc[liste[i]] )
              {
	            ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
	            ctx->OPSlvErrorInfo = liste[i];
                sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
				return;
                /*fprintf(stderr,"donlp2: error in evaluating \n");
                fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                fprintf(stderr,"during numerical differentiation \n");
                exit(1);*/
              }
            }
            for (j = 1 ; j <= ctx->n ; j++)
            {
              xhelp = ctx->xtr[j];
              xincr = min(min(tm2,ctx->deldif*fabs(xhelp)+ctx->deldif),ctx->taubnd);
              if ( xhelp >= zero )
              {
                 ctx->xtr[j] = xhelp+xincr;
              }
              else
              {
                 ctx->xtr[j] = xhelp-xincr;
              }

              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp2,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
				  return;
                  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                grad_constr[j][shift+liste[i]] =
                   ctx->xsc[j]*(ctx->escongrad_fhelp2[liste[i]]-ctx->escongrad_fhelp1[liste[i]])/(ctx->xtr[j]-xhelp);
              }
              ctx->xtr[j] = xhelp ;
            }
          }
          else if ( ctx->difftype == 2 )
          {
            ctx->deldif = min(tm1*pow(ctx->epsfcn,one/three),tm2);
            for (j = 1 ; j <= ctx->n ; j++)
            {
              xhelp  = ctx->xtr[j];
              xincr  = min(min(tm2,ctx->deldif*fabs(xhelp)+ctx->deldif),ctx->taubnd);
              ctx->xtr[j] = xhelp+xincr;

              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp1,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              ctx->xtr[j] = xhelp-xincr;

              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp2,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              for ( i = 1 ; i <= liste[0] ; i++)
              {
                grad_constr[j][shift+liste[i]] =
                    ctx->xsc[j]*(ctx->escongrad_fhelp1[liste[i]]-ctx->escongrad_fhelp2[liste[i]])/(xincr+xincr);
              }
              ctx->xtr[j]    = xhelp;
            }
          }
          else
          {
            ctx->deldif = min(tm1*pow(ctx->epsfcn,one/seven),tm2);
            for (j = 1 ; j <= ctx->n ; j++)
            {
              xhelp  = ctx->xtr[j];
              xincr  = min(min(tm2,ctx->deldif*fabs(xhelp)+ctx->deldif),ctx->taubnd/four);
              ctx->xtr[j] = xhelp-xincr;

              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp1,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              ctx->xtr[j] = xhelp+xincr;
              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp2,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              xincr  = xincr+xincr;
              d1     = xincr;
              ctx->xtr[j] = xhelp-xincr;
              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp3,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              ctx->xtr[j] = xhelp+xincr;
              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp4,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              xincr  = xincr+xincr;
              d2     = xincr;
              ctx->xtr[j] = xhelp-xincr;

              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp5,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              ctx->xtr[j] = xhelp+xincr;
              econ(2,liste,ctx->xtr,ctx->escongrad_fhelp6,ctx->escongrad_errloc, ctx);
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                if ( ctx->escongrad_errloc[liste[i]] )
                {
	              ctx->OPSlvErrorCode = OPSLV1_ERR_EVAL_NONLIN_USER_NUM_DIFF;
  	              ctx->OPSlvErrorInfo = liste[i];
                  sprintf(ctx->ErrorText,"error in evaluating nonlinear user function %i during numerical differentiation", liste[i]);
                  return;
				  /*fprintf(stderr,"donlp2: error in evaluating \n");
                  fprintf(stderr,"nonlinear user function %i \n",liste[i]);
                  fprintf(stderr,"during numerical differentiation \n");
                  exit(1);*/
                }
              }

              ctx->xtr[j]    = xhelp;
              d3        = xincr+xincr;
              for ( i = 1 ; i <= liste[0] ; i++ )
              {
                 sd1       = (ctx->escongrad_fhelp2[liste[i]]-ctx->escongrad_fhelp1[liste[i]])/d1;
                 sd2       = (ctx->escongrad_fhelp4[liste[i]]-ctx->escongrad_fhelp3[liste[i]])/d2;
                 sd3       = (ctx->escongrad_fhelp6[liste[i]]-ctx->escongrad_fhelp5[liste[i]])/d3;
                 sd3       = sd2-sd3;
                 sd2       = sd1-sd2;
                 sd3       = sd2-sd3;
                 grad_constr[j][shift+liste[i]] =
                 ctx->xsc[j]*(sd1+p4*sd2+sd3/c45);
              }  /* end for i */
            } /* end for j */
          }
       } /* end if analyt else */
    } /* end if bloc else */
    return;
}
/* **************************************************************************** */
/* suite of functions to allocate and free memory for dynamic memory allocation */
/* **************************************************************************** */

#ifdef ARRAY_BORDER_CHECK
/**********************************************************************/
/***           check 1D array borders for any violations        *******/
/**********************************************************************/
void checkArrayBorders(char *str)
{
    INTEGER i, violations;
    violations = 0;

    /* Check integer borders */
    for (i=0; i<i_borders_count; i++) {
        if (**(i_borders+2*i) != i_unique) {
            printf("checkArrayBorders: Integer 1D array index %4d:  start border violated\n", i);
            fflush(stdout);
            violations++;
        }
        if (**(i_borders+2*i+1) != i_unique) {
            printf(" checkArrayBorders: Integer 1D array index %4d:  end border violated\n", i);
            fflush(stdout);
            violations++;
        }
    }

    /* Check double borders */
    for (i=0; i<d_borders_count; i++) {
        if (**(d_borders+2*i) != d_unique) {
            printf("checkArrayBorders: Double  1D array index %4d:  start border violated\n", i);
            fflush(stdout);
            violations++;
        }
        if (**(d_borders+2*i+1) != d_unique) {
            printf("checkArrayBorders: Double  1D array index %4d:  end border violated\n", i);
            fflush(stdout);
            violations++;
        }
    }

    /* Check logical borders */
    for (i=0; i<l_borders_count; i++) {
        if (**(l_borders+2*i) != l_unique) {
            printf(" checkArrayBorders: Logical 1D array index %4d:  start border violated\n", i);
            fflush(stdout);
            violations++;
        }
        if (**(l_borders+2*i+1) != l_unique) {
            printf(" checkArrayBorders: Logical 1D array index %4d:  end border violated\n", i);
            fflush(stdout);
            violations++;
        }
    }

    if (violations == 0)
        printf("checkArrayBorders: no violations: %s\n", str);
    else
        printf("checkArrayBorders: ************** violations %4d:  %s\n", violations, str);
    fflush(stdout);
}
#endif


/**********************************************************************/
/***              allocate memory for integer 1D array          *******/
/**********************************************************************/

INTEGER* i1_malloc(INTEGER size1, INTEGER init, TLContext *ctx)
{

    /* Allocate INTEGER precision array with length "size1"          */
    /* assuming zero origin.  If initialize is non zero             */
    /* then initize the allocated array to zero.                    */

    INTEGER* array;
    INTEGER i,j;

    array = (INTEGER*) malloc((size_t) (size1+2*ABC)*sizeof(INTEGER));
    if (!array) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"i1_malloc: memory error: malloc failed");
		return NULL;
        /*fprintf(stderr, "ERROR: i1_malloc: memory error: malloc failed");
        exit(-1);*/
    }

#ifdef ARRAY_BORDER_CHECK
    /* setup array borders */
    if (i_borders_count > 2*ABC_NUM_1DARRAYS-4) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"ARRAYBORDERS: ABC_NUM_1DARRAYS is too small");
		return NULL;
        /*printf("ERROR: ARRAYBORDERS: ABC_NUM_1DARRAYS is too small\n");
	    exit(-1);*/
    }
    array[0] = i_unique;
    i_borders[2*i_borders_count] = &(array[0]);
    array[size1+1] = i_unique;
    i_borders[2*i_borders_count+1] = &(array[size1+1]);
    i_borders_count++;
    array++;
#endif

    /* initialize the array to 0 */
    if (init) {
        for (i=0; i<size1; i++)
            array[i] = 0;
    }

    return array;
}


/**********************************************************************/
/***                free memory for INTEGER 1D array             *******/
/**********************************************************************/
void i1_free(INTEGER* array, TLContext *ctx)
{
    INTEGER i;

    /* Check for null pointer */
    if (!array) {
        ctx->OPSlvWarningCode = OPSLV1_WARN_FREE_MEMORY;
        // OPSlvWarningInfo = ;
        sprintf(ctx->WarningText,"i1_free: memory error: pointer is null");
		return;
        /*fprintf(stderr, "ERROR: i1_free: memory error: pointer is null");
        exit(-1);*/
    }

    /* Free the memory for the INTEGER 1D array */
    free(array-ABC);
}

/**********************************************************************/
/***              allocate memory for INTEGER 2D array           *******/
/**********************************************************************/
INTEGER** i2_malloc(INTEGER size1, INTEGER size2, INTEGER init, TLContext *ctx)
{

    /* Allocate INTEGER precision array with lengths "size1" and     */
    /* size2 assuming zero origin.  If initialize is non zero       */
    /* then initize the allocated array to zero.                    */

    INTEGER** array;
    INTEGER* arraytemp;
    INTEGER i,j;

    array = (INTEGER**) malloc((size_t) size1*sizeof(INTEGER*));
    if (!array) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"d2_malloc: memory error: malloc failed");
		return NULL;
        /*fprintf(stderr, "ERROR: d2_malloc: memory error: malloc failed");
        exit(-1);*/
    }
    for (i=0; i<size1; i++) {
        arraytemp = (INTEGER*) malloc((size_t) (size2+2*ABC)*sizeof(INTEGER));
        if (!arraytemp) {
            ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
            // OPSlvErrorInfo = ;
            sprintf(ctx->ErrorText,"d2_malloc: memory error: malloc failed");
	     	return NULL;
            /*fprintf(stderr, "ERROR: d2_malloc: memory error: malloc failed");
            exit(-1);*/
        }

#ifdef ARRAY_BORDER_CHECK
        /* setup array borders */
        if (i_borders_count > 2*ABC_NUM_1DARRAYS-4) {
            ctx->OPSlvErrorCode = OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL;
            // OPSlvErrorInfo = ;
            sprintf(ctx->ErrorText,"ARRAY_BORDERS_CHECK: ABC_NUM_1DARRAYS is too small");
			return NULL;
            /*printf("ERROR: ARRAY_BORDERS_CHECK: ABC_NUM_1DARRAYS is too small\n");
	        exit(-1);*/
        }
        arraytemp[0] = i_unique;
        i_borders[2*i_borders_count] = &(arraytemp[0]);
        arraytemp[size2+1] = i_unique;
        i_borders[2*i_borders_count+1] = &(arraytemp[size2+1]);
        i_borders_count++;
#endif
        array[i] = arraytemp + ABC;
    }

    if (init) {
       for (i=0; i<size1; i++)
           for (j=0; j<size2; j++)
               array[i][j] = 0.0;
    }

    return array;
}

/**********************************************************************/
/***                free memory for INTEGER 2D array             *******/
/**********************************************************************/
void i2_free(INTEGER** array, INTEGER size1, TLContext *ctx)
{
    INTEGER i;

    /* Check for null pointer */
    if (!array) {
        ctx->OPSlvWarningCode = OPSLV1_WARN_FREE_MEMORY;
        // OPSlvWarningInfo = ;
        sprintf(ctx->WarningText,"d2_free: memory error: pointer is null");
		return;
        /*fprintf(stderr, "ERROR: d2_free: memory error: pointer is null");
        exit(-1);*/
    }

    /* Free the memory for the INTEGER 2D array piece by piece */
    for (i=0; i<size1; i++)
        free(array[i]-ABC);
    free(array);
}


/**********************************************************************/
/***              allocate memory for double 1D array           *******/
/**********************************************************************/
DOUBLE* d1_malloc(INTEGER size1, INTEGER init, TLContext *ctx)
{

    /* Allocate DOUBLE precision array with length "size1"          */
    /* assuming zero origin.  If initialize is non zero             */
    /* then initize the allocated array to zero.                    */

    DOUBLE* array;
    INTEGER i,j;

    array = (DOUBLE*) malloc((size_t) (size1+2*ABC)*sizeof(DOUBLE));
    if (!array) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"d1_malloc: memory error: malloc failed");
		return NULL;
        /*fprintf(stderr, "ERROR: d1_malloc: memory error: malloc failed");
        exit(-1);*/
    }

#ifdef ARRAY_BORDER_CHECK
    /* setup array borders */
    if (d_borders_count > 2*ABC_NUM_1DARRAYS-4) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"ARRAYBORDERS: ABC_NUM_1DARRAYS is too small");
		return NULL;
        /*printf("ERROR: ARRAYBORDERS: ABC_NUM_1DARRAYS is too small\n");
	    exit(-1);*/
    }
    array[0] = d_unique;
    d_borders[2*d_borders_count] = &(array[0]);
    array[size1+1] = d_unique;
    d_borders[2*d_borders_count+1] = &(array[size1+1]);
    d_borders_count++;
    array++;
#endif

    if (init) {
        for (i=0; i<size1; i++)
            array[i] = 0.0;
    }

    return array;
}

/**********************************************************************/
/***                free memory for DOUBLE 1D array             *******/
/**********************************************************************/
void d1_free(DOUBLE* array, TLContext *ctx)
{
    INTEGER i;

    /* Check for null pointer */
    if (!array) {
        ctx->OPSlvWarningCode = OPSLV1_WARN_FREE_MEMORY;
        // OPSlvWarningInfo = ;
        sprintf(ctx->WarningText,"d1_free: memory error: pointer is null");
		return;
        /*fprintf(stderr, "ERROR: d1_free: memory error: pointer is null");
        exit(-1);*/
    }

    /* Free the memory for the DOUBLE 1D array */
    free(array-ABC);
}

/**********************************************************************/
/***              allocate memory for DOUBLE 2D array           *******/
/**********************************************************************/
DOUBLE** d2_malloc(INTEGER size1, INTEGER size2, INTEGER init, TLContext *ctx)
{

    /* Allocate DOUBLE precision array with lengths "size1" and     */
    /* size2 assuming zero origin.  If initialize is non zero       */
    /* then initize the allocated array to zero.                    */

    DOUBLE** array;
    DOUBLE* arraytemp;
    INTEGER i,j;

    array = (DOUBLE**) malloc((size_t) size1*sizeof(DOUBLE*));
    if (!array) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"d2_malloc: memory error: malloc failed");
		return NULL;
        /*fprintf(stderr, "ERROR: d2_malloc: memory error: malloc failed");
        exit(-1);*/
    }
    for (i=0; i<size1; i++) {

        /* Allocate memory */
        arraytemp = (DOUBLE*) malloc((size_t) (size2+2*ABC)*sizeof(DOUBLE));
        if (!arraytemp) {
            ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
            // OPSlvErrorInfo = ;
            sprintf(ctx->ErrorText,"d2_malloc: memory error: malloc failed");
		    return NULL;
            /*fprintf(stderr, "ERROR: d2_malloc: memory error: malloc failed");
            exit(-1);*/
        }

#ifdef ARRAY_BORDER_CHECK
        /* setup array borders */
        if (d_borders_count > 2*ABC_NUM_1DARRAYS-4) {
            ctx->OPSlvErrorCode = OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL;
            // OPSlvErrorInfo = ;
            sprintf(ctx->ErrorText,"ARRAY_BORDERS_CHECK: ABC_NUM_1DARRAYS is too small");
 		    return NULL;
            /*printf("ERROR: ARRAY_BORDERS_CHECK: ABC_NUM_1DARRAYS is too small\n");
	        exit(-1);*/
        }
        arraytemp[0] = d_unique;
        d_borders[2*d_borders_count] = &(arraytemp[0]);
        arraytemp[size2+1] = d_unique;
        d_borders[2*d_borders_count+1] = &(arraytemp[size2+1]);
        d_borders_count++;
#endif
        array[i] = arraytemp + ABC;
    }

    if (init) {
       for (i=0; i<size1; i++)
           for (j=0; j<size2; j++)
               array[i][j] = 0.0;
    }

    return array;
}

/**********************************************************************/
/***                free memory for DOUBLE 2D array             *******/
/**********************************************************************/
void d2_free(DOUBLE** array, INTEGER size1, TLContext *ctx)
{
    INTEGER i;

    /* Check for null pointer */
    if (!array) {
        ctx->OPSlvWarningCode = OPSLV1_WARN_FREE_MEMORY;
        // OPSlvWarningInfo = ;
        sprintf(ctx->WarningText,"d2_free: memory error: pointer is null");
		return;
        /*fprintf(stderr, "ERROR: d2_free: memory error: pointer is null");
        exit(-1);*/
    }

    /* Free the memory for the DOUBLE 2D array piece by piece */
    for (i=0; i<size1; i++)
        free(array[i]-ABC);
    free(array);
}

/**********************************************************************/
/***              allocate memory for LOGICAL 1D array           *******/
/**********************************************************************/
LOGICAL* l1_malloc(INTEGER size1, INTEGER init, TLContext *ctx)
{

    /* Allocate LOGICAL precision array with length "size1"          */
    /* assuming zero origin.  If initialize is non zero             */
    /* then initize the allocated array to zero.                    */

    LOGICAL* array;
    INTEGER i,j;

    array = (LOGICAL*) malloc((size_t) (size1+2*ABC)*sizeof(LOGICAL));
    if (!array) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"l1_malloc: memory error: malloc failed");
		return NULL;
        /*fprintf(stderr, "ERROR: l1_malloc: memory error: malloc failed");
        exit(-1);*/
    }

#ifdef ARRAY_BORDER_CHECK
    /* setup array borders */
    if (i_borders_count > 2*ABC_NUM_1DARRAYS-4) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"ARRAYBORDERS: ABC_NUM_1DARRAYS is too small");
 		return NULL;
        /*printf("ERROR: ARRAYBORDERS: ABC_NUM_1DARRAYS is too small\n");
	    exit(-1);*/
    }
    array[0] = i_unique;
    i_borders[2*i_borders_count] = &(array[0]);
    array[size1+1] = i_unique;
    i_borders[2*i_borders_count+1] = &(array[size1+1]);
    i_borders_count++;
    array++;
#endif

    if (init) {
        for (i=0; i<size1; i++)
            array[i] = 0.0;
    }

    return array;
}

/**********************************************************************/
/***                free memory for LOGICAL 1D array             *******/
/**********************************************************************/
void l1_free(LOGICAL* array, TLContext *ctx)
{
    INTEGER i;

    /* Check for null pointer */
    if (!array) {
        ctx->OPSlvWarningCode = OPSLV1_WARN_FREE_MEMORY;
        // OPSlvWarningInfo = ;
        sprintf(ctx->WarningText,"l1_free: memory error: pointer is null");
		return;
        /*fprintf(stderr, "ERROR: l1_free: memory error: pointer is null");
        exit(-1);*/
    }

    /* Free the memory for the LOGICAL 1D array */
    free(array-ABC);
}

/**********************************************************************/
/***              allocate memory for LOGICAL 2D array           *******/
/**********************************************************************/
LOGICAL** l2_malloc(INTEGER size1, INTEGER size2, INTEGER init, TLContext *ctx)
{

    /* Allocate LOGICAL precision array with lengths "size1" and     */
    /* size2 assuming zero origin.  If initialize is non zero       */
    /* then initize the allocated array to zero.                    */

    LOGICAL** array;
    LOGICAL* arraytemp;
    INTEGER i,j;

    array = (LOGICAL**) malloc((size_t) size1*sizeof(LOGICAL*));
    if (!array) {
        ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
        // OPSlvErrorInfo = ;
        sprintf(ctx->ErrorText,"l2_malloc: memory error: malloc failed");
		return NULL;
        /*fprintf(stderr, "ERROR: l2_malloc: memory error: malloc failed");
        exit(-1);*/
    }
    for (i=0; i<size1; i++) {
        arraytemp = (LOGICAL*) malloc((size_t) (size2+2*ABC)*sizeof(LOGICAL));
        if (!arraytemp) {
            ctx->OPSlvErrorCode = OPSLV1_ERR_NO_MEMORY;
            // OPSlvErrorInfo = ;
            sprintf(ctx->ErrorText,"l2_malloc: memory error: malloc failed");
		    return NULL;
            /*fprintf(stderr, "ERROR: l2_malloc: memory error: malloc failed");
            exit(-1);*/
        }

#ifdef ARRAY_BORDER_CHECK
        /* setup array borders */
        if (i_borders_count > 2*ABC_NUM_1DARRAYS-4) {
            ctx->OPSlvErrorCode = OPSLV1_ERR_ABC_NUM_1DARRAYS_SMALL;
            // OPSlvErrorInfo = ;
            sprintf(ctx->ErrorText,"ARRAY_BORDERS_CHECK: ABC_NUM_1DARRAYS is too small");
 		    return NULL;
            /*printf("ERROR: ARRAY_BORDERS_CHECK: ABC_NUM_1DARRAYS is too small\n");
	        exit(-1);*/
        }
        arraytemp[0] = i_unique;
        i_borders[2*i_borders_count] = &(arraytemp[0]);
        arraytemp[size2+1] = i_unique;
        i_borders[2*i_borders_count+1] = &(arraytemp[size2+1]);
        i_borders_count++;
#endif
        array[i] = arraytemp + ABC;
    }

    if (init) {
       for (i=0; i<size1; i++)
           for (j=0; j<size2; j++)
               array[i][j] = 0.0;
    }

    return array;
}

/**********************************************************************/
/***                free memory for LOGICAL 2D array             *******/
/**********************************************************************/
void l2_free(LOGICAL** array, INTEGER size1, TLContext *ctx)
{
    INTEGER i;

    /* Check for null pointer */
    if (!array) {
        ctx->OPSlvWarningCode = OPSLV1_WARN_FREE_MEMORY;
        // OPSlvWarningInfo = ;
        sprintf(ctx->WarningText,"l2_free: memory error: pointer is null");
		return;
        /*fprintf(stderr, "ERROR: l2_free: memory error: pointer is null");
        exit(-1);*/
    }

    /* Free the memory for the LOGICAL 2D array piece by piece */
    for (i=0; i<size1; i++)
        free(array[i]-ABC);
    free(array);
}


/**********************************************************************/
/***              allocate memory for global arrays            *******/
/**********************************************************************/
void global_mem_malloc(TLContext *ctx) {

    /* o8comm.h */
    ctx->accinf     = d2_malloc(ctx->iterma+1, 33, 1, ctx); ON_ERR;
    ctx->x          = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->x0         = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->x1         = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->xmin       = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->d          = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->d0         = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->dd         = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->difx       = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->resmin     = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin+1), 1, ctx); ON_ERR;
    ctx->gradf      = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->qgf        = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->gphi0      = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->gphi1      = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->gres       = d2_malloc(ctx->n+1, ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->gresn      = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->perm       = i1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->perm1      = i1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->colno      = i1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->qr         = d2_malloc(ctx->n+1, ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->betaq      = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->diag       = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->cscal      = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->colle      = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->a          = d2_malloc(ctx->n+1, ctx->n+1, 1, ctx); ON_ERR;
    ctx->diag0      = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->violis     = i1_malloc(2*ctx->nstep*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->alist      = i1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->o8bind     = i1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->o8bind0    = i1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->aalist     = i1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->clist      = i1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->u          = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->u0         = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->w          = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->w1         = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->res        = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->res0       = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->res1       = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->resst      = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->yu         = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->slack      = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->work       = d1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->ug         = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->og         = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->low        = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->up         = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->xst        = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    /* o8fint.h */
    ctx->xtr        = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->xsc        = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->fu         = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->fugrad     = d2_malloc(ctx->n+1, ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->fud        = d2_malloc(ctx->nlin+ctx->nonlin+1, 7, 1, ctx); ON_ERR;
    /* o8fuco.h */
    ctx->val        = l1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->llow       = l1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->lup        = l1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->cres       = i1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->cgres      = i1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->confuerr   = l1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->nonlinlist = i1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->gunit      = i2_malloc(4, ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->gconst     = l1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->cfuerr     = l1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    /* o8gene.h */
    ctx->np         = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->xj         = d2_malloc(ctx->ndualm+1, ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->ddual      = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->r          = d2_malloc(ctx->ndualm+1, ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->ud         = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->ud1        = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->aitr       = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    /* GLOBAL VARIABLES USED LOCALLY */
    ctx->donlp2_yy     = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8bfgs_dg     = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8bfgs_adx    = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8bfgs_ltdx   = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
/* printf("d_borders_count: %d\n", d_borders_count); */
    ctx->o8bfgs_gtdx   = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->o8bfgs_updx   = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8bfgs_updz   = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8opti_qtx    = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8opti_yy     = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8opti_yx     = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8opti_trvec  = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8opti_con    = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->o8opti_delist = i1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->o8opti_bindba = i1_malloc(2*(ctx->n+ctx->nlin+ctx->nonlin)+1, 1, ctx); ON_ERR;
    ctx->o8eval_con    = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->o8unim_step   = d1_malloc(ctx->nstep+1, 1, ctx); ON_ERR;
    ctx->o8dec_qri     = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8dec_qri0    = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8elim_qri    = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8elim_y      = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8elim_rhsscal= d1_malloc(ctx->n+ctx->nlin+1, 1, ctx); ON_ERR;
    ctx->o8elim_col    = i1_malloc(ctx->n+ctx->nlin+1, 1, ctx); ON_ERR;
    ctx->o8sol_xl      = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8upd_sdiag   = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8upd_rn1     = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8upd_w       = d1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_g0     = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_ci0    = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_cii    = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_cei    = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_xd     = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_s      = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_z      = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_vr     = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_xdold  = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_udold  = d1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_ai     = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_iai    = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_iaexcl = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_aiold  = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_eqlist = i1_malloc(ctx->n+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_iqlist = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_y      = d1_malloc(ctx->ndualm+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_mult   = d1_malloc(ctx->n+ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->o8qpdu_qpdel  = i1_malloc(ctx->mdualm+1, 1, ctx); ON_ERR;
    ctx->escongrad_errloc = i1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->escongrad_fhelp1 = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->escongrad_fhelp2 = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->escongrad_fhelp3 = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->escongrad_fhelp4 = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->escongrad_fhelp5 = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
    ctx->escongrad_fhelp6 = d1_malloc(ctx->nlin+ctx->nonlin+1, 1, ctx); ON_ERR;
}

/**********************************************************************/
/***                free memory from global arrays              *******/
/**********************************************************************/
void global_mem_free(TLContext *ctx) {

    /* GLOBAL VARIABLES USED GLOBALLY */

    /* o8comm.h */
    d2_free(ctx->accinf, ctx->iterma+1, ctx);
    d1_free(ctx->x, ctx);
    d1_free(ctx->x0, ctx);
    d1_free(ctx->x1, ctx);
    d1_free(ctx->xmin, ctx);
    d1_free(ctx->d, ctx);
    d1_free(ctx->d0, ctx);
    d1_free(ctx->dd, ctx);
    d1_free(ctx->difx, ctx);
    d1_free(ctx->resmin, ctx);
    d1_free(ctx->gradf, ctx);
    d1_free(ctx->qgf, ctx);
    d1_free(ctx->gphi0, ctx);
    d1_free(ctx->gphi1, ctx);
    d2_free(ctx->gres, ctx->n+1, ctx);
    d1_free(ctx->gresn, ctx);
    i1_free(ctx->perm, ctx);
    i1_free(ctx->perm1, ctx);
    i1_free(ctx->colno, ctx);
    d2_free(ctx->qr, ctx->n+1, ctx);
    d1_free(ctx->betaq, ctx);
    d1_free(ctx->diag, ctx);
    d1_free(ctx->cscal, ctx);
    d1_free(ctx->colle, ctx);

    d2_free(ctx->a, ctx->n+1, ctx);
    d1_free(ctx->diag0, ctx);
    i1_free(ctx->violis, ctx);
    i1_free(ctx->alist, ctx);
    i1_free(ctx->o8bind, ctx);
    i1_free(ctx->o8bind0, ctx);
    i1_free(ctx->aalist, ctx);
    i1_free(ctx->clist, ctx);
    d1_free(ctx->u, ctx);
    d1_free(ctx->u0, ctx);
    d1_free(ctx->w, ctx);
    d1_free(ctx->w1, ctx);
    d1_free(ctx->res, ctx);
    d1_free(ctx->res0, ctx);
    d1_free(ctx->res1, ctx);
    d1_free(ctx->resst, ctx);
    d1_free(ctx->yu, ctx);
    d1_free(ctx->slack, ctx);
    d1_free(ctx->work, ctx);
    d1_free(ctx->ug, ctx);
    d1_free(ctx->og, ctx);
    d1_free(ctx->low, ctx);
    d1_free(ctx->up, ctx);
    d1_free(ctx->xst, ctx);


    /* o8fint.h */
    d1_free(ctx->xtr, ctx);
    d1_free(ctx->xsc, ctx);
    d1_free(ctx->fu, ctx);
    d2_free(ctx->fugrad, ctx->n+1, ctx);
    d2_free(ctx->fud, ctx->nlin+ ctx->nonlin+1, ctx);

    /* o8fuco.h */
    l1_free(ctx->val, ctx);
    l1_free(ctx->llow, ctx);
    l1_free(ctx->lup, ctx);
    i1_free(ctx->cres, ctx);
    i1_free(ctx->cgres, ctx);
    l1_free(ctx->confuerr, ctx);
    i1_free(ctx->nonlinlist, ctx);
    i2_free(ctx->gunit, 4, ctx);
    l1_free(ctx->gconst, ctx);
    l1_free(ctx->cfuerr, ctx);

    /* o8gene.h */
    d1_free(ctx->np, ctx);
    d2_free(ctx->xj, ctx->ndualm+1, ctx);
    d1_free(ctx->ddual, ctx);
    d2_free(ctx->r, ctx->ndualm+1, ctx);
    d1_free(ctx->ud, ctx);
    d1_free(ctx->ud1, ctx);
    i1_free(ctx->aitr, ctx);

    /* GLOBAL VARIABLES USED LOCALLY */
    d1_free(ctx->donlp2_yy, ctx);
    d1_free(ctx->o8bfgs_dg, ctx);
    d1_free(ctx->o8bfgs_adx, ctx);
    d1_free(ctx->o8bfgs_ltdx, ctx);
    d1_free(ctx->o8bfgs_gtdx, ctx);
    d1_free(ctx->o8bfgs_updx, ctx);
    d1_free(ctx->o8bfgs_updz, ctx);
    d1_free(ctx->o8opti_qtx, ctx);
    d1_free(ctx->o8opti_yy, ctx);
    d1_free(ctx->o8opti_yx, ctx);
    d1_free(ctx->o8opti_trvec, ctx);
    d1_free(ctx->o8opti_con, ctx);
    i1_free(ctx->o8opti_delist, ctx);
    i1_free(ctx->o8opti_bindba, ctx);
    d1_free(ctx->o8eval_con, ctx);
    d1_free(ctx->o8unim_step, ctx);
    d1_free(ctx->o8dec_qri, ctx);
    d1_free(ctx->o8dec_qri0, ctx);
    d1_free(ctx->o8elim_qri, ctx);
    d1_free(ctx->o8elim_y, ctx);
    d1_free(ctx->o8elim_rhsscal, ctx);
    i1_free(ctx->o8elim_col, ctx);
    d1_free(ctx->o8sol_xl, ctx);
    d1_free(ctx->o8upd_sdiag, ctx);
    d1_free(ctx->o8upd_rn1, ctx);
    d1_free(ctx->o8upd_w, ctx);
    d1_free(ctx->o8qpdu_g0, ctx);
    d1_free(ctx->o8qpdu_ci0, ctx);
    d1_free(ctx->o8qpdu_cii, ctx);
    d1_free(ctx->o8qpdu_cei, ctx);
    d1_free(ctx->o8qpdu_xd, ctx);
    d1_free(ctx->o8qpdu_s, ctx);
    d1_free(ctx->o8qpdu_z, ctx);
    d1_free(ctx->o8qpdu_vr, ctx);
    d1_free(ctx->o8qpdu_xdold, ctx);
    d1_free(ctx->o8qpdu_udold, ctx);
    i1_free(ctx->o8qpdu_ai, ctx);
    i1_free(ctx->o8qpdu_iai, ctx);
    i1_free(ctx->o8qpdu_iaexcl, ctx);
    i1_free(ctx->o8qpdu_aiold, ctx);
    i1_free(ctx->o8qpdu_eqlist, ctx);
    i1_free(ctx->o8qpdu_iqlist, ctx);
    d1_free(ctx->o8qpdu_y, ctx);
    d1_free(ctx->o8qpdu_mult, ctx);
    i1_free(ctx->o8qpdu_qpdel, ctx);
    i1_free(ctx->escongrad_errloc, ctx);
    d1_free(ctx->escongrad_fhelp1, ctx);
    d1_free(ctx->escongrad_fhelp2, ctx);
    d1_free(ctx->escongrad_fhelp3, ctx);
    d1_free(ctx->escongrad_fhelp4, ctx);
    d1_free(ctx->escongrad_fhelp5, ctx);
    d1_free(ctx->escongrad_fhelp6, ctx);
}
