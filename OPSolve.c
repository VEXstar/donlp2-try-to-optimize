/* **************************************************************************** */
/*                                 user functions                               */
/* **************************************************************************** */
#include "o8comm.h"

const INTEGER OPSLV1_FLG_GRAD_PRESENT    = 0x00000001;
const INTEGER OPSLV1_FLG_LOG             = 0x00000002;
const INTEGER OPSLV1_FLG_LOG_INTAKT      = 0x00000004;
const INTEGER OPSLV1_FLG_LOG_STEP        = 0x00000008;
const INTEGER OPSLV1_FLG_LOG_POST_MORTEM = 0x00000010;
const INTEGER OPSLV1_FLG_LOG_DETAILED    = 0x00000020;
const INTEGER OPSLV1_FLG_LOG_HESS_GRAD   = 0x00000040;


static const TOPSLV1_INITDATA StartData = {
  "",     /* name */
  0,      /* n */
  0,      /* nlin */
  0,      /* nonlin */
  4000,   /* itermax */
  20,     /* nstep */
  0,      /* flags */
  1.e-16, /* epsdif */
  1.e-16, /* epsfcn */
  1.0,    /* taubnd */
  1,      /* difftype */
  0,      /* nreset */
  1.0e0,  /* del0 */
  1.0e-2, /* tau0 */
  0.1e0,  /* tau */
  1.e20,  /* big */
  NULL,   /* x */
  NULL,   /* low */
  NULL,   /* up */
  NULL    /* gres */
};

LOGICAL __stdcall OPSLV_1(void *pContext, POPSLV1_PROBLEM pProblem, 
  ERRCODE *pErrorCode, ERRINFO *pErrorInfo, char *ErrMsg,
  WARNCODE *pWarningCode, WARNINFO *pWarningInfo, char *WarnMsg,
  void *pReserved)
{
  #define Error(ErrorCode,ErrorInfo,ErrStr) { if (pErrorCode) *pErrorCode = ErrorCode; if (pErrorInfo) *pErrorInfo = ErrorInfo; if (ErrMsg) strcpy(ErrMsg,ErrStr); return FALSE; }
  int i;

  if (pErrorCode) *pErrorCode = 0;
  if (pErrorInfo) *pErrorInfo = 0;
  if (ErrMsg) strcpy(ErrMsg,"");
  if (pWarningCode) *pWarningCode = 0;
  if (pWarningInfo) *pWarningInfo = 0;
  if (WarnMsg) strcpy(WarnMsg,"");
  
  if (!pProblem)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_PPROBLEM,"pProblem == NULL");
  if (!pProblem->Init)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_INIT,"pProblem->Init == NULL");
  if (!pProblem->Func)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_FUNC,"pProblem->Func == NULL");

  TLContext ctx_;
  memset(&ctx_, 0, sizeof(ctx_));
  TLContext *ctx = &ctx_;

  ctx->Context = pContext;
  ctx->InitData = StartData;
  ctx->Problem = *pProblem;
  pProblem->Init(pContext,&ctx->InitData,NULL);

  if (ctx->InitData.n <= 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_DIMENSION,"InitData.n <= 0");
  if (ctx->InitData.nlin < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NLIN,"InitData.nlin < 0");
  if (ctx->InitData.nonlin < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NONLIN,"InitData.nonlin < 0");
  if (ctx->InitData.maxiter <= 1)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_MAXITER,"InitData.maxiter <= 1");
  if (ctx->InitData.nstep <= 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NSTEP,"InitData.nstep <= 0");

  if (!pProblem->GradFunc && ((ctx->InitData.Flags & OPSLV1_FLG_GRAD_PRESENT) == 0))
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRADFUNC,"Flag OPSLV1_FLG_GRAD_PRESENT is set in InitData.Flags, but pProblem->GradFunc == NULL");
  if (!pProblem->GradConstrFunc && ((ctx->InitData.Flags & OPSLV1_FLG_GRAD_PRESENT) == 0))
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRADCONSTRFUNC,"Flag OPSLV1_FLG_GRAD_PRESENT is set in InitData.Flags, but pProblem->GradConstrFunc == NULL");
  if (ctx->InitData.epsdif <= 0.0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_EPSDIF,"InitData.epsdif <= 0");
  if (ctx->InitData.epsfcn <= 0.0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_EPSFCN,"InitData.epsfcn <= 0");
  if (ctx->InitData.taubnd <= 0.0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_TAUBND,"InitData.taubnd <= 0");
  if (ctx->InitData.difftype < 1 || ctx->InitData.difftype > 3)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_DIFFTYPE,"InitData.difftype must be between 1 and 3");
  /*if (!InitData.nreset)
    InitData.nreset = InitData.n;*/
  if (ctx->InitData.nreset < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NRESET,"InitData.nreset < 0");
  if (ctx->InitData.del0 < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_DEL0,"InitData.del0 < 0");
  if (ctx->InitData.tau0 < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_TAU0,"InitData.tau0 < 0");
  if (ctx->InitData.tau < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_TAU,"InitData.tau < 0");
  if (ctx->InitData.big <= 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_BIG,"InitData.big <= 0");
  if (!ctx->InitData.x)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_X,"InitData.x == NULL");
  if (!ctx->InitData.low && ctx->InitData.nlin > 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_LOW,"InitData.nlin > 0 but InitData.low == NULL");
  if (!ctx->InitData.up && ctx->InitData.nlin > 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_UP,"InitData.nlin > 0 but InitData.up == NULL");
  if (!ctx->InitData.gres && ctx->InitData.nlin > 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRES,"InitData.nlin > 0 but InitData.gres == NULL");
  for (i = 0; i < ctx->InitData.nlin/* + InitData.nonlin*/; i++)
    if (!ctx->InitData.gres[i])
      Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRES_INSUFF,"Not enough InitData.gres items allocated");
  //pInitContext = &InitContext;
  
  ctx->nIter = 0;
  ctx->nFuncEval = 0;
  ctx->nGradFuncEval = 0;
  ctx->nConstrEval = 0;
  ctx->nConstrTotalEval = 0;
  ctx->nGradConstrEval = 0;
  ctx->nGradConstrTotalEval = 0;
  donlp2(ctx);
  if (ctx->OPSlvWarningCode)
  {
	  if (pWarningCode) *pWarningCode = ctx->OPSlvWarningCode;
	  if (pWarningInfo) *pWarningInfo = ctx->OPSlvWarningInfo;
	  if (WarnMsg) strcpy(WarnMsg,ctx->WarningText);
  }
  if (ctx->OPSlvErrorCode) 
  {
	  Error(ctx->OPSlvErrorCode,ctx->OPSlvErrorInfo,ctx->ErrorText);
  }
  return TRUE;
  #undef Error
}


LOGICAL __stdcall OPSLV_1_MULTI_INIT(void *pContext, POPSLV1_PROBLEM pProblem, 
  ERRCODE *pErrorCode, ERRINFO *pErrorInfo, char *ErrMsg,
  WARNCODE *pWarningCode, WARNINFO *pWarningInfo, char *WarnMsg,
  void *pReserved)
{  
  #define Error(ErrorCode,ErrorInfo,ErrStr) { if (pErrorCode) *pErrorCode = ErrorCode; if (pErrorInfo) *pErrorInfo = ErrorInfo; if (ErrMsg) strcpy(ErrMsg,ErrStr); return FALSE; }
  int i;

  // TODO!!!! MAKE THIS RIGHT
  TLContext ctx_;
  memset(&ctx_, 0, sizeof(ctx_));
  TLContext *ctx = &ctx_;

  if (ctx->pOPSLV_1_MULTI_CONTEXT)
	  Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_CONTEXT_NONEMPTY,"OPSLV_1_MULTI_INIT: batch mode already initialized");

  if (!pContext)
	  Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_CONTEXT_EMPTY,"OPSLV_1_MULTI_INIT: pContext == NULL");
  ctx->pOPSLV_1_MULTI_CONTEXT = pContext;

  if (pErrorCode) *pErrorCode = 0;
  if (pErrorInfo) *pErrorInfo = 0;
  if (ErrMsg) strcpy(ErrMsg,"");
  if (pWarningCode) *pWarningCode = 0;
  if (pWarningInfo) *pWarningInfo = 0;
  if (WarnMsg) strcpy(WarnMsg,"");
  
  if (!pProblem)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_PPROBLEM,"pProblem == NULL");
  if (!pProblem->Init)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_INIT,"pProblem->Init == NULL");
  if (!pProblem->Func)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_FUNC,"pProblem->Func == NULL");
  if (!pProblem->InitAgain)
    Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_INITAGAIN,"pProblem->InitAgain == NULL");  

  ctx->Context = pContext;
  ctx->InitData = StartData;
  ctx->Problem = *pProblem;
  pProblem->Init(pContext,&ctx->InitData,NULL);

  if (ctx->InitData.n <= 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_DIMENSION,"InitData.n <= 0");
  if (ctx->InitData.nlin < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NLIN,"InitData.nlin < 0");
  if (ctx->InitData.nonlin < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NONLIN,"InitData.nonlin < 0");
  if (ctx->InitData.maxiter <= 1)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_MAXITER,"InitData.maxiter <= 1");
  if (ctx->InitData.nstep <= 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NSTEP,"InitData.nstep <= 0");

  if (!pProblem->GradFunc && ((ctx->InitData.Flags & OPSLV1_FLG_GRAD_PRESENT) == 0))
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRADFUNC,"Flag OPSLV1_FLG_GRAD_PRESENT is set in InitData.Flags, but pProblem->GradFunc == NULL");
  if (!pProblem->GradConstrFunc && ((ctx->InitData.Flags & OPSLV1_FLG_GRAD_PRESENT) == 0))
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRADCONSTRFUNC,"Flag OPSLV1_FLG_GRAD_PRESENT is set in InitData.Flags, but pProblem->GradConstrFunc == NULL");
  if (ctx->InitData.epsdif <= 0.0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_EPSDIF,"InitData.epsdif <= 0");
  if (ctx->InitData.epsfcn <= 0.0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_EPSFCN,"InitData.epsfcn <= 0");
  if (ctx->InitData.taubnd <= 0.0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_TAUBND,"InitData.taubnd <= 0");
  if (ctx->InitData.difftype < 1 || ctx->InitData.difftype > 3)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_DIFFTYPE,"InitData.difftype must be between 1 and 3");
  /*if (!InitData.nreset)
    InitData.nreset = InitData.n;*/
  if (ctx->InitData.nreset < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_NRESET,"InitData.nreset < 0");
  if (ctx->InitData.del0 < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_DEL0,"InitData.del0 < 0");
  if (ctx->InitData.tau0 < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_TAU0,"InitData.tau0 < 0");
  if (ctx->InitData.tau < 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_TAU,"InitData.tau < 0");
  if (ctx->InitData.big <= 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_BIG,"InitData.big <= 0");
  if (!ctx->InitData.x)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_X,"InitData.x == NULL");
  if (!ctx->InitData.low && ctx->InitData.nlin > 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_LOW,"InitData.nlin > 0 but InitData.low == NULL");
  if (!ctx->InitData.up && ctx->InitData.nlin > 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_UP,"InitData.nlin > 0 but InitData.up == NULL");
  if (!ctx->InitData.gres && ctx->InitData.nlin > 0)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRES,"InitData.nlin > 0 but InitData.gres == NULL");
  for (i = 0; i < ctx->InitData.nlin/* + InitData.nonlin*/; i++)
    if (!ctx->InitData.gres[i])
      Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_GRES_INSUFF,"Not enough InitData.gres items allocated");
  //pInitContext = &InitContext;
  ctx->nIter = 0;
  ctx->nFuncEval = 0;
  ctx->nGradFuncEval = 0;
  ctx->nConstrEval = 0;
  ctx->nConstrTotalEval = 0;
  ctx->nGradConstrEval = 0;
  ctx->nGradConstrTotalEval = 0;
  donlp2_multi_1_init(ctx);
  if (ctx->OPSlvWarningCode)
  {
	  if (pWarningCode) *pWarningCode = ctx->OPSlvWarningCode;
	  if (pWarningInfo) *pWarningInfo = ctx->OPSlvWarningInfo;
	  if (WarnMsg) strcpy(WarnMsg,ctx->WarningText);
  }
  if (ctx->OPSlvErrorCode) 
  {
	  Error(ctx->OPSlvErrorCode,ctx->OPSlvErrorInfo,ctx->ErrorText);
  }
  return TRUE;
  #undef Error
}

LOGICAL __stdcall OPSLV_1_MULTI_GO(void *pContext, POPSLV1_PROBLEM pProblem, 
  ERRCODE *pErrorCode, ERRINFO *pErrorInfo, char *ErrMsg,
  WARNCODE *pWarningCode, WARNINFO *pWarningInfo, char *WarnMsg,
  void *pReserved)
{
  #define Error(ErrorCode,ErrorInfo,ErrStr) { if (pErrorCode) *pErrorCode = ErrorCode; if (pErrorInfo) *pErrorInfo = ErrorInfo; if (ErrMsg) strcpy(ErrMsg,ErrStr); return FALSE; }

	// TODO!!!! MAKE THIS RIGHT
	TLContext ctx_;
	memset(&ctx_, 0, sizeof(ctx_));
	TLContext *ctx = &ctx_;

  if (!ctx->pOPSLV_1_MULTI_CONTEXT)
	  Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_CONTEXT_EMPTY,"OPSLV_1_MULTI_GO: batch mode not initialized");

  if (pContext!=ctx->pOPSLV_1_MULTI_CONTEXT)
	  Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_CONTEXT_NOTEQ,"OPSLV_1_MULTI_GO: pContext must be the same as in OPSLV_1_MULTI_INIT call");
  
  if (pErrorCode) *pErrorCode = 0;
  if (pErrorInfo) *pErrorInfo = 0;
  if (ErrMsg) strcpy(ErrMsg,"");
  if (pWarningCode) *pWarningCode = 0;
  if (pWarningInfo) *pWarningInfo = 0;
  if (WarnMsg) strcpy(WarnMsg,"");
  
  if (!pProblem)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_PPROBLEM,"pProblem == NULL");
  if (!pProblem->Init)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_INIT,"pProblem->Init == NULL");
  if (!pProblem->Func)
    Error(OPSLV1_ERR_INVALID_PARAM,OPSLV1_INV_PAR_FUNC,"pProblem->Func == NULL");
  if (!pProblem->InitAgain)
    Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_INITAGAIN,"pProblem->InitAgain == NULL");

  ctx->Context = pContext;
  ctx->InitData = StartData;
  ctx->Problem = *pProblem;
  pProblem->InitAgain(pContext,&ctx->InitData,NULL); 

  user_init_again(ctx);
	
  ctx->nIter = 0;
  ctx->nFuncEval = 0;
  ctx->nGradFuncEval = 0;
  ctx->nConstrEval = 0;
  ctx->nConstrTotalEval = 0;
  ctx->nGradConstrEval = 0;
  ctx->nGradConstrTotalEval = 0;
  donlp2_multi_1_go(ctx);
  if (ctx->OPSlvWarningCode)
  {
	  if (pWarningCode) *pWarningCode = ctx->OPSlvWarningCode;
	  if (pWarningInfo) *pWarningInfo = ctx->OPSlvWarningInfo;
	  if (WarnMsg) strcpy(WarnMsg,ctx->WarningText);
  }
  if (ctx->OPSlvErrorCode) 
  {
	  Error(ctx->OPSlvErrorCode,ctx->OPSlvErrorInfo,ctx->ErrorText);
  }
  return TRUE;
  #undef Error
}

LOGICAL __stdcall OPSLV_1_MULTI_DONE(void *pContext, POPSLV1_PROBLEM pProblem, 
  ERRCODE *pErrorCode, ERRINFO *pErrorInfo, char *ErrMsg,
  WARNCODE *pWarningCode, WARNINFO *pWarningInfo, char *WarnMsg,
  void *pReserved)
{
  #define Error(ErrorCode,ErrorInfo,ErrStr) { if (pErrorCode) *pErrorCode = ErrorCode; if (pErrorInfo) *pErrorInfo = ErrorInfo; if (ErrMsg) strcpy(ErrMsg,ErrStr); return FALSE; }

	// TODO!!!! MAKE THIS RIGHT
	TLContext ctx_;
	memset(&ctx_, 0, sizeof(ctx_));
	TLContext *ctx = &ctx_;

  if (!ctx->pOPSLV_1_MULTI_CONTEXT)
	  Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_CONTEXT_EMPTY,"OPSLV_1_MULTI_DONE: batch mode not initialized");

  if (pContext!=ctx->pOPSLV_1_MULTI_CONTEXT)
	  Error(OPSLV1_MULTI_ERR_INVALID_PARAM,OPSLV1_MULTI_INV_PAR_CONTEXT_NOTEQ,"OPSLV_1_MULTI_DONE: pContext must be the same as in OPSLV_1_MULTI_INIT call");
  
  ctx->pOPSLV_1_MULTI_CONTEXT = NULL;

  donlp2_multi_1_done(ctx);
  if (ctx->OPSlvWarningCode)
  {
	  if (pWarningCode) *pWarningCode = ctx->OPSlvWarningCode;
	  if (pWarningInfo) *pWarningInfo = ctx->OPSlvWarningInfo;
	  if (WarnMsg) strcpy(WarnMsg,ctx->WarningText);
  }
  if (ctx->OPSlvErrorCode) 
  {
	  Error(ctx->OPSlvErrorCode,ctx->OPSlvErrorInfo,ctx->ErrorText);
  }
  return TRUE;
  #undef Error
}


/* **************************************************************************** */
/*                              donlp2-intv size initialization                           */
/* **************************************************************************** */
void user_init_size(TLContext *ctx) {
    /* problem dimension n = dim(x), nlin=number of linear constraints
       nonlin = number of nonlinear constraints  */

    ctx->n      = ctx->InitData.n;
	ctx->nlin   = ctx->InitData.nlin;
	ctx->nonlin = ctx->InitData.nonlin;
	ctx->iterma = ctx->InitData.maxiter;
	ctx->nstep  = ctx->InitData.nstep;
}

/* **************************************************************************** */
/*                              donlp2-intv standard setup                           */
/* **************************************************************************** */
void user_init(TLContext *ctx) {
  INTEGER  i,j;
  DOUBLE *srcrow, *dstrow;

  /* name is ident of the example/user and can be set at users will       */
  /* the first static character must be alphabetic. 40 characters maximum */
  strcpy(ctx->name,ctx->InitData.name);

  /* x is initial guess and also holds the current solution */

  if (ctx->InitData.Flags & OPSLV1_FLG_GRAD_PRESENT)
	  ctx->analyt = TRUE;
  else
	  ctx->analyt = FALSE;
  ctx->epsdif = ctx->InitData.epsdif;
  ctx->epsfcn = ctx->InitData.epsfcn;
  ctx->taubnd = ctx->InitData.taubnd;
  ctx->difftype = ctx->InitData.difftype;

  if (!ctx->InitData.nreset)
	  ctx->nreset = ctx->n;
  else
	  ctx->nreset = ctx->InitData.nreset;

  ctx->del0 = ctx->InitData.del0;
  ctx->tau0 = ctx->InitData.tau0;
  ctx->tau  = ctx->InitData.tau;
  for (i = 1; i <= ctx->n; i++) {
	  ctx->x[i] = ctx->InitData.x[i-1];
  }

  /*  set lower and upper bounds */
  ctx->big = ctx->InitData.big;
  if (ctx->InitData.low)
    for (i = 1; i <= ctx->n+ ctx->nlin+ ctx->nonlin; i++)
		ctx->low[i] = ctx->InitData.low[i-1];
  else
    for (i = 1; i <= ctx->n+ ctx->nlin+ ctx->nonlin; i++)
		ctx->low[i] = -ctx->big;

  if (ctx->InitData.up)
    for (i = 1; i <= ctx->n+ ctx->nlin+ ctx->nonlin; i++)
		ctx->up[i] = ctx->InitData.up[i-1];
  else
    for (i = 1; i <= ctx->n+ ctx->nlin+ ctx->nonlin; i++)
		ctx->up[i] = ctx->big;

  /*for (i = n+1; i <= n + nlin; i++)
  {
    low[i] = InitData.low[i-1];
    up[i] = InitData.up[i-1];
  }*/

  /* store coefficients of linear constraints directly in gres */

  for (i = 1; i <= ctx->n /*nlin + nonlin*/; i++)
  {
    srcrow = ctx->InitData.gres[i-1];
    dstrow = ctx->gres[i];
    for (j = 1; j <= ctx->nlin; j++)
      dstrow[j] = srcrow[j-1];
  }

  if (ctx->InitData.Flags & OPSLV1_FLG_LOG)
	  ctx->silent = FALSE;
  else
	  ctx->silent = TRUE;

  return;
}

void user_init_again(TLContext *ctx) {
  INTEGER  i,j;
  DOUBLE *srcrow, *dstrow;

  for (i = 1; i <= ctx->n; i++) {
    ctx->x[i] = ctx->InitData.x[i-1];
  }

  /*  set lower and upper bounds */
  if (ctx->InitData.low)
    for (i = 1; i <= ctx->n + ctx->nlin + ctx->nonlin; i++)
      ctx->low[i] = ctx->InitData.low[i-1];
  else
    for (i = 1; i <= ctx->n + ctx->nlin + ctx->nonlin; i++)
      ctx->low[i] = -ctx->big;

  if (ctx->InitData.up)
    for (i = 1; i <= ctx->n + ctx->nlin + ctx->nonlin; i++)
      ctx->up[i] = ctx->InitData.up[i-1];
  else
    for (i = 1; i <= ctx->n + ctx->nlin + ctx->nonlin; i++)
      ctx->up[i] = ctx->big;

  /* store coefficients of linear constraints directly in gres */

  for (i = 1; i <= ctx->n /*nlin + nonlin*/; i++)
  {
    srcrow = ctx->InitData.gres[i-1];
    dstrow = ctx->gres[i];
    for (j = 1; j <= ctx->nlin; j++)
      dstrow[j] = srcrow[j-1];
  }

  return;
}

/* **************************************************************************** */
/*                                 special setup                                */
/* **************************************************************************** */
void setup(TLContext *ctx) {
  if (ctx->InitData.Flags & OPSLV1_FLG_LOG_INTAKT)
	  ctx->intakt = TRUE;
  else
	  ctx->intakt = FALSE;

  if (ctx->InitData.Flags & OPSLV1_FLG_LOG_STEP)
	  ctx->te0 = TRUE;
  else
	  ctx->te0 = FALSE;

  if (ctx->InitData.Flags & OPSLV1_FLG_LOG_POST_MORTEM)
	  ctx->te1 = TRUE;
  else
	  ctx->te1 = FALSE;

  if (ctx->InitData.Flags & OPSLV1_FLG_LOG_DETAILED)
	  ctx->te2 = TRUE;
  else
	  ctx->te2 = FALSE;

  if (ctx->InitData.Flags & OPSLV1_FLG_LOG_HESS_GRAD)
	  ctx->te3 = TRUE;
  else
	  ctx->te3 = FALSE;

  return;
}

/* **************************************************************************** */
/*  the user may add additional computations using the computed solution here   */
/* **************************************************************************** */

void solchk(TLContext *ctx) {
  /*EndTickCount = GetTickCount();
  printf("x = %15.9e\n",x[1]);
  printf("y = %15.9e\n",x[2]);
  printf("z = %15.9e\n",x[3]);

  printf("%5.3g\nCallCount = %d\n",0.001*(float)(EndTickCount-StartTickCount),efCallCount);
  */
  int i;

  for (i = 0; i < ctx->n; i++)
    ctx->InitData.x[i] = ctx->x[i+1];

  if (ctx->Problem.pStatData)
  {
    memset(ctx->Problem.pStatData,0,sizeof(TOPSLV1_STATDATA));
    ctx->Problem.pStatData->nIter = ctx->nIter;
    ctx->Problem.pStatData->nFuncEval = ctx->nFuncEval;
    ctx->Problem.pStatData->nGradFuncEval = ctx->nGradFuncEval;
    ctx->Problem.pStatData->nConstrEval = ctx->nConstrEval;
    ctx->Problem.pStatData->nConstrTotalEval = ctx->nConstrTotalEval;
    ctx->Problem.pStatData->nGradConstrEval = ctx->nGradConstrEval;
    ctx->Problem.pStatData->nGradConstrTotalEval = ctx->nGradConstrTotalEval;
  }
  return;
}

/* **************************************************************************** */
/*                               objective function                             */
/* **************************************************************************** */
void ef(DOUBLE x[],DOUBLE *fx, TLContext *ctx) {    
    *fx = ctx->Problem.Func(ctx->Context, &x[1], NULL);
    ctx->nFuncEval++;
    return;
}

/* **************************************************************************** */
/*                          gradient of objective function                      */
/* **************************************************************************** */
void egradf(DOUBLE x[],DOUBLE gradf[], TLContext *ctx) {
  ctx->Problem.GradFunc(ctx->Context, &x[1], &gradf[1], NULL);
  ctx->nGradFuncEval++;
  return;
}

/* **************************************************************************** */
/*  no nonlinear constraints */
/* **************************************************************************** */
void econ(INTEGER type ,INTEGER liste[], DOUBLE x[], DOUBLE con[],
              LOGICAL err[], TLContext *ctx) {
  ctx->Problem.ConstrFunc(ctx->Context, type, liste, &x[1], &con[1], &err[1], NULL);
  ctx->nConstrEval++;
  if (type == 1)
    ctx->nConstrTotalEval += ctx->nonlin;
  else
    ctx->nConstrTotalEval += liste[0];
  return;
}

/* **************************************************************************** */
/* **************************************************************************** */
void econgrad(INTEGER liste[] ,INTEGER shift ,  DOUBLE x[],
               DOUBLE **grad, TLContext *ctx) {
  ctx->Problem.GradConstrFunc(ctx->Context, liste, shift, &x[1], grad, NULL);
  ctx->nGradConstrEval++;
  ctx->nGradConstrTotalEval += liste[0];
  return;
}

/* **************************************************************************** */
/*                        user functions (if bloc == TRUE)                      */
/* **************************************************************************** */
void eval_extern(INTEGER mode) {
    return;
}

/**********************************************************************/
/***    enable user inspection of iteration data                *******/
/**********************************************************************/
void newx ( DOUBLE x[] , DOUBLE u[] , INTEGER itstep , DOUBLE **accinf,
            LOGICAL *cont, TLContext *ctx)
{
/*   x = current solution                                                 */
/*   u = current multiplier estimates                                     */
/*   itstep = last step performed                                         */
/*   accinf : accumulated short information   defined from accinf[1][] to */
/*            accinf[itstep][]                                            */
/*   continue must be set TRUE . otherwise donlp2 will terminate          */
/*   irrespective of the state of the optimization                        */
/*   for description of accinf see below                                  */
/*  accinf : a c c u m u l a t e d   i n f o r m a t i o n                */
/*  on iteration sequence : components 0--32 for one step                 */
/*  0: not used                                                           */
/*  1: step-nr                                                            */
/*  2: f(x-k) current value of objective (zero in feasibility improvement */
/*            phase (-1)  )                                               */
/*  3: scf    internal scaling of objective (zero in phase -1)            */
/*  4: psi    the weighted penalty-term                                   */
/*  5: upsi   the unweighted penalty-term (l1-norm of constraint vector)  */
/*  6: del_k_1 bound for currently active constraints                     */
/*  7: b2n0   weigthed l2-norm of projected gradient, based on inequality */
/*            constraints <= delmin and equality constraints              */
/*  8: b2n    l2-norm of projected gradient based on del_k_1              */
/*  9: nr     number of binding constraints                               */
/*  10: sing  if 1, the binding constraints don't satisfy the regularity  */
/*            condition                                                   */
/*  11: umin   infinity norm of negative part of multiplier               */
/*  12: presently not used                                                */
/*  13: cond_r condition number of diagonal part of qr-decomposition      */
/*             of normalized gradients of binding constraints             */
/*  14: cond_h condition number of diagonal of cholesky-factor            */
/*             of updated full hessian                                    */
/*  15: scf0   the relative damping of tangential component if upsi>tau0/2*/
/*  16: xnorm  l2-norm of x                                               */
/*  17: dnorm  l2-norm of d (correction from qp -subproblem, unscaled)    */
/*  18: phase  -1:infeasibility improvement phase, 0:initial optimization */
/*            1: binding constraints unchanged ,                          */
/*            2: d small, maratos correction in use                       */
/*  19: c_k    number of decreases of penalty weights                     */
/*  20: wmax   infinity norm of current penalty weights                   */
/*  21: sig_k  stepsize from unidimensional minimization (backtracking)   */
/*  22: cfincr number of objective evaluations for stepsize-algorithm     */
/*  23: dirder directional derivative of penalty-function along d (scaled)*/
/*  24: dscal  scaling factor for d                                       */
/*  25: cosphi cos of arc between d and d_previous. if cosphi>=theta      */
/*             then  sig larger  than     one (up to sigla) is tried      */
/*  26: violis[0] number of constraints not binding at x but hit during   */
/*             line search                                                */
/*  27:        type of update for hessian:                                */
/*             1: normal p&m-bfgs-update,                                 */
/*             0: update suppress$                                        */
/*            -1 restart with scaled unit matrix                          */
/*             2 standard bfgs (unconstrained)                            */
/*             3 bfgs modified by powells device                          */
/*  28: ny_k resp. t_k  modification factor for damping the projector in  */
/*             the bfgs  resp. the pantoja-mayne update                   */
/*  29: 1-my_k/xsi_k modification factor for damping the                  */
/*             quasi-newton-condition in bfgs resp the regularization term*/
/*            for unmodified bfgs ny_k should be larger than updmy0       */
/*            (near one)                                                  */
/*            and 1-my_k equal one.                                       */
/*  30: qpterm 0, if sing=-1                                              */
/*                termination indicator of qp-solver otherwise            */
/*             1: successful,                                             */
/*             -1: tau becomes larger than tauqp without slack-           */
/*                 variables becoming sufficiently small .                */
/*             -2: infeasible qp-problem (theoretically impossible)       */
/*  31: tauqp: weight of slack-variables in qp-solver                     */
/*  32: infeas l1-norm of slack-variables in qp-solver                    */

  if (ctx->Problem.Step)
    *cont = ctx->Problem.Step(ctx->Context, &x[1], &u[1], itstep, accinf, NULL);
  else
    *cont = TRUE ;
  ctx->nIter++;
}
