#include "o8comm.h"

void user_eval(DOUBLE xvar[],INTEGER mode, TLContext *ctx) {
/* ************************************************************************ */
/* called only if bloc = TRUE                                               */
/* interface to a users evaluation code of functions                        */
/* which define the optimization problem                                    */
/* mode = -1 : evaluate function values at xvar only, store them in fu      */
/* mode = 1 : evaluate f , grad(f), con[], grad(con[]) and store            */
/*             them in  fu and fugrad                                       */
/* mode = 2  : evaluate gradients only at xvar and store them in fugrad     */
/* The users evaluation routine                                             */
/*             eval_extern(mode)                                            */
/* is called with two settings of its mode only:                            */
/* on one variable only (gunit[1][i] = 1).                                  */
/* mode = 1: set function value                                             */
/* fu using xtr, mode = 2 set function values fu and gradients fugrad using */
/* xtr                                                                      */
/* Since donlp2 uses internally a scaled value of x, the external value xtr */
/* is obtained by multiplying with xsc. In evaluating the gradients,        */
/* the user must not take care of this scaling. Scaling of the gradients    */
/* is done by donlp2 internally.                                            */
/* that means xtr and the function values returned below to the unscaled    */
/* original problem                                                         */
/* If the user has set analyt = TRUE, he is responsible for computing       */
/* the gradients himself correctly                                          */
/* otherwise, if analyt = FALSE, then this routine does numerical           */
/* differentiation according to the method defined by difftype:             */
/* difftype = 1   ordinary forward difference quotient, using               */
/*                min(0.1*sqrt(epsfcn),1.e-2) as stepsize                   */
/*                epsfcn is the assumed precision of function values        */
/*                and must be set by the user in setup0                     */
/* difftype = 2   central difference quotient with stepsize                 */
/*                min(0.1*pow(epsfcn,2/3),1.e-2) as stepsize                */
/* difftype = 3   6-th order Richardson extrapolation method using          */
/*                6 function values for each component of grad,             */
/*                with stepsize min(0.1*pow(epsfcn,1/7),0.01)               */
/* This is done by several calls of eval_extern                             */
/* xtr is the current design to be used by the external routine             */
/* Values must be returned in the arrays fu (and fugrad).                   */
/*  The order of information is as follows:                                 */
/* fu[0]  objective function value                                          */
/* fugrad[i][0] : objective function gradient, i = 1,...,n the components   */
/* fu[1],...,fu[nonlin]  the values of c  (if any)                          */
/* fugrad[i][1],.., fugrad[i][nonlin] gradients of  constraint functions c  */
/* i=1,..,n                                                                 */
/* bound constraints and linear constraints must not be evaluated here      */
/* The method uses the parameters                                           */
/*      epsfcn  =  relative accuracy of function values                     */
/*      taubnd  =  amount by which bound constraints may be violated        */
/*                 during finite differencing                               */
/*      difftype   (see above)                                              */
/*      analyt     (see above)                                              */
/* These must be set prior in user_init                                     */
/* ************************************************************************ */
        
     DOUBLE   *fusave = NULL,xhelp=0,xincr=0,sd1=0,sd2=0,sd3=0,d1=0,d2=0,d3=0;
     INTEGER  i,j;
     LOGICAL  changed;
     INTEGER  firstTime = 1;
    
    if (firstTime) {
        firstTime = 0;
	fusave = d1_malloc(ctx->nlin+ ctx->nonlin+1, 1, ctx);
    }	
	
    
    if ( mode < -1 || mode  > 2 || mode == 0 ) { 
		ctx->OPSlvErrorCode = OPSLV1_ERR_INV_USER_EVAL_MODE;
		ctx->OPSlvErrorInfo = mode;
		sprintf(ctx->ErrorText,"donlp2: call of user_eval with undefined mode\n");
		return;
        /*fprintf(stderr,"donlp2: call of user_eval with undefined mode\n");
        exit(1);*/
    }
    if ( mode == -1 ) {
        changed = FALSE;
        for (i = 1 ; i <= ctx->n ; i++) {
            if (ctx->xtr[i] != ctx->xsc[i]*xvar[i] ) changed = TRUE;
        }
        if ( changed || !ctx->valid ) {
            for (i = 1 ; i <= ctx->n ; i++) {
				ctx->xtr[i] = ctx->xsc[i]*xvar[i];
            }
            eval_extern(1);
            
			ctx->valid = TRUE;
        }
        return;
    }
    if ( mode >= 1 ) {
    
        /* evaluate functions and its gradients */
        
        for (i = 1 ; i <= ctx->n ; i++) {
			ctx->xtr[i] = ctx->xsc[i]*xvar[i];
        }
        /* even if xtr did not change we must evaluate externally, since now */
        /* gradients are required                                            */
        
        if (ctx->analyt ) {
        
            eval_extern(2);
            
			ctx->valid = TRUE;
            
            return;
            
        } else {
            if ( mode == 1 ) eval_extern(1);
        }
        /* continue with computing the gradients : mode = 1 or mode = 2 */
        /* require gradients                                            */
        
        if (ctx->difftype == 1 ) {
			ctx->deldif = min(tm1*sqrt(ctx->epsfcn), tm2);
            
            if ( mode == 2 ) eval_extern(1);
            
            for (j = 0 ; j <= ctx->nonlin ; j++) {
                fusave[j] = ctx->fu[j];
            }
            for (i = 1 ; i <= ctx->n ; i++) {
                xhelp = ctx->xtr[i];
                xincr = min(min(tm2, ctx->deldif*fabs(xhelp)+ ctx->deldif), ctx->taubnd);
                if ( xhelp >= zero ) {
					ctx->xtr[i] = xhelp+xincr;
                } else {
					ctx->xtr[i] = xhelp-xincr;
                }
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fugrad[i][j] = (ctx->fu[j]-fusave[j])/(ctx->xtr[i]-xhelp);
                }
				ctx->xtr[i] = xhelp;
            }
            for (j = 0 ; j <= ctx->nonlin ; j++) {
				ctx->fu[j] = fusave[j];
            }
			ctx->valid = TRUE;
            
            return;
            
        } else if (ctx->difftype == 2 ) {
			ctx->deldif = min(tm1*pow(ctx->epsfcn,one/three),tm2);
            for (j = 0 ; j <= ctx->nonlin ; j++) {
                fusave[j] = ctx->fu[j];
            }
            for (i = 1 ; i <= ctx->n ; i++) {
                xhelp  = ctx->xtr[i];
                xincr  = min(min(tm2, ctx->deldif*fabs(xhelp)+ ctx->deldif), ctx->taubnd);
				ctx->xtr[i] = xhelp+xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][1] = ctx->fu[j];
                }
				ctx->xtr[i] = xhelp-xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][2] = ctx->fu[j];
                }
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fugrad[i][j] = (ctx->fud[j][1]- ctx->fud[j][2])/(xincr+xincr);
                }
				ctx->xtr[i] = xhelp;
            }
            for (j = 0 ; j <= ctx->nonlin ; j++) {
				ctx->fu[j] = fusave[j];
            }
			ctx->valid = TRUE;
            
            return;
            
        } else {
			ctx->deldif = min(tm1*pow(ctx->epsfcn,one/seven),tm2);
            for (j = 0 ; j <= ctx->nonlin ; j++) {
                fusave[j] = ctx->fu[j];
            }
            for (i = 1 ; i <= ctx->n ; i++) {
                xhelp  = ctx->xtr[i];
                xincr  = min(min(tm2, ctx->deldif*fabs(xhelp)+ ctx->deldif), ctx->taubnd/four);
				ctx->xtr[i] = xhelp-xincr;
                
                eval_extern(1);
        
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][1] = ctx->fu[j];
                }
				ctx->xtr[i] = xhelp+xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][2] = ctx->fu[j];
                }
                xincr  = xincr+xincr;
                d1     = xincr;
				ctx->xtr[i] = xhelp-xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][3] = ctx->fu[j];
                }
				ctx->xtr[i] = xhelp+xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][4] = ctx->fu[j];
                }
                xincr  = xincr+xincr;
                d2     = xincr;
				ctx->xtr[i] = xhelp-xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][5] = ctx->fu[j];
                }
				ctx->xtr[i] = xhelp+xincr;
                
                eval_extern(1);
                
                for (j = 0 ; j <= ctx->nonlin ; j++) {
					ctx->fud[j][6] = ctx->fu[j];
                }
				ctx->xtr[i] = xhelp;
                d3     = xincr+xincr;
                for (j = 0 ; j <= ctx->nonlin ; j++) {
                    sd1 = (ctx->fud[j][2]- ctx->fud[j][1])/d1;
                    sd2 = (ctx->fud[j][4]- ctx->fud[j][3])/d2;
                    sd3 = (ctx->fud[j][6]- ctx->fud[j][5])/d3;
                    sd3 = sd2-sd3;
                    sd2 = sd1-sd2;
                    sd3 = sd2-sd3;
					ctx->fugrad[i][j] = sd1+ p4*sd2+sd3/c45;
                }
            }
            for (j = 0 ; j <= ctx->nonlin ; j++) {
				ctx->fu[j] = fusave[j];
            }
			ctx->valid = TRUE;
            
            return;
        }
    }
}
